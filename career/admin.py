from django.contrib import admin
from .models import careerDetail, careerList, careerListDisc, careerDetailReq, careerDetailReqDetail

admin.site.register(careerDetail)
admin.site.register(careerList)
admin.site.register(careerListDisc)
admin.site.register(careerDetailReq)
admin.site.register(careerDetailReqDetail)