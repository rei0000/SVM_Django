from django.db import models
class careerList(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.TextField(max_length=999)
    subtitle = models.TextField(max_length=999)
    salary = models.TextField(max_length=999)
    url = models.TextField(max_length=999)
    def __str__(self):
        return str(self.subtitle)
    class Meta:
        verbose_name_plural = "Список вакансий"

class careerListDisc(models.Model):
    id = models.AutoField(primary_key=True)
    careerList = models.ForeignKey(careerList, related_name='listItems', on_delete=models.CASCADE)
    itemTitle = models.TextField(max_length=999)
    itemName = models.TextField(max_length=999)
    def __str__(self):
        return str(self.itemName)
    class Meta:
        verbose_name_plural = "Список вакансий, детали"
class careerDetail(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.TextField(max_length=999)
    vacancyName = models.TextField(max_length=999)
    addres = models.TextField(max_length=999)
    salary = models.TextField(max_length=999)
    url = models.TextField(max_length=999, null=True)
    def __str__(self):
        return str(self.vacancyName)
    class Meta:
        verbose_name_plural = "Детальная вакансия"

class careerDetailReq(models.Model):
    id = models.AutoField(primary_key=True)
    careerDetail = models.ForeignKey(careerDetail, related_name='require', on_delete=models.CASCADE)
    name= models.TextField(max_length=999)
    def __str__(self):
        return str(self.name)
    class Meta:
        verbose_name_plural = "Детальная вакансия, список"

class careerDetailReqDetail(models.Model):
    id = models.AutoField(primary_key=True)
    careerDetailReq = models.ForeignKey(careerDetailReq, related_name='reqValue', on_delete=models.CASCADE)
    req= models.TextField(max_length=999)
    def __str__(self):
        return str(self.req)
    class Meta:
        verbose_name_plural = "Детальная вакансия, список требований"