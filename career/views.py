from django.shortcuts import render

def career(request):
    return render(request, 'main/career.html')

def test(request, detail_vacancy):
    return render(request, 'main/career/detail_vacancy.html')
