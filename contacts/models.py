from django.db import models
class contactsBlock(models.Model):
    id = models.AutoField(primary_key=True)
    nameBlock = models.TextField(max_length=999)

    def __str__(self):
        return str(self.nameBlock)
    class Meta:
        verbose_name_plural = "Контакты наименование блока"
class contactsItemsList(models.Model):
    id = models.AutoField(primary_key=True)
    contactsItems = models.ForeignKey(contactsBlock, related_name='contactsItems', on_delete=models.CASCADE)
    item = models.TextField(max_length=999)

    def __str__(self):
        return str(self.item)
    class Meta:
        verbose_name_plural = "Контакты лист"