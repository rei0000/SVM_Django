from django.contrib import admin
from .models import contactsBlock, contactsItemsList

admin.site.register(contactsBlock)
admin.site.register(contactsItemsList)