from calendar import c
from django.db import models
from .validators import validate_file_size

class MainHistorySlider(models.Model):
    id = models.AutoField(primary_key=True)
    year = models.TextField('Год', max_length=999)
    img = models.FileField(upload_to='static/static/img/MainHistorySlider', validators=[validate_file_size])
    disc = models.TextField('Описание', max_length=999)

    def __str__(self):
        return str(self.year)

    class Meta:
        verbose_name_plural = "История слайдер"