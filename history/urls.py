# from django.conf.urls import url
from django.urls import path, include
from . import views
from django.urls import include, re_path
from rest_framework import routers
router = routers.DefaultRouter()
# router.register(r'postsgeographytable', views.GeoraphyTableViewSet)
# router.register(r'companyIcon', views.CompanyIconsViewSet)
# router.register(r'mainPageSlider', views.MainPageSliderViewSet)
# router.register(r'мainhistoryslider', views.MainHistorySliderViewSet)
# router.register(r'мainProductList', views.MainProductsListViewSet)
# router.register(r'NomenclatureProductsBlock', views.NomenclatureProductsBlockViewSet)
# router.register(r'NomenclatureProductsDiscFirstViewSet', views.NomenclatureProductsDiscFirstViewSet)
# router.register(r'NomenclatureProductsDiscSecondViewSet', views.NomenclatureProductsDiscSecondViewSet)
# router.register(r'NomenclatureDiscSecond', views.NomenclatureDiscSecondViewSet)
# router.register(r'OptionsProductsBlock', views.OptionsProductsBlockViewSet)
# router.register(r'AdvantagesProductsBlock', views.AdvantagesProductsBlockViewSet)
# router.register(r'DockFilesList', views.DockFilesListViewSet)
# router.register(r'docProduct', views.docProductViewSet)
# router.register(r'aboutUsProdBase', views.aboutUsProdBaseViewSet)
# router.register(r'careerListViewSet', views.careerListViewSet)
# router.register(r'contactsBlockViewSet', views.contactsBlockViewSet)
# router.register(r'aboutUsMainBlockViewSet', views.aboutUsMainBlockViewSet)
# router.register(r'aboutUsSecondBlockViewSet', views.aboutUsSecondBlockViewSet)
# router.register(r'aboutUsNumberBlockViewSet', views.aboutUsNumberBlockViewSet)
# router.register(r'careerDetailViewSet', views.careerDetailViewSet)
urlpatterns = [
    # path('', views.index),
    path('api', include(router.urls)),
    path('', views.history),
    # path('about-us', views.aboutUs),
    # path('products', views.products),
    # path('products/welded_valves', views.index1),
    # path('products/sour_service', views.index1),
    # path('products/ball_valves', views.index1),
    # path('products/drives', views.index1),
    # path('career', views.career),
    # path('contacts', views.contacts),
    # path('posts', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # # re_path(r'^(?P<question_id>[0-9]+)/$', views.product),
    # re_path(r'^products/(?P<detail_product>\w+)/$', views.product),
    # re_path(r'^career/(?P<detail_vacancy>\w+)/$', views.test),
]