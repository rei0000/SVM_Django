from django.shortcuts import render

def history(request):
    return render(request, 'main/history.html')
