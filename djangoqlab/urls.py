from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('index.urls')),
    path('history/', include('history.urls')),
    path('about-us/', include('aboutUs.urls')),
    path('products/', include('products.urls')),
    path('career/', include('career.urls')),
    path('contacts/', include('contacts.urls')),
]