from django.shortcuts import render
from django_filters import rest_framework as filters

def products(request):
    return render(request, 'main/products.html')

def product(request, detail_product):
    return render(request, 'main/products/detail_product.html')