from django.contrib import admin
from .models import MainProductsList, OptionsProductsBlock, AdvantagesProductsBlock, DockFilesList, NomenclatureProductsDiscFirst, NomenclatureProductsDiscFirstList, NomenclatureProductsDiscSecond, NomenclatureProductsDiscSecondList, NomenclatureProductsBlock, ProductConnections

admin.site.register(MainProductsList)
admin.site.register(OptionsProductsBlock)
admin.site.register(AdvantagesProductsBlock)
admin.site.register(DockFilesList)
admin.site.register(NomenclatureProductsDiscFirst)
admin.site.register(NomenclatureProductsDiscFirstList)
admin.site.register(NomenclatureProductsDiscSecond)
admin.site.register(NomenclatureProductsDiscSecondList)
admin.site.register(NomenclatureProductsBlock)
admin.site.register(ProductConnections)