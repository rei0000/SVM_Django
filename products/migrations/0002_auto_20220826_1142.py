# Generated by Django 3.2.9 on 2022-08-26 07:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='docProduct',
        ),
        migrations.AlterModelOptions(
            name='advantagesproductsblock',
            options={'verbose_name_plural': 'Детальная продукция, блок Аккордион'},
        ),
        migrations.AlterModelOptions(
            name='dockfileslist',
            options={'verbose_name_plural': 'Детальная продукция, вложенные файлы'},
        ),
        migrations.AlterModelOptions(
            name='mainproductslist',
            options={'verbose_name_plural': 'Список продукции'},
        ),
        migrations.AlterModelOptions(
            name='nomenclatureproductsblock',
            options={'verbose_name_plural': 'Номенклатура DN блок'},
        ),
        migrations.AlterModelOptions(
            name='nomenclatureproductsdiscfirst',
            options={'verbose_name_plural': 'Номенклатура свойства первый блок'},
        ),
        migrations.AlterModelOptions(
            name='nomenclatureproductsdiscfirstlist',
            options={'verbose_name_plural': 'Номенклатура свойства первый блок подробности'},
        ),
        migrations.AlterModelOptions(
            name='nomenclatureproductsdiscsecond',
            options={'verbose_name_plural': 'Номенклатура свойства второй блок'},
        ),
        migrations.AlterModelOptions(
            name='nomenclatureproductsdiscsecondlist',
            options={'verbose_name_plural': 'Номенклатура свойства второй блок подробности'},
        ),
        migrations.AlterModelOptions(
            name='optionsproductsblock',
            options={'verbose_name_plural': 'Продукция, возможные опции'},
        ),
        migrations.AlterModelOptions(
            name='productconnections',
            options={'verbose_name_plural': 'Номенклатура DN свойства соединения'},
        ),
        migrations.AddField(
            model_name='mainproductslist',
            name='img2',
            field=models.FileField(null=True, upload_to='static/static/img/MainProductsList'),
        ),
        migrations.AlterField(
            model_name='advantagesproductsblock',
            name='body',
            field=models.TextField(max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='advantagesproductsblock',
            name='img',
            field=models.FileField(blank=True, null=True, upload_to='static/static/img/AdvantagesProductsBlock'),
        ),
        migrations.AlterField(
            model_name='advantagesproductsblock',
            name='number',
            field=models.TextField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='advantagesproductsblock',
            name='title',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='dockfileslist',
            name='adminupload',
            field=models.FileField(null=True, upload_to='static/static/img/DockFilesList'),
        ),
        migrations.AlterField(
            model_name='dockfileslist',
            name='title',
            field=models.CharField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='mainproductslist',
            name='disc',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='mainproductslist',
            name='img',
            field=models.FileField(null=True, upload_to='static/static/img/MainProductsList'),
        ),
        migrations.AlterField(
            model_name='mainproductslist',
            name='name',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsblock',
            name='productConnection',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsblock',
            name='productName',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsblock',
            name='productPressure',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsblock',
            name='productPressures',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsblock',
            name='productSize',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsblock',
            name='productSizes',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsblock',
            name='titleArray',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsdiscfirst',
            name='name',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsdiscfirstlist',
            name='item',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsdiscsecond',
            name='name',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='nomenclatureproductsdiscsecondlist',
            name='item',
            field=models.TextField(max_length=500),
        ),
        migrations.AlterField(
            model_name='optionsproductsblock',
            name='disc',
            field=models.TextField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='productconnections',
            name='item',
            field=models.TextField(max_length=500, null=True),
        ),
    ]
