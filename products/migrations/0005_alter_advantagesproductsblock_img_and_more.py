# Generated by Django 4.1.1 on 2022-10-06 00:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0004_alter_advantagesproductsblock_body'),
    ]

    operations = [
        migrations.AlterField(
            model_name='advantagesproductsblock',
            name='img',
            field=models.FileField(default='', editable=False, upload_to='static/static/img/AdvantagesProductsBlock'),
        ),
        migrations.AlterField(
            model_name='dockfileslist',
            name='adminupload',
            field=models.FileField(default='', editable=False, upload_to='static/static/img/DockFilesList'),
        ),
        migrations.AlterField(
            model_name='mainproductslist',
            name='img',
            field=models.FileField(default='', editable=False, upload_to='static/static/img/MainProductsList'),
        ),
        migrations.AlterField(
            model_name='mainproductslist',
            name='img2',
            field=models.FileField(default='', editable=False, upload_to='static/static/img/MainProductsList'),
        ),
    ]
