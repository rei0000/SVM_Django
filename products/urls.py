from django.urls import path
from . import views
from django.urls import re_path
from rest_framework import routers
router = routers.DefaultRouter()
urlpatterns = [
    path('', views.products),
    re_path(r'^(?P<detail_product>\w+)/$', views.product),
]