from calendar import c
from django.db import models
from ckeditor.fields import RichTextField 
from .validators import validate_file_size

class MainProductsList(models.Model):
    name = models.TextField(max_length=999, null=True)
    disc = models.TextField(max_length=999, null=True)
    img = models.FileField(upload_to='static/static/img/MainProductsList', validators=[validate_file_size])
    img2 = models.FileField(upload_to='static/static/img/MainProductsList', validators=[validate_file_size])
    referAddres = models.TextField(max_length=999, null=True)
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return (str(self.name))
    class Meta:
        verbose_name_plural = "Список продукции"
        
class OptionsProductsBlock(models.Model):
    referAddres = models.TextField(max_length=999, null=True)
    disc = models.TextField(max_length=999, null=True)
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return (str(self.disc))
    class Meta:
        verbose_name_plural = "Продукция, возможные опции"

class AdvantagesProductsBlock(models.Model):
    referAddres = models.TextField(max_length=999, null=True)
    body = RichTextField(blank=True, null=True, max_length=999)
    title = models.TextField(max_length=999, null=True)
    img = models.FileField(upload_to='static/static/img/AdvantagesProductsBlock', validators=[validate_file_size])
    number = models.TextField(max_length=999, null=True)
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return (str(self.title))
    class Meta:
        verbose_name_plural = "Детальная продукция, блок Аккордион"

class DockFilesList(models.Model):
    adminupload=models.FileField(upload_to='static/static/img/DockFilesList', validators=[validate_file_size])
    title=models.CharField(max_length=999, null=True)
    id = models.AutoField(primary_key=True)
    referAddres = models.TextField(max_length=999, null=True)

    def __str__(self):
        return (str(self.title))
    class Meta:
        verbose_name_plural = "Детальная продукция, вложенные файлы"
class NomenclatureProductsDiscFirst(models.Model):
    referAddres=models.TextField(max_length=999, null=True)
    name=models.TextField(max_length=999, null=True)
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return (str(self.referAddres) + ' ' + str(self.name))
    class Meta:
        verbose_name_plural = "Номенклатура свойства первый блок"
class NomenclatureProductsDiscFirstList(models.Model):
    id = models.AutoField(primary_key=True)
    NomenclatureProductsDiscFirst = models.ForeignKey(NomenclatureProductsDiscFirst, related_name='nomenclatureDiscList', on_delete=models.CASCADE)
    item=models.TextField(max_length=999, null=True)

    def __str__(self):
        return (str(self.item))
    class Meta:
        verbose_name_plural = "Номенклатура свойства первый блок подробности"
class NomenclatureProductsDiscSecond(models.Model):
    referAddres=models.TextField(max_length=999, null=True)
    name=models.TextField(max_length=999, null=True)
    id = models.AutoField(primary_key=True)


    def __str__(self):
        return (str(self.referAddres) + ' ' + str(self.name))
    class Meta:
        verbose_name_plural = "Номенклатура свойства второй блок"
class NomenclatureProductsDiscSecondList(models.Model):
    id = models.AutoField(primary_key=True)
    NomenclatureProductsDiscSecond = models.ForeignKey(NomenclatureProductsDiscSecond, related_name='nomenclatureDiscList', on_delete=models.CASCADE)
    item=models.TextField(max_length=999)

    def __str__(self):
        return (str(self.item))
    class Meta:
        verbose_name_plural = "Номенклатура свойства второй блок подробности"
    
class NomenclatureProductsBlock(models.Model):
    id = models.AutoField(primary_key=True)
    titleArray = models.TextField(max_length=999, null=True)
    productName = models.TextField(max_length=999, null=True)
    productSize = models.TextField(max_length=999, null=True)
    productSizes = models.TextField(max_length=999, null=True)
    productPressure = models.TextField(max_length=999, null=True)
    productPressures = models.TextField(max_length=999, null=True)
    productConnection = models.TextField(max_length=999, null=True)
    referAddres = models.TextField(max_length=999, null=True)

    def __str__(self):
        return (str(self.referAddres) + ' ' + str(self.titleArray))
    class Meta:
        verbose_name_plural = "Номенклатура DN блок"

class ProductConnections(models.Model):
    id = models.AutoField(primary_key=True)
    NomenclatureProductsBlock = models.ForeignKey(NomenclatureProductsBlock, related_name='productConnections', on_delete=models.CASCADE)
    item=models.TextField(max_length=999, null=True)

    def __str__(self):
        return (str(self.NomenclatureProductsBlock) + ' ' + str(self.item))
    class Meta:
        verbose_name_plural = "Номенклатура DN свойства соединения"