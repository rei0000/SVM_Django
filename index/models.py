from calendar import c
from django.db import models
from ckeditor.fields import RichTextField
from .validators import validate_file_size

class GeoraphyTable(models.Model):
    id = models.AutoField(primary_key=True)
    disc = models.TextField('Описание', max_length=999)

    def __str__(self):
        return str(self.disc)
    class Meta:
        verbose_name_plural = "Главная слайдер и тайтл"

class CompanyIcons(models.Model):
    id = models.AutoField(primary_key=True)
    img = models.FileField(upload_to='static/static/img/CompanyIcons', validators=[validate_file_size])

    def __str__(self):
        return str(self.img)
    class Meta:
        verbose_name_plural = "Логотипы компаний партнеров"
class MainPageSlider(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField('Заголовок', max_length=999)
    img = models.FileField(upload_to='static/static/img/MainPageSlider')

    def __str__(self):
        return str(self.name)
    class Meta:
        verbose_name_plural = "Главная заголовок"

class QuoteBlock(models.Model):
    id = models.AutoField(primary_key=True)
    img = models.FileField(upload_to='static/static/img/QuoteBlock', validators=[validate_file_size])
    name = models.TextField(max_length=999)
    jobTitle = models.TextField(max_length=999)

    def __str__(self):
        return str(self.name)
    class Meta:
        verbose_name_plural = "Главная фото, имя, должность"

class QuoteBlockDisc(models.Model):
    id = models.AutoField(primary_key=True)
    QuoteBlock = models.ForeignKey(QuoteBlock, related_name='quoteList', on_delete=models.CASCADE)
    disc = RichTextField(blank=True, null=True, max_length=999)

    def __str__(self):
        return str(self.disc)
    class Meta:
        verbose_name_plural = "Главная цитата"