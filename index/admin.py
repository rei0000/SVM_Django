from django.contrib import admin
from .models import GeoraphyTable, CompanyIcons, MainPageSlider, QuoteBlock, QuoteBlockDisc

admin.site.register(GeoraphyTable)
admin.site.register(CompanyIcons)
admin.site.register(MainPageSlider)
admin.site.register(QuoteBlock)
admin.site.register(QuoteBlockDisc)