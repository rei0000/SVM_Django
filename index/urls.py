from django.urls import path, include
from . import views
from django.urls import include
from rest_framework import routers
router = routers.DefaultRouter()
router.register(r'postsgeographytable', views.GeoraphyTableViewSet)
router.register(r'companyIcon', views.CompanyIconsViewSet)
router.register(r'mainPageSlider', views.MainPageSliderViewSet)
router.register(r'QuoteBlockViewSet', views.QuoteBlockViewSet)
router.register(r'MainHistorySliderViewSet', views.MainHistorySliderViewSet)
router.register(r'mainProductList', views.MainProductsListViewSet)
router.register(r'NomenclatureProductsBlock', views.NomenclatureProductsBlockViewSet)
router.register(r'NomenclatureProductsDiscFirstViewSet', views.NomenclatureProductsDiscFirstViewSet)
router.register(r'NomenclatureProductsDiscSecondViewSet', views.NomenclatureProductsDiscSecondViewSet)
router.register(r'OptionsProductsBlock', views.OptionsProductsBlockViewSet)
router.register(r'AdvantagesProductsBlock', views.AdvantagesProductsBlockViewSet)
router.register(r'DockFilesList', views.DockFilesListViewSet)
router.register(r'aboutUsProdBase', views.aboutUsProdBaseViewSet)
router.register(r'careerListViewSet', views.careerListViewSet)
router.register(r'contactsBlockViewSet', views.contactsBlockViewSet)
router.register(r'aboutUsMainBlockViewSet', views.aboutUsMainBlockViewSet)
router.register(r'aboutUsSecondBlockViewSet', views.aboutUsSecondBlockViewSet)
router.register(r'aboutUsNumberBlockViewSet', views.aboutUsNumberBlockViewSet)
router.register(r'careerDetailViewSet', views.careerDetailViewSet)
router.register(r'aboutUsRewueFileViewSet', views.aboutUsRewueFileViewSet)
urlpatterns = [
    path('', views.index),
    path('api', include(router.urls)),
]