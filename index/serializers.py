from aboutUs.models import aboutUsMainBlock, aboutUsMainBlockSlider, aboutUsMainBlockText, aboutUsNumberBlock, aboutUsProdBase, aboutUsProdBaseSlider, aboutUsSecondBlock, aboutUsSecondBlockSlider, aboutUsRewueFile
from career.models import careerDetail, careerDetailReq, careerDetailReqDetail, careerList, careerListDisc
from contacts.models import contactsBlock, contactsItemsList
from history.models import MainHistorySlider
from products.models import AdvantagesProductsBlock, DockFilesList, MainProductsList, NomenclatureProductsBlock, NomenclatureProductsDiscFirst, NomenclatureProductsDiscFirstList, NomenclatureProductsDiscSecond, NomenclatureProductsDiscSecondList, OptionsProductsBlock, ProductConnections
from .models import GeoraphyTable, MainPageSlider,CompanyIcons, QuoteBlock, QuoteBlockDisc
from rest_framework import serializers      
class GeoraphyTableSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GeoraphyTable
        fields = ['id', 'disc']
class CompanyIconsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CompanyIcons
        fields = ['id', 'img']
class MainPageSliderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MainPageSlider
        fields = ['id', 'img', 'name']
class MainHistorySliderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MainHistorySlider
        fields = ['id', 'year', 'img', 'disc']
class QuoteBlockDiscSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuoteBlockDisc
        fields = ['id', 'QuoteBlock', 'disc']

class QuoteBlockSerializer(serializers.ModelSerializer):
    quoteList = QuoteBlockDiscSerializer(many=True, read_only=True)
    class Meta:
        model = QuoteBlock
        fields = ['id', 'img', 'name', 'jobTitle', 'quoteList']
        
class MainProductsListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MainProductsList
        fields = ['name', 'img', 'img2', 'disc', 'referAddres', 'id']
class OptionsProductsBlockSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = OptionsProductsBlock
        fields = ['referAddres', 'disc', 'id']

class AdvantagesProductsBlockSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AdvantagesProductsBlock
        fields = ['body', 'title', 'referAddres', 'img', 'number', 'id']

class DockFilesListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DockFilesList
        fields = ['adminupload', 'title', 'id', 'referAddres']
class careerListDiscSerializer(serializers.ModelSerializer):
    class Meta:
        model = careerListDisc
        fields = ['id', 'careerList', 'itemTitle', 'itemName']
class careerListSerializer(serializers.ModelSerializer):
    listItems = careerListDiscSerializer(many=True, read_only=True)
    class Meta:
        model = careerList
        fields = ['id', 'date', 'subtitle', 'salary', 'url', 'listItems']
class aboutUsMainBlockSliderSerializer(serializers.ModelSerializer):
    class Meta:
        model = aboutUsMainBlockSlider
        fields = ['id', 'aboutUsMainBlock', 'imgupload']

class aboutUsMainBlockTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = aboutUsMainBlockText
        fields = ['id', 'aboutUsMainBlock', 'text']

class aboutUsMainBlockSerializer(serializers.ModelSerializer):
    sliderList = aboutUsMainBlockSliderSerializer(many=True, read_only=True)
    textList = aboutUsMainBlockTextSerializer(many=True, read_only=True)

    class Meta:
        model = aboutUsMainBlock
        fields = ['id', 'title', 'sliderList', 'textList']
class aboutUsSecondBlockSliderSerializer(serializers.ModelSerializer):
    class Meta:
        model = aboutUsSecondBlockSlider
        fields = ['id', 'aboutUsSecondBlock', 'imgupload', 'disc']
class aboutUsSecondBlockSerializer(serializers.ModelSerializer):
    sliderList = aboutUsSecondBlockSliderSerializer(many=True, read_only=True)

    class Meta:
        model = aboutUsSecondBlock
        fields = ['id', 'title', 'disc', 'sliderList']
class aboutUsNumberBlockSerializer(serializers.ModelSerializer):
    class Meta:
        model = aboutUsNumberBlock
        fields = ['id', 'number', 'disc']
class aboutUsProdBaseSliderSerializer(serializers.ModelSerializer):
    class Meta:
        model = aboutUsProdBaseSlider
        fields = ['id', 'aboutUsProdBase', 'imgupload']
class aboutUsProdBaseSerializer(serializers.HyperlinkedModelSerializer):
    sliderList = aboutUsProdBaseSliderSerializer(many=True, read_only=True)
    class Meta:
        model = aboutUsProdBase
        fields = ['id', 'anchor', 'title', 'description', 'sliderList' ]
class careerDetailReqDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = careerDetailReqDetail
        fields = ['id', 'careerDetailReq', 'req']
class careerDetailReqSerializer(serializers.HyperlinkedModelSerializer):
    reqValue = careerDetailReqDetailSerializer(many=True, read_only=True)
    class Meta:
        model = careerDetailReq
        fields = ['id', 'reqValue', 'name', 'careerDetail']
class careerDetailSerializer(serializers.HyperlinkedModelSerializer):
    require = careerDetailReqSerializer(many=True, read_only=True)
    class Meta:
        model = careerDetail
        fields = ['id', 'require', 'date', 'vacancyName', 'addres', 'salary', 'url']
class contactsItemsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = contactsItemsList
        fields = ['id', 'contactsItems', 'item']

class contactsBlockSerializer(serializers.ModelSerializer):
    contactsItems = contactsItemsListSerializer(many=True, read_only=True)

    class Meta:
        model = contactsBlock
        fields = ['id', 'nameBlock', 'contactsItems']
class NomenclatureProductsDiscFirstListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NomenclatureProductsDiscFirstList
        fields = ['id', 'NomenclatureProductsDiscFirst', 'item']

class NomenclatureProductsDiscFirstSerializer(serializers.ModelSerializer):
    nomenclatureDiscList = NomenclatureProductsDiscFirstListSerializer(many=True, read_only=True)

    class Meta:
        model = NomenclatureProductsDiscFirst
        fields = ['id', 'referAddres', 'name', 'nomenclatureDiscList']
        
class NomenclatureProductsDiscSeconsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NomenclatureProductsDiscSecondList
        fields = ['id', 'NomenclatureProductsDiscSecond', 'item']

class NomenclatureProductsDiscSecondSerializer(serializers.ModelSerializer):
    nomenclatureDiscList = NomenclatureProductsDiscSeconsListSerializer(many=True, read_only=True)

    class Meta:
        model = NomenclatureProductsDiscSecond
        fields = ['id', 'referAddres', 'name', 'nomenclatureDiscList']
class ProductConnectionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductConnections
        fields = ['id', 'NomenclatureProductsBlock', 'item']

class NomenclatureProductsBlockSerializer(serializers.ModelSerializer):
    productConnections = ProductConnectionsSerializer(many=True, read_only=True)
    class Meta:
        model = NomenclatureProductsBlock
        fields = ['id', 'referAddres', 'productConnections', 'titleArray','productName', 'productSize', 'productSizes', 'productPressure', 'productPressures', 'productConnection']
        
class aboutUsRewueFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = aboutUsRewueFile
        fields = ['id', 'titleDescription', 'rewuefile']