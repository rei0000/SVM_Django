from django.shortcuts import render
from aboutUs.models import aboutUsMainBlock, aboutUsNumberBlock, aboutUsProdBase, aboutUsSecondBlock, aboutUsRewueFile
from career.models import careerDetail, careerList
from contacts.models import contactsBlock

from history.models import MainHistorySlider
from products.models import AdvantagesProductsBlock, DockFilesList, MainProductsList, NomenclatureProductsBlock, NomenclatureProductsDiscFirst, NomenclatureProductsDiscSecond, OptionsProductsBlock
from .models import GeoraphyTable, MainPageSlider, CompanyIcons, QuoteBlock
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import AdvantagesProductsBlockSerializer, DockFilesListSerializer, GeoraphyTableSerializer, CompanyIconsSerializer, MainHistorySliderSerializer, MainPageSliderSerializer, MainProductsListSerializer, NomenclatureProductsBlockSerializer, NomenclatureProductsDiscFirstSerializer, NomenclatureProductsDiscSecondSerializer, OptionsProductsBlockSerializer, QuoteBlockSerializer, aboutUsMainBlockSerializer, aboutUsNumberBlockSerializer, aboutUsProdBaseSerializer, aboutUsSecondBlockSerializer, careerDetailSerializer, careerListSerializer, contactsBlockSerializer, aboutUsRewueFileSerializer
from django_filters import rest_framework as filters
def index(request):
    return render(request, 'main/index.html')
class GeoraphyTableViewSet(viewsets.ModelViewSet):
    queryset = GeoraphyTable.objects.all()
    serializer_class = GeoraphyTableSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class CompanyIconsViewSet(viewsets.ModelViewSet):
    queryset = CompanyIcons.objects.all()
    serializer_class = CompanyIconsSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class MainPageSliderViewSet(viewsets.ModelViewSet):
    queryset = MainPageSlider.objects.all()
    serializer_class = MainPageSliderSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class QuoteBlockViewSet(viewsets.ModelViewSet):
    queryset = QuoteBlock.objects.all()
    serializer_class = QuoteBlockSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'name', 'jobTitle', 'quoteList')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class MainHistorySliderViewSet(viewsets.ModelViewSet):
    queryset = MainHistorySlider.objects.all()
    serializer_class = MainHistorySliderSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('year', 'disc')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class MainProductsListViewSet(viewsets.ModelViewSet):
    queryset = MainProductsList.objects.all()
    serializer_class = MainProductsListSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('name', 'disc', 'referAddres','id')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class NomenclatureProductsBlockViewSet(viewsets.ModelViewSet):
    queryset = NomenclatureProductsBlock.objects.all()
    serializer_class = NomenclatureProductsBlockSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'referAddres')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class OptionsProductsBlockViewSet(viewsets.ModelViewSet):
    queryset = OptionsProductsBlock.objects.all()
    serializer_class = OptionsProductsBlockSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('referAddres', 'disc', 'id')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class AdvantagesProductsBlockViewSet(viewsets.ModelViewSet):
    queryset = AdvantagesProductsBlock.objects.all()
    serializer_class = AdvantagesProductsBlockSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('body', 'title', 'referAddres', 'number', 'id')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class DockFilesListViewSet(viewsets.ModelViewSet):
    queryset = DockFilesList.objects.all()
    serializer_class = DockFilesListSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'title', 'referAddres')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class careerListViewSet(viewsets.ModelViewSet):
    queryset = careerList.objects.all()
    serializer_class = careerListSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'date', 'subtitle', 'salary', 'url', 'listItems')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class contactsBlockViewSet(viewsets.ModelViewSet):
    queryset = contactsBlock.objects.all()
    serializer_class = contactsBlockSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'nameBlock', 'contactsItems')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class aboutUsMainBlockViewSet(viewsets.ModelViewSet):
    queryset = aboutUsMainBlock.objects.all()
    serializer_class = aboutUsMainBlockSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'title', 'sliderList', 'textList')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class aboutUsSecondBlockViewSet(viewsets.ModelViewSet):
    queryset = aboutUsSecondBlock.objects.all()
    serializer_class = aboutUsSecondBlockSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'title', 'disc', 'sliderList')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class aboutUsNumberBlockViewSet(viewsets.ModelViewSet):
    queryset = aboutUsNumberBlock.objects.all()
    serializer_class = aboutUsNumberBlockSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'number', 'disc')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class aboutUsProdBaseViewSet(viewsets.ModelViewSet):
    queryset = aboutUsProdBase.objects.all()
    serializer_class = aboutUsProdBaseSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'anchor', 'title', 'description', 'sliderList')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class careerDetailViewSet(viewsets.ModelViewSet):
    queryset = careerDetail.objects.all()
    serializer_class = careerDetailSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'require', 'date', 'vacancyName', 'addres', 'salary', 'url')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class NomenclatureProductsDiscFirstViewSet(viewsets.ModelViewSet):
    queryset = NomenclatureProductsDiscFirst.objects.all()
    serializer_class = NomenclatureProductsDiscFirstSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'referAddres', 'name', 'nomenclatureDiscList')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    
class NomenclatureProductsDiscSecondViewSet(viewsets.ModelViewSet):
    queryset = NomenclatureProductsDiscSecond.objects.all()
    serializer_class = NomenclatureProductsDiscSecondSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_fields = ('id', 'referAddres', 'name', 'nomenclatureDiscList')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class aboutUsRewueFileViewSet(viewsets.ModelViewSet):
    queryset = aboutUsRewueFile.objects.all()
    serializer_class = aboutUsRewueFileSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    