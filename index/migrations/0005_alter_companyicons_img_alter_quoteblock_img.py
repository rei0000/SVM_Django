# Generated by Django 4.1.1 on 2022-10-06 00:22

from django.db import migrations, models
import index.validators


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0004_alter_quoteblockdisc_disc'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companyicons',
            name='img',
            field=models.FileField(upload_to='static/static/img/CompanyIcons', validators=[index.validators.validate_file_size]),
        ),
        migrations.AlterField(
            model_name='quoteblock',
            name='img',
            field=models.FileField(upload_to='static/static/img/QuoteBlock', validators=[index.validators.validate_file_size]),
        ),
    ]
