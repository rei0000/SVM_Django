# Generated by Django 3.2.9 on 2022-08-29 09:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0002_auto_20220826_1142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='georaphytable',
            name='disc',
            field=models.TextField(max_length=999, verbose_name='Описание'),
        ),
        migrations.AlterField(
            model_name='mainpageslider',
            name='name',
            field=models.TextField(max_length=999, verbose_name='Заголовок'),
        ),
        migrations.AlterField(
            model_name='quoteblock',
            name='jobTitle',
            field=models.TextField(max_length=999),
        ),
        migrations.AlterField(
            model_name='quoteblock',
            name='name',
            field=models.TextField(max_length=999),
        ),
        migrations.AlterField(
            model_name='quoteblockdisc',
            name='disc',
            field=models.TextField(max_length=999),
        ),
    ]
