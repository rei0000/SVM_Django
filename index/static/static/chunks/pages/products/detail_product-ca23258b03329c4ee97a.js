(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [649], {
        10075: function (t, e, s) {
            "use strict";
            s(24812);
            var c = s(96156),
                r = s(85893),
                o = (s(67294), s(47166)),
                n = s.n(o),
                i = s(97085),
                _ = s.n(i);

            function d(t, e) {
                var s = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var c = Object.getOwnPropertySymbols(t);
                    e && (c = c.filter((function (e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    }))), s.push.apply(s, c)
                }
                return s
            }

            function a(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var s = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? d(Object(s), !0).forEach((function (e) {
                        (0, c.Z)(t, e, s[e])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(s)) : d(Object(s)).forEach((function (e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(s, e))
                    }))
                }
                return t
            }
            var l = function (t) {
                return (0, r.jsx)("svg", a(a({}, t), {}, {
                    children: (0, r.jsx)("path", {
                        d: "M21.6983 0L20.3453 1.45968L26.346 7.96623H0V10.0338H26.346L20.3453 16.5403L21.6983 18L30 9L21.6983 0Z",
                        fill: "currentColor"
                    })
                }))
            };
            l.defaultProps = {
                width: "30",
                height: "18",
                viewBox: "0 0 30 18",
                fill: "none",
                xmlns: "http://www.w3.org/2000/svg"
            };
            var u = function (t) {
                return (0, r.jsx)("svg", a(a({}, t), {}, {
                    children: (0, r.jsx)("path", {
                        d: "M8.30169 18L9.65473 16.5403L3.65397 10.0338L30 10.0338L30 7.96623L3.65397 7.96623L9.65473 1.45968L8.30169 -1.89693e-06L7.86805e-07 9L8.30169 18Z",
                        fill: "currentColor"
                    })
                }))
            };
            u.defaultProps = {
                width: "30",
                height: "18",
                viewBox: "0 0 30 18",
                fill: "none",
                xmlns: "http://www.w3.org/2000/svg"
            };
            var m = n().bind(_());
            e.Z = function (t) {
                var e = t.className,
                    s = t.onClick,
                    c = t.type,
                    o = void 0 === c ? "right" : c,
                    n = t.disabled,
                    i = void 0 !== n && n,
                    _ = t.white,
                    d = void 0 !== _ && _,
                    a = t.blue,
                    p = void 0 !== a && a,
                    P = "left" === o ? u : l;
                return (0, r.jsx)("button", {
                    className: m("Arrow", "Arrow_".concat(o), {
                        Arrow_disabled: i
                    }, {
                        Arrow_white: d
                    }, {
                        Arrow_blue: p
                    }, e),
                    onClick: s,
                    children: (0, r.jsx)(P, {})
                })
            }
        },
        59927: function (t, e, s) {
            "use strict";
            s.r(e), s.d(e, {
                default: function () {
                    return jt
                }
            });
            s(24812);
            var c = s(85893),
                r = s(67294),
                o = s(9008),
                n = s(14146),
                i = (s(66992), s(21249), s(41539), s(88674), s(78783), s(33948), s(60285), s(15027)),
                _ = s(5563),
                d = s(96156),
                a = s(30653),
                l = s(47166),
                u = s.n(l),
                m = s(75764),
                p = s.n(m);

            function P(t, e) {
                var s = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var c = Object.getOwnPropertySymbols(t);
                    e && (c = c.filter((function (e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    }))), s.push.apply(s, c)
                }
                return s
            }

            function N(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var s = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? P(Object(s), !0).forEach((function (e) {
                        (0, d.Z)(t, e, s[e])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(s)) : P(Object(s)).forEach((function (e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(s, e))
                    }))
                }
                return t
            }
            var I = function (t) {
                return (0, c.jsxs)("svg", N(N({}, t), {}, {
                    children: [(0, c.jsx)("path", {
                        d: "M4.53125 14.5039H24.4687",
                        stroke: "#292C31",
                        strokeWidth: "2",
                        strokeLinecap: "round",
                        strokeLinejoin: "round"
                    }), (0, c.jsx)("path", {
                        d: "M14.5 4.52734V24.4645",
                        stroke: "#292C31",
                        strokeWidth: "2",
                        strokeLinecap: "round",
                        strokeLinejoin: "round"
                    })]
                }))
            };
            I.defaultProps = {
                width: "29",
                height: "29",
                viewBox: "0 0 29 29",
                fill: "#292C31",
                xmlns: "http://www.w3.org/2000/svg"
            };
            var f = u().bind(p()),
                h = function (t) {
                    var e = t.className,
                        s = t.title,
                        o = t.children,
                        n = t.isOpened,
                        i = void 0 !== n && n,
                        _ = t.onClick,
                        d = (0, r.useRef)(null),
                        l = (0, r.useState)(0),
                        u = l[0],
                        m = l[1];
                    return (0, r.useEffect)((function () {
                        if (d && d.current && i) {
                            var t = d.current.getBoundingClientRect().height;
                            m(t)
                        } else i || m(0)
                    }), [i]), (0, c.jsxs)("div", {
                        className: f("Accordion", {
                            Accordion_opened: i
                        }, e),
                        children: [(0, c.jsxs)("div", {
                            className: f("Accordion__head"),
                            onClick: _,
                            children: [(0, c.jsx)("div", {
                                className: f("Accordion__title"),
                                children: (0, a.ZP)(s)
                            }), (0, c.jsx)("div", {
                                className: f("Accordion__btn"),
                                children: (0, c.jsx)(I, {})
                            })]
                        }), (0, c.jsx)("div", {
                            className: f("Accordion__bodyWrap"),
                            style: {
                                height: "".concat(u, "px")
                            },
                            children: (0, c.jsx)("div", {
                                className: f("Accordion__body"),
                                ref: d,
                                children: o
                            })
                        })]
                    })
                },
                j = s(32082),
                x = s.n(j),
                y = u().bind(x()),
                v = function () {
                    var t = (0, r.useState)(null),
                        e = t[0],
                        s = t[1],
                        o = (0, r.useState)([{
                            body: "",
                            referAddres: "",
                            img: "",
                            title: "",
                            number: "",
                            id: ""
                        }]),
                        n = o[0],
                        d = o[1];
                    return (0, r.useEffect)((function () {
                        var t = new URL(window.location.href);
                        fetch("/apiAdvantagesProductsBlock/?format=json&referAddres=".concat(t.pathname)).then((function (t) {
                            return t.json()
                        })).then((function (t) {
                            return d(t)
                        }))
                    }), []), (0, c.jsx)(c.Fragment, {
                        children: n == [] ? (0, c.jsx)(c.Fragment, {}) : (0, c.jsx)(c.Fragment, {
                            children: (0, c.jsx)("div", {
                                className: y("AdvantagesProductsBlock"),
                                children: (0, c.jsxs)(i.Z, {
                                    className: y("AdvantagesProductsBlock__container"),
                                    children: [(0, c.jsx)("div", {
                                        className: y("AdvantagesProductsBlock__titleWrap"),
                                        children: (0, c.jsx)(_.Z, {
                                            className: y("AdvantagesProductsBlock__title"),
                                            level: "2",
                                            children: "\u041a\u041e\u041d\u0421\u0422\u0420\u0423\u041a\u0422\u0418\u0412\u041d\u042b\u0415 \u041f\u0420\u0415\u0418\u041c\u0423\u0429\u0415\u0421\u0422\u0412\u0410"
                                        })
                                    }), (0, c.jsx)("div", {
                                        className: y("AdvantagesProductsBlock__contentWrap"),
                                        children: n.map((function (t) {
                                            return (0, c.jsxs)(h, {
                                                number: t.number,
                                                title: t.title,
                                                isOpened: e === t.id,
                                                onClick: function () {
                                                    return c = t.id, void s(e === c ? null : c);
                                                    var c
                                                },
                                                children: [(0, c.jsxs)("div", {
                                                    className: y("AdvantagesProductsBlock__descriptionItemWrapp"),
                                                    dangerouslySetInnerHTML: {
                                                        __html: t.body
                                                    },
                                                }), null == t.img ? (0, c.jsx)(c.Fragment, {}) : (0, c.jsx)("img", {
                                                    src: t.img
                                                })]
                                            }, t.id)
                                        }))
                                    })]
                                })
                            })
                        })
                    })
                },
                D = s(41664),
                b = s(69957),
                g = s(65897),
                w = s.n(g),
                k = u().bind(w()),
                C = function () {
                    var t = (0, r.useState)([{
                            adminupload: "",
                            title: "",
                            id: ""
                        }]),
                        e = t[0],
                        s = t[1];
                    return (0, r.useEffect)((function () {
                        var t = new URL(window.location.href);
                        fetch("/apiDockFilesList/?format=json&referAddres=".concat(t.pathname)).then((function (t) {
                            return t.json()
                        })).then((function (t) {
                            return s(t)
                        }))
                    }), []), (0, c.jsx)("div", {
                        className: k("DetailedProductsItem"),
                        children: (0, c.jsx)(i.Z, {
                            className: k("DetailedProductsItem__container"),
                            children: (0, c.jsx)("div", {
                                className: k("DetailedProductsItem__contentWrap"),
                                children: (0, c.jsxs)("div", {
                                    className: k("DetailedProductsItem__docsWrap"),
                                    children: [(0, c.jsx)("div", {
                                        className: k("DetailedProductsItem__docsTitle"),
                                        children: "\u0414\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u044b"
                                    }), (0, c.jsx)("div", {
                                        className: k("DetailedProductsItem__docsList"),
                                        children: e.map((function (t) {
                                            return (0, c.jsxs)("div", {
                                                className: k("DetailedProductsItem__docsItem"),
                                                children: [(0, c.jsx)("div", {
                                                    className: k("DetailedProductsItem__docsIconWrap"),
                                                    children: (0, c.jsxs)("svg", {
                                                        width: "24",
                                                        height: "24",
                                                        viewBox: "0 0 24 24",
                                                        fill: "none",
                                                        xmlns: "http://www.w3.org/2000/svg",
                                                        children: [(0, c.jsxs)("g", {
                                                            clipPath: "url(#clip0_865_12134)",
                                                            children: [(0, c.jsx)("path", {
                                                                d: "M20.9346 5.83087L15.3097 0.206016C15.1785 0.0746719 14.9993 0 14.8125 0H4.96875C3.80564 0 2.85938 0.946266 2.85938 2.10938V21.8906C2.85938 23.0537 3.80564 24 4.96875 24H19.0312C20.1944 24 21.1406 23.0537 21.1406 21.8906V6.32812C21.1406 6.13641 21.0601 5.95636 20.9346 5.83087ZM15.5156 2.40061L18.74 5.625H16.2188C15.831 5.625 15.5156 5.30958 15.5156 4.92188V2.40061ZM19.0312 22.5938H4.96875C4.58105 22.5938 4.26562 22.2783 4.26562 21.8906V2.10938C4.26562 1.72167 4.58105 1.40625 4.96875 1.40625H14.1094V4.92188C14.1094 6.08498 15.0556 7.03125 16.2188 7.03125H19.7344V21.8906C19.7344 22.2783 19.419 22.5938 19.0312 22.5938Z",
                                                                fill: "#03439A"
                                                            }), (0, c.jsx)("path", {
                                                                d: "M16.2188 9.9375H7.78125C7.39294 9.9375 7.07812 10.2523 7.07812 10.6406C7.07812 11.0289 7.39294 11.3438 7.78125 11.3438H16.2188C16.6071 11.3438 16.9219 11.0289 16.9219 10.6406C16.9219 10.2523 16.6071 9.9375 16.2188 9.9375Z",
                                                                fill: "#03439A"
                                                            }), (0, c.jsx)("path", {
                                                                d: "M16.2188 12.75H7.78125C7.39294 12.75 7.07812 13.0648 7.07812 13.4531C7.07812 13.8414 7.39294 14.1562 7.78125 14.1562H16.2188C16.6071 14.1562 16.9219 13.8414 16.9219 13.4531C16.9219 13.0648 16.6071 12.75 16.2188 12.75Z",
                                                                fill: "#03439A"
                                                            }), (0, c.jsx)("path", {
                                                                d: "M16.2188 15.5625H7.78125C7.39294 15.5625 7.07812 15.8773 7.07812 16.2656C7.07812 16.6539 7.39294 16.9688 7.78125 16.9688H16.2188C16.6071 16.9688 16.9219 16.6539 16.9219 16.2656C16.9219 15.8773 16.6071 15.5625 16.2188 15.5625Z",
                                                                fill: "#03439A"
                                                            }), (0, c.jsx)("path", {
                                                                d: "M13.4062 18.375H7.78125C7.39294 18.375 7.07812 18.6898 7.07812 19.0781C7.07812 19.4664 7.39294 19.7812 7.78125 19.7812H13.4062C13.7946 19.7812 14.1094 19.4664 14.1094 19.0781C14.1094 18.6898 13.7946 18.375 13.4062 18.375Z",
                                                                fill: "#03439A"
                                                            })]
                                                        }), (0, c.jsx)("defs", {
                                                            children: (0, c.jsx)("clipPath", {
                                                                id: "clip0_865_12134",
                                                                children: (0, c.jsx)("rect", {
                                                                    width: "24",
                                                                    height: "24",
                                                                    fill: "white"
                                                                })
                                                            })
                                                        })]
                                                    })
                                                }), (0, c.jsx)("div", {
                                                    className: k("DetailedProductsItem__docsItemTitleWrap"),
                                                    children: (0, c.jsx)("a", {
                                                        href: t.adminupload,
                                                        target: "_blank",
                                                        download: !0,
                                                        className: k("DetailedProductsItem__docsItemTitle"),
                                                        rel: "noreferrer",
                                                        children: t.title
                                                    })
                                                })]
                                            }, t.id)
                                        }))
                                    }), (0, c.jsx)(D.default, {
                                        href: " ",
                                        children: (0, c.jsx)("div", {
                                            className: k("DetailedProductsItem__buttonWrap"),
                                            children: (0, c.jsx)(b.Z, {
                                                theme: "wBorder",
                                                className: k("DetailedProductsItem__button"),
                                                children: "\u0417\u0410\u041f\u041e\u041b\u041d\u0418\u0422\u042c \u041e\u041f\u0420\u041e\u0421\u041d\u042b\u0419 \u041b\u0418\u0421\u0422"
                                            })
                                        })
                                    })]
                                })
                            })
                        })
                    })
                },
                W = s(82050),
                O = (s(68309), s(99523)),
                A = s.n(O),
                B = s(56041),
                M = s.n(B),
                T = u().bind(M()),
                L = function (t) {
                    var e = t.itemsList;
                    return (0, c.jsx)("div", {
                        className: T("NomenclatureProductsBlock__contentWrap"),
                        children: e.map((function (t) {
                            return (0, c.jsx)("div", {
                                className: T("NomenclatureProductsDiscItem__infoItem"),
                                children: t.item
                            }, t.id)
                        }))
                    })
                },
                S = u().bind(A()),
                Z = function () {
                    var t = (0, r.useState)([{
                            id: 1,
                            ref: "",
                            name: "",
                            nomenclatureDiscList: [{
                                id: 1,
                                NomenclatureProductsDiscFirst: 1,
                                item: ""
                            }]
                        }]),
                        e = t[0],
                        s = t[1];
                    (0, r.useEffect)((function () {
                        var t = new URL(window.location.href);
                        fetch("/apiNomenclatureProductsDiscFirstViewSet/?format=json&referAddres=".concat(t.pathname)).then((function (t) {
                            return t.json()
                        })).then((function (t) {
                            return s(t)
                        }))
                    }), []);
                    var o = (0, r.useState)([{
                            id: 1,
                            ref: "",
                            name: "",
                            nomenclatureDiscList: [{
                                id: 1,
                                NomenclatureProductsDiscFirst: 1,
                                item: ""
                            }]
                        }]),
                        n = o[0],
                        i = o[1];
                    return (0, r.useEffect)((function () {
                        var t = new URL(window.location.href);
                        fetch("/apiNomenclatureProductsDiscSecondViewSet/?format=json&referAddres=".concat(t.pathname)).then((function (t) {
                            return t.json()
                        })).then((function (t) {
                            return i(t)
                        }))
                    }), []), (0, c.jsxs)(c.Fragment, {
                        children: [e == [] ? (0, c.jsx)(c.Fragment, {}) : (0, c.jsx)(c.Fragment, {
                            children: (0, c.jsx)("div", {
                                className: S("NomenclatureProductsDiscItem"),
                                children: (0, c.jsx)("div", {
                                    className: S("NomenclatureProductsDiscItem__contentWrap"),
                                    children: (0, c.jsx)("div", {
                                        className: S("NomenclatureProductsDiscItem__infoWrap"),
                                        children: e.map((function (t) {
                                            return (0, c.jsxs)("div", {
                                                className: S("NomenclatureProductsDiscItem__info"),
                                                children: [(0, c.jsx)("div", {
                                                    className: S("NomenclatureProductsDiscItem__infoTitle"),
                                                    children: t.name
                                                }), (0, c.jsx)("div", {
                                                    className: S("NomenclatureProductsDiscItem__infoItemList"),
                                                    children: (0, c.jsx)(L, {
                                                        itemsList: t.nomenclatureDiscList
                                                    })
                                                })]
                                            }, t.id)
                                        }))
                                    })
                                })
                            })
                        }), n == [] ? (0, c.jsx)(c.Fragment, {}) : (0, c.jsx)(c.Fragment, {
                            children: (0, c.jsx)("div", {
                                className: S("NomenclatureProductsDiscItem"),
                                children: (0, c.jsx)("div", {
                                    className: S("NomenclatureProductsDiscItem__contentWrap"),
                                    children: (0, c.jsx)("div", {
                                        className: S("NomenclatureProductsDiscItem__infoWrap"),
                                        children: n.map((function (t) {
                                            return (0, c.jsxs)("div", {
                                                className: S("NomenclatureProductsDiscItem__info"),
                                                children: [(0, c.jsx)("div", {
                                                    className: S("NomenclatureProductsDiscItem__infoTitle"),
                                                    children: t.name
                                                }), (0, c.jsx)("div", {
                                                    className: S("NomenclatureProductsDiscItem__infoItemList"),
                                                    children: (0, c.jsx)(L, {
                                                        itemsList: t.nomenclatureDiscList
                                                    })
                                                })]
                                            }, t.id)
                                        }))
                                    })
                                })
                            })
                        })]
                    })
                },
                F = s(81095),
                H = s(70818),
                E = s.n(H),
                z = s(95882),
                U = s.n(z),
                R = u().bind(U()),
                V = function () {
                    var t = (0, r.useState)([{
                            disc: "",
                            optionsproductsblock_ref: "",
                            id: 1
                        }]),
                        e = t[0],
                        s = t[1];
                    return (0, r.useEffect)((function () {
                        var t = new URL(window.location.href);
                        fetch("/apiOptionsProductsBlock/?format=json&referAddres=".concat(t.pathname)).then((function (t) {
                            return t.json()
                        })).then((function (t) {
                            return s(t)
                        }))
                    }), []), (0, c.jsx)(c.Fragment, {
                        children: e == [] ? (0, c.jsx)(c.Fragment, {}) : (0, c.jsx)(c.Fragment, {
                            children: (0, c.jsx)("div", {
                                className: R("OptionsProductsBlock"),
                                children: (0, c.jsxs)(i.Z, {
                                    className: R("OptionsProductsBlock__container"),
                                    children: [(0, c.jsx)("div", {
                                        className: R("OptionsProductsBlock__titleWrap"),
                                        children: (0, c.jsx)(_.Z, {
                                            className: R("OptionsProductsBlock__title"),
                                            level: "2",
                                            children: "\u0412\u041e\u0417\u041c\u041e\u0416\u041d\u042b\u0415 \u041e\u041f\u0426\u0418\u0418"
                                        })
                                    }), (0, c.jsx)("div", {
                                        className: R("OptionsProductsBlock__contentWrap"),
                                        children: e.map((function (t) {
                                            return (0, c.jsx)("div", {
                                                className: R("OptionsProductsBlock__content"),
                                                children: (0, c.jsx)("div", {
                                                    className: R("OptionsProductsBlock__subtitle"),
                                                    children: t.disc
                                                })
                                            }, t.id)
                                        }))
                                    })]
                                })
                            })
                        })
                    })
                },
                X = s(61136),
                q = s.n(X),
                K = s(10075),
                J = u().bind(q()),
                G = function () {
                    var t = (0, r.useState)([{
                            name: "",
                            disc: "",
                            img: "",
                            referAddres: ""
                        }]),
                        e = t[0],
                        s = t[1];
                    return (0, r.useEffect)((function () {
                        var t = new URL(window.location.href);
                        console.log(t.pathname), fetch("/apimainProductList/?format=json&referAddres=".concat(t.pathname)).then((function (t) {
                            return t.json()
                        })).then((function (t) {
                            return s(t)
                        }))
                    }), []), (0, c.jsx)(c.Fragment, {
                        children: e == [] ? (0, c.jsx)(c.Fragment, {}) : (0, c.jsxs)(c.Fragment, {
                            children: [(0, c.jsxs)("a", {
                                href: "/products",
                                className: J("DetailedProductsItem__arrowWrap"),
                                children: [(0, c.jsx)(K.Z, {
                                    className: J("DetailedProductsItem__arrow", "DetailedProductsItem__arrow_prev"),
                                    type: "left"
                                }), (0, c.jsx)("div", {
                                    className: J("DetailedProductsItem__breadcrumbText"),
                                    children: "\u0421\u0435\u0440\u0438\u0439\u043d\u0430\u044f \u043f\u0440\u043e\u0434\u0443\u043a\u0446\u0438\u044f"
                                })]
                            }), (0, c.jsxs)("div", {
                                className: J("MainProductsBlock__item"),
                                children: [(0, c.jsxs)("div", {
                                    className: J("MainProductsItem__infoWrap"),
                                    children: [(0, c.jsx)("h2", {
                                        className: J("MainProductsItem__productName"),
                                        children: e[0].name
                                    }), (0, c.jsx)("div", {
                                        className: J("MainProductsItem__productUsage"),
                                        children: "\u041f\u0440\u0438\u043c\u0435\u043d\u0435\u043d\u0438\u0435"
                                    }), (0, c.jsx)("div", {
                                        className: J("MainProductsItem__productUsages"),
                                        children: e[0].disc
                                    })]
                                }), (0, c.jsx)("div", 
                                {
                                    className: J("MainProductsItem__imageWrap"),
                                    children: (0, c.jsx)("img", {
                                        src: e[0].img,
                                        onMouseOver: function (i) {
                                            return i.currentTarget.src = e[0].img2
                                        },
                                        onMouseOut: function (i) {
                                            return i.currentTarget.src = e[0].img
                                        }
                                    })
                                }
                                )]
                            })]
                        })
                    })
                },
                Q = s(60450),
                Y = s.n(Q),
                $ = u().bind(Y()),
                tt = function (t) {
                    var e = t.className,
                        s = t.title,
                        o = t.setSelectedTab,
                        n = t.index,
                        i = (0, r.useCallback)((function () {
                            o(n)
                        }), [o, n]);
                    return (0, c.jsx)(b.Z, {
                        theme: "noBorder",
                        className: $("TabTitle", e),
                        onClick: i,
                        children: s
                    })
                },
                et = s(1937),
                st = s.n(et),
                ct = u().bind(st()),
                rt = function (t) {
                    var e = t.className,
                        s = t.children,
                        o = (0, r.useState)(0),
                        n = o[0],
                        i = o[1];
                    return (0, c.jsxs)("div", {
                        className: ct("Tabs", e),
                        children: [(0, c.jsx)("div", {
                            className: ct("Tabs__wrapper"),
                            children: s.map((function (t, e) {
                                return (0, c.jsx)(tt, {
                                    title: t.props.title,
                                    index: e,
                                    setSelectedTab: i,
                                    className: ct(n == e ? "TabTitle_active" : "TabTitle_inactive")
                                }, e)
                            }))
                        }), s[n]]
                    })
                },
                ot = s(25910),
                nt = s.n(ot),
                it = u().bind(nt()),
                _t = function (t) {
                    var e = t.className,
                        s = t.children;
                    return (0, c.jsx)("div", {
                        className: it("Tab", e),
                        children: s
                    })
                },
                dt = s(23621),
                at = s.n(dt),
                lt = u().bind(at()),
                ut = function (t) {
                    var e = t.productName,
                        s = t.productSize,
                        r = t.productSizes,
                        o = t.productPressure,
                        n = t.productPressures,
                        i = t.productConnection,
                        d = t.productConnections;
                    return (0, c.jsx)("div", {
                        className: lt("NomenclatureProductsItem"),
                        children: (0, c.jsx)("div", {
                            className: lt("NomenclatureProductsItem__contentWrap"),
                            children: (0, c.jsxs)("div", {
                                className: lt("NomenclatureProductsItem__infoWrap"),
                                children: [(0, c.jsx)("div", {
                                    className: lt("NomenclatureProductsItem__content"),
                                    children: (0, c.jsx)(_.Z, {
                                        className: lt("NomenclatureProductsItem__productTitle"),
                                        level: "2",
                                        children: e
                                    })
                                }), (0, c.jsxs)("div", {
                                    className: lt("NomenclatureProductsItem__content"),
                                    children: [(0, c.jsxs)("div", {
                                        className: lt("NomenclatureProductsItem__productCell"),
                                        children: [(0, c.jsx)("div", {
                                            className: lt("NomenclatureProductsItem__productItem", "NomenclatureProductsItem__productSize"),
                                            children: s
                                        }), (0, c.jsx)("div", {
                                            className: lt("NomenclatureProductsItem__productSizes"),
                                            children: r
                                        })]
                                    }), (0, c.jsxs)("div", {
                                        className: lt("NomenclatureProductsItem__productCell"),
                                        children: [(0, c.jsx)("div", {
                                            className: lt("NomenclatureProductsItem__productItem", "NomenclatureProductsItem__productPressure"),
                                            children: o
                                        }), (0, c.jsx)("div", {
                                            className: lt("NomenclatureProductsItem__productPressures"),
                                            children: n
                                        })]
                                    }), (0, c.jsxs)("div", {
                                        className: lt("NomenclatureProductsItem__productCell"),
                                        children: [(0, c.jsx)("div", {
                                            className: lt("NomenclatureProductsItem__productItem", "NomenclatureProductsItem__productConnection"),
                                            children: i
                                        }), (0, c.jsx)("div", {
                                            className: lt("NomenclatureProductsItem__productConnectionWrap"),
                                            children: d.map((function (t) {
                                                return (0, c.jsx)("div", {
                                                    className: lt("NomenclatureProductsItem__productConnections", void 0 == t ? "NomenclatureProductsItem__productConnectionsNone" : "NomenclatureProductsItem__productConnections"),
                                                    children: t.item
                                                }, t.id)
                                            }))
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                mt = s(28738),
                pt = s.n(mt);

            function Pt(t, e) {
                var s = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var c = Object.getOwnPropertySymbols(t);
                    e && (c = c.filter((function (e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    }))), s.push.apply(s, c)
                }
                return s
            }

            function Nt(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var s = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? Pt(Object(s), !0).forEach((function (e) {
                        (0, d.Z)(t, e, s[e])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(s)) : Pt(Object(s)).forEach((function (e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(s, e))
                    }))
                }
                return t
            }
            var It = u().bind(pt()),
                ft = function () {
                    var t = (0, r.useState)([{
                            id: 1,
                            referAddres: "/",
                            productConnection: "",
                            productConnections: [{
                                id: 1,
                                NomenclatureProductsBlock: 1,
                                item: ""
                            }],
                            productName: "",
                            productPressure: "",
                            productPressures: "",
                            productSize: "",
                            productSizes: "",
                            titleArray: ""
                        }]),
                        e = t[0],
                        s = t[1];
                    return (0, r.useEffect)((function () {
                        var t = new URL(window.location.href);
                        fetch("/apiNomenclatureProductsBlock/?format=json&referAddres=".concat(t.pathname)).then((function (t) {
                            return t.json()
                        })).then((function (t) {
                            return s(t)
                        }))
                    }), []), (0, c.jsx)(c.Fragment, {
                        children: e == [] ? (0, c.jsx)(c.Fragment, {}) : (0, c.jsx)(c.Fragment, {
                            children: (0, c.jsx)("div", {
                                className: It("NomenclatureProductsBlock"),
                                children: (0, c.jsxs)(i.Z, {
                                    className: It("NomenclatureProductsBlock__container"),
                                    children: [(0, c.jsx)("div", {
                                        className: It("NomenclatureProductsBlock__titleWrap"),
                                        children: (0, c.jsx)(_.Z, {
                                            className: It("NomenclatureProductsBlock__title"),
                                            level: "2",
                                            children: "\u041d\u043e\u043c\u0435\u043d\u043a\u043b\u0430\u0442\u0443\u0440\u0430"
                                        })
                                    }), (0, c.jsx)("div", {
                                        className: It("NomenclatureProductsBlock__contentWrap"),
                                        children: (0, c.jsx)(rt, {
                                            className: It("NomenclatureProductsBlock__tabsWrap"),
                                            children: e.map((function (t) {
                                                return (0, c.jsx)(_t, {
                                                    title: t.titleArray,
                                                    children: (0, c.jsx)(ut, Nt({}, t))
                                                }, t.id)
                                            }))
                                        })
                                    })]
                                })
                            })
                        })
                    })
                },
                ht = u().bind(E());

            function jt() {
                return (0, c.jsxs)("div", {
                    className: E().container,
                    children: [(0, c.jsx)(o.default, {
                        children: (0, c.jsx)("title", {
                            children: F.TP.title
                        })
                    }), (0, c.jsxs)(n.Z, {
                        className: ht("MainPage"),
                        children: [(0, c.jsx)(G, {}), (0, c.jsx)(ft, {}), (0, c.jsx)(Z, {}), (0, c.jsx)(V, {}), (0, c.jsx)(v, {}), (0, c.jsx)(C, {}), (0, c.jsx)(W.Z, {
                            title: F.er.title,
                            titleDescription: "",
                            button: []
                        })]
                    })]
                })
            }
        },
        50493: function (t, e, s) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/products/detail_product", function () {
                return s(59927)
            }])
        },
        75764: function (t) {
            t.exports = {
                Accordion: "styles_Accordion__q2j1K",
                Accordion__head: "styles_Accordion__head__1dZnL",
                Accordion__number: "styles_Accordion__number__myttw",
                Accordion__title: "styles_Accordion__title__3BAs3",
                Accordion__btn: "styles_Accordion__btn__DPTmW",
                Accordion__bodyWrap: "styles_Accordion__bodyWrap__2GuZs",
                Accordion__body: "styles_Accordion__body__3n6bN",
                Accordion_opened: "styles_Accordion_opened__3uh2j"
            }
        },
        97085: function (t) {
            t.exports = {
                Arrow: "styles_Arrow__1qcSu",
                Arrow_disabled: "styles_Arrow_disabled__2lel5",
                Arrow_white: "styles_Arrow_white__s62dv",
                Arrow_blue: "styles_Arrow_blue__2rF_U"
            }
        },
        25910: function (t) {
            t.exports = {
                Container: "styles_Container__2Whuh"
            }
        },
        60450: function (t) {
            t.exports = {
                TabTitle: "styles_TabTitle__3d4gg",
                TabTitle_inactive: "styles_TabTitle_inactive__2MELZ",
                TabTitle_active: "styles_TabTitle_active__GhP2H"
            }
        },
        1937: function (t) {
            t.exports = {
                Tabs__wrapper: "styles_Tabs__wrapper__3zyZP"
            }
        },
        32082: function (t) {
            t.exports = {
                AdvantagesProductsBlock: "styles_AdvantagesProductsBlock__peUgT",
                AdvantagesProductsBlock__titleWrap: "styles_AdvantagesProductsBlock__titleWrap__2VH7J",
                AdvantagesProductsBlock__descriptionItemWrapp: "styles_AdvantagesProductsBlock__descriptionItemWrapp__1hAZJ",
                AdvantagesProductsBlock__descriptionItem: "styles_AdvantagesProductsBlock__descriptionItem__3VBQL"
            }
        },
        65897: function (t) {
            t.exports = {
                DetailedProductsItem: "styles_DetailedProductsItem__1D0JA",
                DetailedProductsItem__docsWrap: "styles_DetailedProductsItem__docsWrap__35A_X",
                DetailedProductsItem__docsTitle: "styles_DetailedProductsItem__docsTitle__bN3c7",
                DetailedProductsItem__docsList: "styles_DetailedProductsItem__docsList__XcWMs",
                DetailedProductsItem__docsItem: "styles_DetailedProductsItem__docsItem__1q-nb",
                DetailedProductsItem__docsItemTitleWrap: "styles_DetailedProductsItem__docsItemTitleWrap__1l8O9",
                DetailedProductsItem__docsItemTitle: "styles_DetailedProductsItem__docsItemTitle__17LbD",
                DetailedProductsItem__docsSize: "styles_DetailedProductsItem__docsSize__XKs9N",
                DetailedProductsItem__buttonWrap: "styles_DetailedProductsItem__buttonWrap__sUqN4",
                DetailedProductsItem__button: "styles_DetailedProductsItem__button__3_zQz"
            }
        },
        99523: function (t) {
            t.exports = {
                NomenclatureProductsDiscItem: "styles_NomenclatureProductsDiscItem__1fBCS",
                NomenclatureProductsDiscItem__productTitle: "styles_NomenclatureProductsDiscItem__productTitle__3NB1O",
                NomenclatureProductsDiscItem__productConnectionWrap: "styles_NomenclatureProductsDiscItem__productConnectionWrap__2-Ksm",
                NomenclatureProductsDiscItem__infoWrap: "styles_NomenclatureProductsDiscItem__infoWrap__X2oq5",
                NomenclatureProductsDiscItem__info: "styles_NomenclatureProductsDiscItem__info__1JI5y",
                NomenclatureProductsDiscItem__infoTitle: "styles_NomenclatureProductsDiscItem__infoTitle__120h_",
                NomenclatureProductsDiscItem__infoDescription: "styles_NomenclatureProductsDiscItem__infoDescription__33a_m",
                NomenclatureProductsDiscItem__infoItemList: "styles_NomenclatureProductsDiscItem__infoItemList__mITom",
                NomenclatureProductsDiscItem__infoItem: "styles_NomenclatureProductsDiscItem__infoItem__6GwfD",
                NomenclatureProductsDiscItem__content: "styles_NomenclatureProductsDiscItem__content__cLX1v",
                NomenclatureProductsDiscItem__productCell: "styles_NomenclatureProductsDiscItem__productCell__iXdH0",
                NomenclatureProductsDiscItem__productConnections: "styles_NomenclatureProductsDiscItem__productConnections__2fRoc",
                NomenclatureProductsDiscItem__productItem: "styles_NomenclatureProductsDiscItem__productItem__1ZcfP"
            }
        },
        28738: function (t) {
            t.exports = {
                NomenclatureProductsBlock: "styles_NomenclatureProductsBlock__PVj44",
                NomenclatureProductsBlock__titleWrap: "styles_NomenclatureProductsBlock__titleWrap__2g1CT",
                NomenclatureProductsBlock__content: "styles_NomenclatureProductsBlock__content__HQJI1",
                NomenclatureProductsBlock__title: "styles_NomenclatureProductsBlock__title__fzqm5"
            }
        },
        56041: function (t) {
            t.exports = {
                NomenclatureProductsDiscItem: "styles_NomenclatureProductsDiscItem__2zD0k",
                NomenclatureProductsDiscItem__productTitle: "styles_NomenclatureProductsDiscItem__productTitle__DGRzr",
                NomenclatureProductsDiscItem__productConnectionWrap: "styles_NomenclatureProductsDiscItem__productConnectionWrap__dNas_",
                NomenclatureProductsDiscItem__infoWrap: "styles_NomenclatureProductsDiscItem__infoWrap__2WneC",
                NomenclatureProductsDiscItem__info: "styles_NomenclatureProductsDiscItem__info__1MHRE",
                NomenclatureProductsDiscItem__infoTitle: "styles_NomenclatureProductsDiscItem__infoTitle__jzRXp",
                NomenclatureProductsDiscItem__infoDescription: "styles_NomenclatureProductsDiscItem__infoDescription__3j7N-",
                NomenclatureProductsDiscItem__infoItemList: "styles_NomenclatureProductsDiscItem__infoItemList__1s_Hb",
                NomenclatureProductsDiscItem__infoItem: "styles_NomenclatureProductsDiscItem__infoItem__FI7KS",
                NomenclatureProductsDiscItem__content: "styles_NomenclatureProductsDiscItem__content__3EXS-",
                NomenclatureProductsDiscItem__productCell: "styles_NomenclatureProductsDiscItem__productCell__2CHik",
                NomenclatureProductsDiscItem__productConnections: "styles_NomenclatureProductsDiscItem__productConnections__2qGqs",
                NomenclatureProductsDiscItem__productItem: "styles_NomenclatureProductsDiscItem__productItem__14CrD"
            }
        },
        23621: function (t) {
            t.exports = {
                NomenclatureProductsItem: "styles_NomenclatureProductsItem__2KtFa",
                NomenclatureProductsItem__productTitle: "styles_NomenclatureProductsItem__productTitle__1mvXw",
                NomenclatureProductsItem__productConnectionWrap: "styles_NomenclatureProductsItem__productConnectionWrap__1Swmb",
                NomenclatureProductsItem__infoWrap: "styles_NomenclatureProductsItem__infoWrap__2DJPk",
                NomenclatureProductsItem__info: "styles_NomenclatureProductsItem__info__2wbje",
                NomenclatureProductsItem__infoTitle: "styles_NomenclatureProductsItem__infoTitle__19iQ9",
                NomenclatureProductsItem__infoDescription: "styles_NomenclatureProductsItem__infoDescription__1tGqT",
                NomenclatureProductsItem__infoItemList: "styles_NomenclatureProductsItem__infoItemList__3dQI7",
                NomenclatureProductsItem__infoItem: "styles_NomenclatureProductsItem__infoItem__2-etb",
                NomenclatureProductsItem__content: "styles_NomenclatureProductsItem__content__2nwwX",
                NomenclatureProductsItem__productCell: "styles_NomenclatureProductsItem__productCell__mKraD",
                NomenclatureProductsItem__productConnections: "styles_NomenclatureProductsItem__productConnections__1EC9K",
                NomenclatureProductsItem__productItem: "styles_NomenclatureProductsItem__productItem__1KsDK",
                NomenclatureProductsDiscItem: "styles_NomenclatureProductsDiscItem__3uy-a",
                NomenclatureProductsDiscItem__infoWrap: "styles_NomenclatureProductsDiscItem__infoWrap__18NIq"
            }
        },
        95882: function (t) {
            t.exports = {
                OptionsProductsBlock: "styles_OptionsProductsBlock__1Cpiz",
                OptionsProductsBlock__titleWrap: "styles_OptionsProductsBlock__titleWrap__3lYia",
                OptionsProductsBlock__contentWrap: "styles_OptionsProductsBlock__contentWrap__2-aze",
                OptionsProductsBlock__content: "styles_OptionsProductsBlock__content__2StVj",
                OptionsProductsBlock__subtitle: "styles_OptionsProductsBlock__subtitle__11ypR",
                OptionsProductsBlock__container: "styles_OptionsProductsBlock__container__3_tbc"
            }
        },
        61136: function (t) {
            t.exports = {
                MainProductsBlock__item: "styles_MainProductsBlock__item__3nzr_",
                MainPage: "styles_MainPage__3xBYx",
                DetailedProductsItem__arrowWrap: "styles_DetailedProductsItem__arrowWrap__Wvfpc",
                DetailedProductsItem__breadcrumbText: "styles_DetailedProductsItem__breadcrumbText__1LRWB",
                MainProductsItem__item: "styles_MainProductsItem__item__35MM-",
                MainProductsItem__infoWrap: "styles_MainProductsItem__infoWrap__2hTZ-",
                MainProductsItem__productId: "styles_MainProductsItem__productId__16iFh",
                MainProductsItem__productName: "styles_MainProductsItem__productName__3NrXs",
                MainProductsItem__productUsage: "styles_MainProductsItem__productUsage__3xNOs",
                MainProductsItem__productUsages: "styles_MainProductsItem__productUsages__HArPH",
                MainProductsItem__productType: "styles_MainProductsItem__productType__2_bIh",
                MainProductsItem__productTypes: "styles_MainProductsItem__productTypes__N3k2P",
                MainProductsItem__productTypesNone: "styles_MainProductsItem__productTypesNone__2n9CJ",
                MainProductsItem__productStructure: "styles_MainProductsItem__productStructure__RmD4R",
                MainProductsItem__productStructureWrap: "styles_MainProductsItem__productStructureWrap__31Fzi",
                MainProductsItem__productStructures: "styles_MainProductsItem__productStructures__1aJVC",
                MainProductsItem__productStructuresNone: "styles_MainProductsItem__productStructuresNone__A1UsM",
                MainProductsItem__imageWrap: "styles_MainProductsItem__imageWrap__eOX-V",
                MainProductsItem__imageWrap__product1: "styles_MainProductsItem__imageWrap__product1__HUFCD",
                MainProductsItem__imageWrap__product2: "styles_MainProductsItem__imageWrap__product2__2UlTy",
                MainProductsItem__imageWrap__product3: "styles_MainProductsItem__imageWrap__product3__1-sxV",
                MainProductsItem__imageWrap__product4: "styles_MainProductsItem__imageWrap__product4__3utR6",
                MainProductsItem__buttonWrap: "styles_MainProductsItem__buttonWrap__1yB2B",
                MainProductsItem__button: "styles_MainProductsItem__button__1oA4S"
            }
        },
        70818: function (t) {
            t.exports = {
                MainPage: "styles_MainPage__2krQb"
            }
        }
    },
    function (t) {
        t.O(0, [663, 334, 653, 146, 308, 774, 888, 179], (function () {
            return e = 50493, t(t.s = e);
            var e
        }));
        var e = t.O();
        _N_E = e
    }
]);