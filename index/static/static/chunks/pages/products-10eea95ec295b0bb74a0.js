(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [345], {
        23646: function (t, e, r) {
            var n = r(67228);
            t.exports = function (t) {
                if (Array.isArray(t)) return n(t)
            }
        },
        59713: function (t) {
            t.exports = function (t, e, r) {
                return e in t ? Object.defineProperty(t, e, {
                    value: r,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[e] = r, t
            }
        },
        46860: function (t) {
            t.exports = function (t) {
                if ("undefined" !== typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t)
            }
        },
        98206: function (t) {
            t.exports = function () {
                throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
            }
        },
        319: function (t, e, r) {
            var n = r(23646),
                o = r(46860),
                a = r(60379),
                c = r(98206);
            t.exports = function (t) {
                return n(t) || o(t) || a(t) || c()
            }
        },
        53671: function (t, e, r) {
            var n = r(17854),
                o = r(19662),
                a = r(47908),
                c = r(68361),
                s = r(26244),
                i = n.TypeError,
                u = function (t) {
                    return function (e, r, n, u) {
                        o(r);
                        var d = a(e),
                            _ = c(d),
                            l = s(d),
                            p = t ? l - 1 : 0,
                            f = t ? -1 : 1;
                        if (n < 2)
                            for (;;) {
                                if (p in _) {
                                    u = _[p], p += f;
                                    break
                                }
                                if (p += f, t ? p < 0 : l <= p) throw i("Reduce of empty array with no initial value")
                            }
                        for (; t ? p >= 0 : l > p; p += f) p in _ && (u = r(u, _[p], p, d));
                        return u
                    }
                };
            t.exports = {
                left: u(!1),
                right: u(!0)
            }
        },
        85827: function (t, e, r) {
            "use strict";
            var n = r(82109),
                o = r(53671).left,
                a = r(9341),
                c = r(7392),
                s = r(35268);
            n({
                target: "Array",
                proto: !0,
                forced: !a("reduce") || !s && c > 79 && c < 83
            }, {
                reduce: function (t) {
                    var e = arguments.length;
                    return o(this, t, e, e > 1 ? arguments[1] : void 0)
                }
            })
        },
        65069: function (t, e, r) {
            "use strict";
            var n = r(82109),
                o = r(1702),
                a = r(43157),
                c = o([].reverse),
                s = [1, 2];
            n({
                target: "Array",
                proto: !0,
                forced: String(s) === String(s.reverse())
            }, {
                reverse: function () {
                    return a(this) && (this.length = this.length), c(this)
                }
            })
        },
        69957: function (t, e, r) {
            "use strict";
            r(24812);
            var n = r(85893),
                o = (r(67294), r(47166)),
                a = r.n(o),
                c = r(94328),
                s = r.n(c),
                i = a().bind(s());
            e.Z = function (t) {
                var e = t.theme,
                    r = void 0 === e ? "wBorder" : e,
                    o = t.onClick,
                    a = t.className,
                    c = t.children,
                    s = t.download;
                return s ? (0, n.jsx)("a", {
                    href: s,
                    className: i("Button", "Button_".concat(r), a),
                    onClick: o,
                    download: !0,
                    target: "_blank",
                    rel: "noreferrer",
                    children: c
                }) : (0, n.jsx)("button", {
                    className: i("Button", "Button_".concat(r), a),
                    onClick: o,
                    children: c
                })
            }
        },
        23398: function (t, e, r) {
            "use strict";
            var n;
            e.__esModule = !0, e.AmpStateContext = void 0;
            var o = ((n = r(67294)) && n.__esModule ? n : {
                default: n
            }).default.createContext({});
            e.AmpStateContext = o
        },
        76393: function (t, e, r) {
            "use strict";
            e.__esModule = !0, e.isInAmpMode = c, e.useAmp = function () {
                return c(o.default.useContext(a.AmpStateContext))
            };
            var n, o = (n = r(67294)) && n.__esModule ? n : {
                    default: n
                },
                a = r(23398);

            function c() {
                var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                    e = t.ampFirst,
                    r = void 0 !== e && e,
                    n = t.hybrid,
                    o = void 0 !== n && n,
                    a = t.hasQuery,
                    c = void 0 !== a && a;
                return r || o && c
            }
        },
        92775: function (t, e, r) {
            "use strict";
            r(92222), r(57327), r(82772), r(66992), r(21249), r(85827), r(65069), r(47042), r(5212), r(69070), r(38880), r(41539), r(70189), r(78783), r(23157), r(4129), r(88921), r(96248), r(13599), r(11477), r(64362), r(15389), r(90401), r(45164), r(91238), r(54837), r(87485), r(56767), r(76651), r(61437), r(35285), r(39865), r(78206), r(33948);
            var n = r(59713);

            function o(t, e) {
                var r = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter((function (e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    }))), r.push.apply(r, n)
                }
                return r
            }
            e.__esModule = !0, e.defaultHead = l, e.default = void 0;
            var a, c = function (t) {
                    if (t && t.__esModule) return t;
                    if (null === t || "object" !== typeof t && "function" !== typeof t) return {
                        default: t
                    };
                    var e = _();
                    if (e && e.has(t)) return e.get(t);
                    var r = {},
                        n = Object.defineProperty && Object.getOwnPropertyDescriptor;
                    for (var o in t)
                        if (Object.prototype.hasOwnProperty.call(t, o)) {
                            var a = n ? Object.getOwnPropertyDescriptor(t, o) : null;
                            a && (a.get || a.set) ? Object.defineProperty(r, o, a) : r[o] = t[o]
                        } r.default = t, e && e.set(t, r);
                    return r
                }(r(67294)),
                s = (a = r(73244)) && a.__esModule ? a : {
                    default: a
                },
                i = r(23398),
                u = r(41165),
                d = r(76393);

            function _() {
                if ("function" !== typeof WeakMap) return null;
                var t = new WeakMap;
                return _ = function () {
                    return t
                }, t
            }

            function l() {
                var t = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
                    e = [c.default.createElement("meta", {
                        charSet: "utf-8"
                    })];
                return t || e.push(c.default.createElement("meta", {
                    name: "viewport",
                    content: "width=device-width"
                })), e
            }

            function p(t, e) {
                return "string" === typeof e || "number" === typeof e ? t : e.type === c.default.Fragment ? t.concat(c.default.Children.toArray(e.props.children).reduce((function (t, e) {
                    return "string" === typeof e || "number" === typeof e ? t : t.concat(e)
                }), [])) : t.concat(e)
            }
            var f = ["name", "httpEquiv", "charSet", "itemProp"];

            function m(t, e) {
                return t.reduce((function (t, e) {
                    var r = c.default.Children.toArray(e.props.children);
                    return t.concat(r)
                }), []).reduce(p, []).reverse().concat(l(e.inAmpMode)).filter(function () {
                    var t = new Set,
                        e = new Set,
                        r = new Set,
                        n = {};
                    return function (o) {
                        var a = !0,
                            c = !1;
                        if (o.key && "number" !== typeof o.key && o.key.indexOf("$") > 0) {
                            c = !0;
                            var s = o.key.slice(o.key.indexOf("$") + 1);
                            t.has(s) ? a = !1 : t.add(s)
                        }
                        switch (o.type) {
                            case "title":
                            case "base":
                                e.has(o.type) ? a = !1 : e.add(o.type);
                                break;
                            case "meta":
                                for (var i = 0, u = f.length; i < u; i++) {
                                    var d = f[i];
                                    if (o.props.hasOwnProperty(d))
                                        if ("charSet" === d) r.has(d) ? a = !1 : r.add(d);
                                        else {
                                            var _ = o.props[d],
                                                l = n[d] || new Set;
                                            "name" === d && c || !l.has(_) ? (l.add(_), n[d] = l) : a = !1
                                        }
                                }
                        }
                        return a
                    }
                }()).reverse().map((function (t, r) {
                    var a = t.key || r;
                    if (!e.inAmpMode && "link" === t.type && t.props.href && ["https://fonts.googleapis.com/css", "https://use.typekit.net/"].some((function (e) {
                            return t.props.href.startsWith(e)
                        }))) {
                        var s = function (t) {
                            for (var e = 1; e < arguments.length; e++) {
                                var r = null != arguments[e] ? arguments[e] : {};
                                e % 2 ? o(Object(r), !0).forEach((function (e) {
                                    n(t, e, r[e])
                                })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(r)) : o(Object(r)).forEach((function (e) {
                                    Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(r, e))
                                }))
                            }
                            return t
                        }({}, t.props || {});
                        return s["data-href"] = s.href, s.href = void 0, s["data-optimized-fonts"] = !0, c.default.cloneElement(t, s)
                    }
                    return c.default.cloneElement(t, {
                        key: a
                    })
                }))
            }
            var h = function (t) {
                var e = t.children,
                    r = (0, c.useContext)(i.AmpStateContext),
                    n = (0, c.useContext)(u.HeadManagerContext);
                return c.default.createElement(s.default, {
                    reduceComponentsToState: m,
                    headManager: n,
                    inAmpMode: (0, d.isInAmpMode)(r)
                }, e)
            };
            e.default = h
        },
        73244: function (t, e, r) {
            "use strict";
            var n = r(319),
                o = r(34575),
                a = r(93913),
                c = (r(81506), r(2205)),
                s = r(78585),
                i = r(29754);

            function u(t) {
                var e = function () {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Date.prototype.toString.call(Reflect.construct(Date, [], (function () {}))), !0
                    } catch (t) {
                        return !1
                    }
                }();
                return function () {
                    var r, n = i(t);
                    if (e) {
                        var o = i(this).constructor;
                        r = Reflect.construct(n, arguments, o)
                    } else r = n.apply(this, arguments);
                    return s(this, r)
                }
            }
            e.__esModule = !0, e.default = void 0;
            var d = r(67294),
                _ = function (t) {
                    c(r, t);
                    var e = u(r);

                    function r(t) {
                        var a;
                        return o(this, r), (a = e.call(this, t))._hasHeadManager = void 0, a.emitChange = function () {
                            a._hasHeadManager && a.props.headManager.updateHead(a.props.reduceComponentsToState(n(a.props.headManager.mountedInstances), a.props))
                        }, a._hasHeadManager = a.props.headManager && a.props.headManager.mountedInstances, a
                    }
                    return a(r, [{
                        key: "componentDidMount",
                        value: function () {
                            this._hasHeadManager && this.props.headManager.mountedInstances.add(this), this.emitChange()
                        }
                    }, {
                        key: "componentDidUpdate",
                        value: function () {
                            this.emitChange()
                        }
                    }, {
                        key: "componentWillUnmount",
                        value: function () {
                            this._hasHeadManager && this.props.headManager.mountedInstances.delete(this), this.emitChange()
                        }
                    }, {
                        key: "render",
                        value: function () {
                            return null
                        }
                    }]), r
                }(d.Component);
            e.default = _
        },
        57684: function (t, e, r) {
            "use strict";
            r.r(e), r.d(e, {
                default: function () {
                    return k
                }
            });
            r(24812);
            var n = r(85893),
                o = r(67294),
                a = r(9008),
                c = r(14146),
                s = (r(21249), r(41539), r(88674), r(96156)),
                i = r(15027),
                u = (r(68309), r(47166)),
                d = r.n(u),
                _ = r(702),
                l = r.n(_),
                p = r(41664),
                f = r(69957),
                m = d().bind(l()),
                h = function (t) {
                    var e = t.referAddres,
                        r = t.name,
                        o = t.disc,
                        a = t.img;
                    return (0, n.jsxs)("div", {
                        className: m("MainProductsItem__item"),
                        children: [(0, n.jsxs)("div", {
                            className: m("MainProductsItem__infoWrap"),
                            children: [(0, n.jsx)("h2", {
                                className: m("MainProductsItem__productName"),
                                children: r
                            }), (0, n.jsx)("div", {
                                className: m("MainProductsItem__productUsage"),
                                children: "\u041f\u0440\u0438\u043c\u0435\u043d\u0435\u043d\u0438\u0435"
                            }), (0, n.jsx)("div", {
                                className: m("MainProductsItem__productUsages"),
                                children: o
                            }), (0, n.jsx)("div", {
                                className: m("MainProductsItem__buttonWrap"),
                                children: (0, n.jsx)(p.default, {
                                    href: e,
                                    children: (0, n.jsx)("div", {
                                        className: m("MainProductsItem__button"),
                                        children: (0, n.jsx)(f.Z, {
                                            theme: "noBorder",
                                            className: m("MainProductsItem__button"),
                                            children: "\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u0435\u0435"
                                        })
                                    })
                                })
                            })]
                        }), (0, n.jsx)("div", 
                        {
                            className: m("MainProductsItem__imageWrap"),
                            children: (0, n.jsx)("img", {
                                className: m("MainProductsItem__img"),
                                src: t.img,
                                onMouseOver: function (e) {
                                    return e.currentTarget.src = t.img2
                                },
                                onMouseOut: function (e) {
                                    return e.currentTarget.src = t.img
                                }
                            
                            })
                        }
                        )]
                    })
                },
                y = r(5147),
                M = r.n(y);

            function P(t, e) {
                var r = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter((function (e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    }))), r.push.apply(r, n)
                }
                return r
            }
            var v = d().bind(M()),
                g = function () {
                    var t = (0, o.useState)([{
                            name: "",
                            img: "",
                            disc: ".",
                            referAddres: "",
                            id: 1
                        }]),
                        e = t[0],
                        r = t[1];
                    return (0, o.useEffect)((function () {
                        fetch("/apimainProductList/?format=json").then((function (t) {
                            return t.json()
                        })).then((function (t) {
                            return r(t)
                        }))
                    }), []), (0, n.jsx)("div", {
                        className: v("MainProductsBlock", "MainProductsBlock__noMargin"),
                        children: (0, n.jsx)(i.Z, {
                            className: v("MainProductsBlock__container"),
                            children: (0, n.jsx)("div", {
                                className: v("MainProductsBlock__itemWrap"),
                                children: e.map((function (t) {
                                    return (0, n.jsx)(h, function (t) {
                                        for (var e = 1; e < arguments.length; e++) {
                                            var r = null != arguments[e] ? arguments[e] : {};
                                            e % 2 ? P(Object(r), !0).forEach((function (e) {
                                                (0, s.Z)(t, e, r[e])
                                            })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(r)) : P(Object(r)).forEach((function (e) {
                                                Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(r, e))
                                            }))
                                        }
                                        return t
                                    }({}, t), t.id)
                                }))
                            })
                        })
                    })
                },
                b = r(81095),
                I = r(81543),
                j = r.n(I),
                O = d().bind(j());

            function k() {
                return (0, n.jsxs)("div", {
                    className: j().container,
                    children: [(0, n.jsx)(a.default, {
                        children: (0, n.jsx)("title", {
                            children: b.TP.title
                        })
                    }), (0, n.jsx)(c.Z, {
                        className: O("MainPage"),
                        children: (0, n.jsx)(g, {})
                    })]
                })
            }
        },
        80719: function (t, e, r) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/products", function () {
                return r(57684)
            }])
        },
        94328: function (t) {
            t.exports = {
                Button: "styles_Button__JVxxi",
                Button_noBorder: "styles_Button_noBorder__aDEJg"
            }
        },
        5147: function (t) {
            t.exports = {
                MainProductsBlock: "styles_MainProductsBlock__3saVp",
                MainProductsBlock__noMargin: "styles_MainProductsBlock__noMargin__3h7IE",
                MainProductsBlock__titleWrap: "styles_MainProductsBlock__titleWrap__KPJHc",
                MainProductsBlock__noTitleWrap: "styles_MainProductsBlock__noTitleWrap__23SSU",
                MainProductsBlock__title: "styles_MainProductsBlock__title__2bHei",
                MainProductsBlock__itemWrap: "styles_MainProductsBlock__itemWrap__klWbr"
            }
        },
        702: function (t) {
            t.exports = {
                MainProductsItem__item: "styles_MainProductsItem__item__3bm9l",
                MainProductsItem__infoWrap: "styles_MainProductsItem__infoWrap__3-q8S",
                MainProductsItem__productId: "styles_MainProductsItem__productId__1MoGG",
                MainProductsItem__productName: "styles_MainProductsItem__productName__1IWr6",
                MainProductsItem__productUsage: "styles_MainProductsItem__productUsage__30CkE",
                MainProductsItem__productUsages: "styles_MainProductsItem__productUsages__1ukdK",
                MainProductsItem__productType: "styles_MainProductsItem__productType__22ZqU",
                MainProductsItem__productTypes: "styles_MainProductsItem__productTypes__1NLHI",
                MainProductsItem__productTypesNone: "styles_MainProductsItem__productTypesNone__2wgn6",
                MainProductsItem__productStructure: "styles_MainProductsItem__productStructure__3_kxR",
                MainProductsItem__productStructureWrap: "styles_MainProductsItem__productStructureWrap__17DYk",
                MainProductsItem__productStructures: "styles_MainProductsItem__productStructures__NcWuZ",
                MainProductsItem__productStructuresNone: "styles_MainProductsItem__productStructuresNone__268Ke",
                MainProductsItem__imageWrap: "styles_MainProductsItem__imageWrap__3R4uA",
                MainProductsItem__img: "styles_MainProductsItem__img__3R4uA",
                MainProductsItem__imageWrap__product1: "styles_MainProductsItem__imageWrap__product1__3720i",
                MainProductsItem__imageWrap__product2: "styles_MainProductsItem__imageWrap__product2__39zam",
                MainProductsItem__imageWrap__product3: "styles_MainProductsItem__imageWrap__product3__2-rBH",
                MainProductsItem__imageWrap__product4: "styles_MainProductsItem__imageWrap__product4__Y9B7H",
                MainProductsItem__buttonWrap: "styles_MainProductsItem__buttonWrap__1RV6M",
                MainProductsItem__button: "styles_MainProductsItem__button___8z_N"
            }
        },
        81543: function (t) {
            t.exports = {
                MainPage: "styles_MainPage__20MMS"
            }
        },
        9008: function (t, e, r) {
            t.exports = r(92775)
        }
    },
    function (t) {
        t.O(0, [663, 334, 146, 774, 888, 179], (function () {
            return e = 80719, t(t.s = e);
            var e
        }));
        var e = t.O();
        _N_E = e
    }
]);