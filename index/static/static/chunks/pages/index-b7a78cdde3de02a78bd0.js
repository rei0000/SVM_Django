(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [405], {
        82174: function (e, n, i) {
            "use strict";
            i.r(n), i.d(n, {
                default: function () {
                    return C
                }
            });
            i(24812);
            var _ = i(85893),
                a = i(67294),
                t = i(9008),
                o = i(14146),
                l = (i(21249), i(68309), i(41539), i(88674), i(63845)),
                s = i(95186),
                r = i(52997),
                c = (i(21079), i(15027)),
                d = i(47166),
                u = i.n(d),
                B = i(51618),
                M = i.n(B);
            l.Z.use([s.Z, r.Z]);
            var b = u().bind(M()),
                m = function () {
                    var e = (0, a.useState)([{
                            id: 0,
                            img: "",
                            name: "\u0411\u041e\u041b\u0415\u0415 30 \u041b\u0415\u0422 \u0423\u0421\u041f\u0415\u0428\u041d\u041e\u0419 \u0420\u0410\u0411\u041e\u0422\u042b"
                        }]),
                        n = e[0],
                        i = e[1];
                    return (0, a.useEffect)((function () {
                        fetch("/apimainPageSlider/?format=json").then((function (e) {
                            return e.json()
                        })).then((function (e) {
                            return i(e)
                        }))
                    }), []), (0, _.jsx)("div", {
                        className: b("MainBannerSlider"),
                        children: (0, _.jsxs)(c.Z, {
                            className: b("MainBannerSlider__container"),
                            children: [(0, _.jsx)("div", {
                                className: b("MainBannerSlider__contentWrap"),
                                children: (0, _.jsx)("h1", {
                                    className: b("MainBannerSlider__title"),
                                    children: n[0].name
                                })
                            }), (0, _.jsx)("div", {
                                className: b("MainBannerSlider__items"),
                                children: (0, _.jsx)("div", {
                                    className: b("MainBannerSlider__itemsWrap"),
                                    children: n.map((function (e) {
                                        return (0, _.jsx)("video", {
                                            className: b("MainBannerSlider__video"),
                                            src: e.img,
                                            muted: !0,
                                            autoPlay: !0,
                                            loop: !0,
                                            children: " "
                                        })
                                    }))
                                })
                            })]
                        })
                    })
                },
                p = i(96156),
                k = i(83528),
                h = i.n(k);

            function y(e, n) {
                var i = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var _ = Object.getOwnPropertySymbols(e);
                    n && (_ = _.filter((function (n) {
                        return Object.getOwnPropertyDescriptor(e, n).enumerable
                    }))), i.push.apply(i, _)
                }
                return i
            }

            function f(e) {
                for (var n = 1; n < arguments.length; n++) {
                    var i = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? y(Object(i), !0).forEach((function (n) {
                        (0, p.Z)(e, n, i[n])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(i)) : y(Object(i)).forEach((function (n) {
                        Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(i, n))
                    }))
                }
                return e
            }
            var g = function (e) {
                return (0, _.jsx)("svg", f(f({}, e), {}, {
                    children: (0, _.jsx)("path", {
                        d: "M29.9116 28L41 3.9375L33.8061 0L19.9591 22.4219L29.9116 28ZM9.95251 28L21.0409 3.9375L13.847 0L0 22.4219L9.95251 28Z",
                        fill: "#B90C2F"
                    })
                }))
            };
            g.defaultProps = {
                width: "41",
                height: "28",
                viewBox: "0 0 41 28",
                fill: "none",
                xmlns: "http://www.w3.org/2000/svg"
            };
            var v = u().bind(h()),
                A = function () {
                    var e = (0, a.useState)([{
                            id: 1,
                            img: "http://0.0.0.0:8000/static/media/photo.png",
                            name: "\u0410\u0440\u0442\u0435\u043c\u044c\u0435\u0432 \u0410\u043b\u0435\u043a\u0441\u0435\u0439 \u0412\u043b\u0430\u0434\u0438\u0441\u043b\u0430\u0432\u043e\u0432\u0438\u0447",
                            jobTitle: "\u0413\u0435\u043d\u0435\u0440\u0430\u043b\u044c\u043d\u044b\u0439 \u0434\u0438\u0440\u0435\u043a\u0442\u043e\u0440",
                            quoteList: [{
                                id: 1,
                                QuoteBlock: 1,
                                disc: "\u0421\u0442\u0440\u0430\u0442\u0435\u0433\u0438\u0447\u0435\u0441\u043a\u043e\u0439 \u0446\u0435\u043b\u044c\u044e \u041e\u041e\u041e \xab\u0421\u0430\u043c\u0430\u0440\u0430\u0432\u043e\u043b\u0433\u043e\u043c\u0430\u0448\xbb \u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u0434\u043e\u0441\u0442\u0438\u0436\u0435\u043d\u0438\u0435 \u043b\u0438\u0434\u0435\u0440\u0441\u0442\u0432\u0430 \u0432 \u043e\u0431\u043b\u0430\u0441\u0442\u0438 \u043f\u0440\u043e\u0435\u043a\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f, \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430 \u0438 \u043f\u043e\u0441\u0442\u0430\u0432\u043e\u043a \u0442\u0440\u0443\u0431\u043e\u043f\u0440\u043e\u0432\u043e\u0434\u043d\u043e\u0439 \u0430\u0440\u043c\u0430\u0442\u0443\u0440\u044b, \u043f\u043d\u0435\u0432\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0438\u0445 \u0438 \u043f\u043d\u0435\u0432\u043c\u043e\u0433\u0438\u0434\u0440\u0430\u0432\u043b\u0438\u0447\u0435\u0441\u043a\u0438\u0445 \u043f\u0440\u0438\u0432\u043e\u0434\u043e\u0432 \u0434\u043b\u044f \u043d\u0435\u0444\u0442\u044f\u043d\u043e\u0439, \u0433\u0430\u0437\u043e\u0432\u043e\u0439 \u0438 \u0445\u0438\u043c\u0438\u0447\u0435\u0441\u043a\u043e\u0439 \u043f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u043e\u0441\u0442\u0438, \u043e\u0431\u0435\u0441\u043f\u0435\u0447\u0438\u0432\u0430\u044f \u0438\u043d\u0434\u0438\u0432\u0438\u0434\u0443\u0430\u043b\u044c\u043d\u044b\u0439 \u043f\u043e\u0434\u0445\u043e\u0434 \u043a \u0432\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044e \u0432\u0441\u0435\u0445 \u0442\u0440\u0435\u0431\u043e\u0432\u0430\u043d\u0438\u0439 \u043f\u043e\u0442\u0440\u0435\u0431\u0438\u0442\u0435\u043b\u0435\u0439, \u0441\u0442\u0440\u0435\u043c\u044f\u0441\u044c \u043e\u043f\u0440\u0430\u0432\u0434\u0430\u0442\u044c \u0438 \u043f\u0440\u0435\u0432\u0437\u043e\u0439\u0442\u0438 \u0438\u0445 \u043e\u0436\u0438\u0434\u0430\u043d\u0438\u044f \u0432 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0438 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u0430."
                            }, {
                                id: 2,
                                QuoteBlock: 1,
                                disc: "\u0412 \u0441\u0432\u043e\u0438\u0445 \u0432\u0437\u0430\u0438\u043c\u043e\u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u044f\u0445 \u0441 \u0437\u0430\u043a\u0430\u0437\u0447\u0438\u043a\u043e\u043c \u043c\u044b \u0441\u0442\u0440\u0435\u043c\u0438\u043c\u0441\u044f \u043a \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0441\u0442\u0438 \u043e\u0442\u043d\u043e\u0448\u0435\u043d\u0438\u0439 \u043d\u0430 \u0432\u0441\u0435\u0445 \u0443\u0440\u043e\u0432\u043d\u044f\u0445 \u0438 \u0447\u0435\u0441\u0442\u043d\u043e\u043c\u0443 \u0432\u0435\u0434\u0435\u043d\u0438\u044e \u0431\u0438\u0437\u043d\u0435\u0441\u0430. \u0413\u0435\u043e\u0433\u0440\u0430\u0444\u0438\u044f \u043f\u043e\u0441\u0442\u0430\u0432\u043e\u043a \u043f\u0440\u043e\u0434\u0443\u043a\u0446\u0438\u0438 \u041e\u041e\u041e \xab\u0421\u0430\u043c\u0430\u0440\u0430\u0432\u043e\u043b\u0433\u043e\u043c\u0430\u0448\xbb \u0443\u0436\u0435 \u0434\u043e\u0441\u0442\u0430\u0442\u043e\u0447\u043d\u043e \u0448\u0438\u0440\u043e\u043a\u0430. \u041d\u0430\u0448\u0438 \u0448\u0430\u0440\u043e\u0432\u044b\u0435 \u043a\u0440\u0430\u043d\u044b \u0438 \u043f\u043d\u0435\u0432\u043c\u043e\u0433\u0438\u0434\u0440\u0430\u0432\u043b\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u043f\u0440\u0438\u0432\u043e\u0434\u044b \u043e\u0442\u043b\u0438\u0447\u043d\u043e \u0437\u0430\u0440\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u043e\u0432\u0430\u043b\u0438 \u0441\u0435\u0431\u044f \u043d\u0430 \u043a\u0440\u0443\u043f\u043d\u0435\u0439\u0448\u0438\u0445 \u043c\u0435\u0441\u0442\u043e\u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f\u0445 \u043d\u0435\u0444\u0442\u0438 \u0438 \u0433\u0430\u0437\u0430 \u0420\u043e\u0441\u0441\u0438\u0438: \u0423\u0440\u0435\u043d\u0433\u043e\u0439\u0441\u043a\u043e\u043c, \u042f\u043c\u0431\u0443\u0440\u0433\u0441\u043a\u043e\u043c, \u0417\u0430\u043f\u043e\u043b\u044f\u0440\u043d\u043e\u043c, \u0427\u0430\u044f\u043d\u0434\u0438\u043d\u0441\u043a\u043e\u043c, \u0411\u043e\u0432\u0430\u043d\u0435\u043d\u043a\u043e\u0432\u0441\u043a\u043e\u043c, \u042e\u0436\u043d\u043e-\u0420\u0443\u0441\u0441\u043a\u043e\u043c, \u041f\u0435\u0441\u0446\u043e\u0432\u043e\u043c, \u0417\u0430\u043f\u0430\u0434\u043d\u043e-\u0421\u0430\u043b\u044b\u043c\u0441\u043a\u043e\u043c, \u0412\u0430\u043d\u043a\u043e\u0440\u0441\u043a\u043e\u043c, \u041e\u0440\u0435\u043d\u0431\u0443\u0440\u0433\u0441\u043a\u043e\u043c \u0438 \u0434\u0440."
                            }, {
                                id: 3,
                                QuoteBlock: 1,
                                disc: "\u0421\u043e\u0432\u0435\u0440\u0448\u0435\u043d\u0441\u0442\u0432\u043e\u0432\u0430\u043d\u0438\u0435 \u0434\u0435\u0439\u0441\u0442\u0432\u0443\u044e\u0449\u0438\u0445 \u043d\u0430 \u043f\u0440\u0435\u0434\u043f\u0440\u0438\u044f\u0442\u0438\u0438 \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u043e\u0432, \u0432\u043d\u0435\u0434\u0440\u0435\u043d\u0438\u0435 \u0438\u043d\u043d\u043e\u0432\u0430\u0446\u0438\u0439, \u043a\u0430\u0434\u0440\u043e\u0432\u044b\u0439 \u0438 \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0439 \u043f\u043e\u0442\u0435\u043d\u0446\u0438\u0430\u043b \u2013 \u0432\u0441\u0435 \u044d\u0442\u043e \u043f\u043e\u0437\u0432\u043e\u043b\u044f\u0435\u0442 \u043d\u0430\u043c \u0441 \u0443\u0432\u0435\u0440\u0435\u043d\u043d\u044b\u043c \u043e\u043f\u0442\u0438\u043c\u0438\u0437\u043c\u043e\u043c \u0441\u043c\u043e\u0442\u0440\u0435\u0442\u044c \u0432 \u0431\u0443\u0434\u0443\u0449\u0435\u0435 \u0438 \u0431\u044b\u0442\u044c \u0433\u043e\u0442\u043e\u0432\u044b\u043c\u0438 \u043a \u043d\u043e\u0432\u044b\u043c \u0432\u044b\u0437\u043e\u0432\u0430\u043c, \u043f\u043e\u043a\u043e\u0440\u0435\u043d\u0438\u044e \u043d\u043e\u0432\u044b\u0445 \u0441\u0442\u0440\u0430\u0442\u0435\u0433\u0438\u0447\u0435\u0441\u043a\u0438\u0445 \u0432\u044b\u0441\u043e\u0442. \u0417\u0430 \u043d\u0430\u0448\u0438\u043c\u0438 \u0443\u0441\u043f\u0435\u0445\u0430\u043c\u0438 \u0441\u0442\u043e\u0438\u0442 \u0433\u043b\u0430\u0432\u043d\u043e\u0435 \u2013 \u0434\u043e\u0432\u0435\u0440\u0438\u0435 \u043d\u0430\u0448\u0438\u0445 \u0437\u0430\u043a\u0430\u0437\u0447\u0438\u043a\u043e\u0432 \u0438 \u043f\u0430\u0440\u0442\u043d\u0435\u0440\u043e\u0432."
                            }]
                        }]),
                        n = e[0],
                        i = e[1];
                    return (0, a.useEffect)((function () {
                        fetch("/apiQuoteBlockViewSet/?format=json").then((function (e) {
                            return e.json()
                        })).then((function (e) {
                            return i(e)
                        }))
                    }), []), (0, _.jsx)("div", {
                        className: v("MainInfoBlock"),
                        children: (0, _.jsx)(c.Z, {
                            className: v("MainInfoBlock__container"),
                            children: (0, _.jsxs)("div", {
                                className: v("MainInfoBlock__contentWrap"),
                                children: [(0, _.jsx)("div", {
                                    className: v("MainInfoBlock__content"),
                                    children: (0, _.jsx)("div", {
                                        className: v("MainInfoBlock__imageWrap"),
                                        children: (0, _.jsx)("img", {
                                            className: v("MainInfoBlock__image"),
                                            src: n[0].img,
                                            alt: n[0].img
                                        })
                                    })
                                }), (0, _.jsxs)("div", {
                                    className: v("MainInfoBlock__content"),
                                    children: [(0, _.jsx)("div", {
                                        className: v("MainInfoBlock__quotes"),
                                        children: (0, _.jsx)(g, {})
                                    }), n[0].quoteList.map((function (e) {
                                        return (0, _.jsx)("div", {
                                            className: v("MainInfoBlock__quote"),
                                            dangerouslySetInnerHTML: {
                                                __html: e.disc
                                            }
                                        }, e.id)
                                    })), (0, _.jsx)("div", {
                                        className: v("MainInfoBlock__quote"),
                                        children: (0, _.jsxs)("div", {
                                            children: [(0, _.jsx)("div", {
                                                className: v("MainInfoBlock__title"),
                                                children: n[0].name
                                            }), (0, _.jsx)("div", {
                                                className: v("MainInfoBlock__subtitle"),
                                                children: n[0].jobTitle
                                            })]
                                        })
                                    })]
                                })]
                            })
                        })
                    })
                },
                j = i(14583),
                x = i.n(j);
            l.Z.use([s.Z]);
            var S = u().bind(x()),
                W = function () {
                    var e = (0, a.useState)([{
                            id: 1,
                            img: ""
                        }, {
                            id: 2,
                            img: ""
                        }, {
                            id: 3,
                            img: ""
                        }, {
                            id: 4,
                            img: ""
                        }, {
                            id: 5,
                            img: ""
                        }, {
                            id: 6,
                            img: ""
                        }, {
                            id: 7,
                            img: ""
                        }, {
                            id: 8,
                            img: ""
                        }, {
                            id: 9,
                            img: ""
                        }, {
                            id: 10,
                            img: ""
                        }, {
                            id: 11,
                            img: ""
                        }, {
                            id: 12,
                            img: ""
                        }, {
                            id: 13,
                            img: ""
                        }, {
                            id: 14,
                            img: ""
                        }, {
                            id: 15,
                            img: ""
                        }, {
                            id: 16,
                            img: ""
                        }]),
                        n = e[0],
                        i = e[1];
                    return (0, a.useEffect)((function () {
                        fetch("apicompanyIcon/?format=json").then((function (e) {
                            return e.json()
                        })).then((function (e) {
                            return i(e)
                        }))
                    }), []), (0, _.jsx)("div", {
                        className: S("MainAboutBlock"),
                        children: (0, _.jsxs)(c.Z, {
                            className: S("Header__container"),
                            children: [(0, _.jsx)("div", {
                                className: S("MainAboutBlock__titleWrap"),
                                children: (0, _.jsx)("h1", {
                                    className: S("MainAboutBlock__title"),
                                    children: "\u041d\u0410\u0421 \u0417\u041d\u0410\u042e\u0422, \u041d\u0410\u041c \u0414\u041e\u0412\u0415\u0420\u042f\u042e\u0422"
                                })
                            }), (0, _.jsx)("div", {
                                className: S("MainAboutBlock__contentWrap"),
                                children: (0, _.jsx)("div", {
                                    className: S("MainAboutBlock__content"),
                                    children: (0, _.jsx)("div", {
                                        className: S("MainAboutBlock__tabContent"),
                                        children: (0, _.jsx)("div", {
                                            className: S("MainAboutBlock__items"),
                                            children: (0, _.jsx)("div", {
                                                className: S("MainAboutBlock__itemsWrap"),
                                                children: n.map((function (e) {
                                                    return (0, _.jsx)("div", {
                                                        className: S("MainAboutBlock__item"),
                                                        children: (0, _.jsx)("div", {
                                                            className: S("MainAboutBlock__imageWrap"),
                                                            children: (0, _.jsx)("img", {
                                                                src: e.img,
                                                                alt: e.img
                                                            })
                                                        })
                                                    }, e.id)
                                                }))
                                            })
                                        })
                                    })
                                })
                            })]
                        })
                    })
                },
                N = i(57305),
                I = i.n(N),
                w = u().bind(I()),
                T = function () {
                    var e = (0, a.useState)([{
                            id: 2,
                            disc: "\u041b\u041e\u041a\u0410\u041b\u0418\u0417\u0410\u0426\u0418\u042f 100%"
                        }, {
                            id: 3,
                            disc: "\u0418\u041d\u0414\u0418\u0412\u0418\u0414\u0423\u0410\u041b\u042c\u041d\u041e\u0415 \u041f\u0420\u041e\u0415\u041a\u0422\u0418\u0420\u041e\u0412\u0410\u041d\u0418\u0415"
                        }, {
                            id: 4,
                            disc: "\u0420\u0410\u0421\u0428\u0418\u0420\u0415\u041d\u041d\u0410\u042f \u0413\u0410\u0420\u0410\u041d\u0422\u0418\u042f"
                        }, {
                            id: 5,
                            disc: "\u0415\u0414\u0418\u041d\u0418\u0427\u041d\u042b\u0415 \u041f\u0410\u0420\u0422\u0418\u0418"
                        }, {
                            id: 6,
                            disc: "\u0418\u0421\u041f\u041e\u041b\u041d\u0415\u041d\u0418\u042f \u0414\u041b\u042f \u0410\u0413\u0420\u0415\u0421\u0421\u0418\u0412\u041d\u042b\u0425 \u0421\u0420\u0415\u0414 (\u041c\u0415\u0422\u0410\u041d\u041e\u041b, H\u2082S, CO\u2082)"
                        }, {
                            id: 7,
                            disc: "\u0418\u0421\u041f\u041e\u041b\u041d\u0415\u041d\u0418\u042f \u0414\u041b\u042f \u0412\u042b\u0421\u041e\u041a\u041e\u0422\u0415\u041c\u041f\u0415\u0420\u0410\u0422\u0423\u0420\u041d\u042b\u0425 \u0421\u0420\u0415\u0414 (\u0414\u041e +250\xb0\u0421)"
                        }, {
                            id: 8,
                            disc: "\u0418\u0421\u041f\u041e\u041b\u041d\u0415\u041d\u0418\u042f \u0414\u041b\u042f \u041d\u0418\u0417\u041a\u041e\u0422\u0415\u041c\u041f\u0415\u0420\u0410\u0422\u0423\u0420\u041d\u042b\u0425 \u041a\u041b\u0418\u041c\u0410\u0422\u0418\u0427\u0415\u0421\u041a\u0418\u0425 \u0417\u041e\u041d (\u0414\u041e \u2013 70\xb0\u0421)"
                        }, {
                            id: 9,
                            disc: "\u041c\u0418\u041d\u0418\u041c\u0410\u041b\u042c\u041d\u042b\u0415 \u0421\u0420\u041e\u041a\u0418 \u0418\u0417\u0413\u041e\u0422\u041e\u0412\u041b\u0415\u041d\u0418\u042f"
                        }]),
                        n = e[0],
                        i = e[1];
                    return (0, a.useEffect)((function () {
                        fetch("apipostsgeographytable/?format=json").then((function (e) {
                            return e.json()
                        })).then((function (e) {
                            return i(e)
                        }))
                    }), []), (0, _.jsx)("div", {
                        className: w("GeographyTable"),
                        children: (0, _.jsxs)(c.Z, {
                            className: w("GeographyTable__container"),
                            children: [(0, _.jsx)("h2", {
                                className: w("GeographyTable__title"),
                                children: "\u0427\u0442\u043e \u043e\u0442\u043b\u0438\u0447\u0430\u0435\u0442 \u043d\u0430\u0448\u0435 \u043f\u0440\u0435\u0434\u043f\u0440\u0438\u044f\u0442\u0438\u0435 \u043e\u0442 \u0434\u0440\u0443\u0433\u0438\u0445"
                            }), (0, _.jsx)("div", {
                                className: w("GeographyTable__itemWrap"),
                                children: n.map((function (e) {
                                    return (0, _.jsx)("div", {
                                        className: w("GeographyTable__item"),
                                        children: (0, _.jsx)("div", {
                                            className: w("GeographyTable__name__Wrapp"),
                                            children: (0, _.jsx)("div", {
                                                className: w("GeographyTable__name"),
                                                children: e.disc
                                            })
                                        })
                                    }, e.id)
                                }))
                            })]
                        })
                    })
                },
                G = i(81095),
                O = i(84125),
                P = i.n(O),
                L = u().bind(P());

            function C() {
                return (0, _.jsxs)("div", {
                    className: P().container,
                    children: [(0, _.jsx)(t.default, {
                        children: (0, _.jsx)("title", {
                            children: G.TP.title
                        })
                    }), (0, _.jsxs)(o.Z, {
                        className: L("MainPage"),
                        children: [(0, _.jsx)(m, {}), (0, _.jsx)(A, {}), (0, _.jsx)(T, {}), (0, _.jsx)(W, {})]
                    })]
                })
            }
        },
        45301: function (e, n, i) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/", function () {
                return i(82174)
            }])
        },
        57305: function (e) {
            e.exports = {
                GeographyTable: "styles_GeographyTable__1AYaG",
                GeographyTable__title: "styles_GeographyTable__title__1WGsS",
                GeographyTable__itemWrap: "styles_GeographyTable__itemWrap__YV8jU",
                GeographyTable__item: "styles_GeographyTable__item__2AStO",
                GeographyTable__name: "styles_GeographyTable__name__zzyjU",
                GeographyTable__descriptionWrapp: "styles_GeographyTable__descriptionWrapp__2_O2d",
                GeographyTable__description: "styles_GeographyTable__description__aK0VV"
            }
        },
        14583: function (e) {
            e.exports = {
                MainAboutBlock: "styles_MainAboutBlock__1et9S",
                MainAboutBlock__buttonWrap: "styles_MainAboutBlock__buttonWrap__3ccEL",
                MainAboutBlock__titleWrap: "styles_MainAboutBlock__titleWrap__13iCl",
                MainAboutBlock__title: "styles_MainAboutBlock__title__1wxmL",
                MainAboutBlock__contentWrap: "styles_MainAboutBlock__contentWrap__3Kkx_",
                MainAboutBlock__content: "styles_MainAboutBlock__content__2fXJX",
                MainAboutBlock__tabContent: "styles_MainAboutBlock__tabContent__2cyb9",
                MainAboutBlock__itemsWrap: "styles_MainAboutBlock__itemsWrap__3jpsX",
                MainAboutBlock__item: "styles_MainAboutBlock__item__9xjiw",
                MainAboutBlock__imageWrap: "styles_MainAboutBlock__imageWrap__wJ2vL",
                MainAboutBlock__image: "styles_MainAboutBlock__image__3VQEW",
                MainAboutBlock__imageTop: "styles_MainAboutBlock__imageTop__f8RLo",
                MainAboutBlock__achievements: "styles_MainAboutBlock__achievements__2K_cu",
                MainAboutBlock__achievementsWrap: "styles_MainAboutBlock__achievementsWrap__1csw6",
                MainAboutBlock__achievement: "styles_MainAboutBlock__achievement__1vGjl",
                MainAboutBlock__achievementImageWrap: "styles_MainAboutBlock__achievementImageWrap__1OiQH",
                MainAboutBlock__achievementImage: "styles_MainAboutBlock__achievementImage__13_Q1",
                MainAboutBlock__achievementButtonWrap: "styles_MainAboutBlock__achievementButtonWrap__3CpUh",
                MainAboutBlock__navigationBlock: "styles_MainAboutBlock__navigationBlock__1xUGn",
                MainAboutBlock__navigation: "styles_MainAboutBlock__navigation__1CXrG",
                MainAboutBlock__arrowWrap: "styles_MainAboutBlock__arrowWrap__7WhXJ",
                MainAboutBlock__policyTitle: "styles_MainAboutBlock__policyTitle__3A2Sn",
                MainAboutBlock__policyDesc: "styles_MainAboutBlock__policyDesc__eCa6T",
                MainAboutBlock__description: "styles_MainAboutBlock__description__8RxO3",
                MainAboutBlock__tabs: "styles_MainAboutBlock__tabs__3ZkBl",
                MainAboutBlock__tabsWrap: "styles_MainAboutBlock__tabsWrap__axn7u",
                MainAboutBlock__tabsWrap_active: "styles_MainAboutBlock__tabsWrap_active__CyWCf",
                MainAboutBlock__button: "styles_MainAboutBlock__button__wOC2I",
                MainAboutBlock__tabsWrap_inActive: "styles_MainAboutBlock__tabsWrap_inActive__1Yqbf",
                MainAboutBlock__buttonWrap_active: "styles_MainAboutBlock__buttonWrap_active__5dWU5",
                MainAboutBlock__button_active: "styles_MainAboutBlock__button_active__1JP-8",
                MainAboutBlock__button_inactive: "styles_MainAboutBlock__button_inactive__2UUpG",
                active: "styles_active__uHdeu",
                inactive: "styles_inactive__27lDM"
            }
        },
        51618: function (e) {
            e.exports = {
                MainBannerSlider__contentWrap: "styles_MainBannerSlider__contentWrap__dNovr",
                MainBannerSlider__title: "styles_MainBannerSlider__title__1ujuj",
                MainBannerSlider__textWrap: "styles_MainBannerSlider__textWrap__1sx1O",
                MainBannerSlider__description: "styles_MainBannerSlider__description__2b5-2",
                MainBannerSlider__items: "styles_MainBannerSlider__items__8VW7L",
                MainBannerSlider__itemsWrap: "styles_MainBannerSlider__itemsWrap__1R0px",
                MainBannerSlider__item: "styles_MainBannerSlider__item__Jd_wK",
                MainBannerSlider__imageWrap: "styles_MainBannerSlider__imageWrap__2MPam",
                MainBannerSlider__video: "styles_MainBannerSlider__video__18fdN",
                MainBannerSlider__navigationWrap: "styles_MainBannerSlider__navigationWrap__2BT0a",
                MainBannerSlider__navigation: "styles_MainBannerSlider__navigation__cD94t",
                MainBannerSlider__navigation_currentSlide: "styles_MainBannerSlider__navigation_currentSlide__3v7XS",
                MainBannerSlider__navigation_allSlides: "styles_MainBannerSlider__navigation_allSlides__BXz-6",
                MainBannerSlider__line: "styles_MainBannerSlider__line__2sHMg",
                MainBannerSlider__lineActive: "styles_MainBannerSlider__lineActive__3d_KE",
                MainBannerSlider__arrowWrap: "styles_MainBannerSlider__arrowWrap__Pq1SP",
                MainBannerSlider__arrow_next: "styles_MainBannerSlider__arrow_next__1FLgc",
                MainBannerSlider__arrow_disabled: "styles_MainBannerSlider__arrow_disabled__2AAtc"
            }
        },
        83528: function (e) {
            e.exports = {
                MainInfoBlock: "styles_MainInfoBlock__1z5HD",
                MainInfoBlock__contentWrap: "styles_MainInfoBlock__contentWrap__2Q4RP",
                MainInfoBlock__content: "styles_MainInfoBlock__content__3YX6p",
                MainInfoBlock__imageWrap: "styles_MainInfoBlock__imageWrap__34zwA",
                MainInfoBlock__quotes: "styles_MainInfoBlock__quotes__2PP8v",
                MainInfoBlock__quote: "styles_MainInfoBlock__quote__Gek-p",
                MainInfoBlock__title: "styles_MainInfoBlock__title__IY5Kf",
                MainInfoBlock__subtitle: "styles_MainInfoBlock__subtitle__2xFzb"
            }
        },
        84125: function (e) {
            e.exports = {
                MainPage: "styles_MainPage__1kPWL"
            }
        }
    },
    function (e) {
        e.O(0, [663, 334, 53, 270, 146, 774, 888, 179], (function () {
            return n = 45301, e(e.s = n);
            var n
        }));
        var n = e.O();
        _N_E = n
    }
]);