(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [552], {
        15218: function (o, i, l) {
            "use strict";
            var e = l(82109),
                t = l(14230);
            e({
                target: "String",
                proto: !0,
                forced: l(43429)("anchor")
            }, {
                anchor: function (o) {
                    return t(this, "a", "name", o)
                }
            })
        },
        10075: function (o, i, l) {
            "use strict";
            l(24812);
            var e = l(96156),
                t = l(85893),
                r = (l(67294), l(47166)),
                _ = l.n(r),
                c = l(97085),
                n = l.n(c);

            function d(o, i) {
                var l = Object.keys(o);
                if (Object.getOwnPropertySymbols) {
                    var e = Object.getOwnPropertySymbols(o);
                    i && (e = e.filter((function (i) {
                        return Object.getOwnPropertyDescriptor(o, i).enumerable
                    }))), l.push.apply(l, e)
                }
                return l
            }

            function s(o) {
                for (var i = 1; i < arguments.length; i++) {
                    var l = null != arguments[i] ? arguments[i] : {};
                    i % 2 ? d(Object(l), !0).forEach((function (i) {
                        (0, e.Z)(o, i, l[i])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(o, Object.getOwnPropertyDescriptors(l)) : d(Object(l)).forEach((function (i) {
                        Object.defineProperty(o, i, Object.getOwnPropertyDescriptor(l, i))
                    }))
                }
                return o
            }
            var a = function (o) {
                return (0, t.jsx)("svg", s(s({}, o), {}, {
                    children: (0, t.jsx)("path", {
                        d: "M21.6983 0L20.3453 1.45968L26.346 7.96623H0V10.0338H26.346L20.3453 16.5403L21.6983 18L30 9L21.6983 0Z",
                        fill: "currentColor"
                    })
                }))
            };
            a.defaultProps = {
                width: "30",
                height: "18",
                viewBox: "0 0 30 18",
                fill: "none",
                xmlns: "http://www.w3.org/2000/svg"
            };
            var u = function (o) {
                return (0, t.jsx)("svg", s(s({}, o), {}, {
                    children: (0, t.jsx)("path", {
                        d: "M8.30169 18L9.65473 16.5403L3.65397 10.0338L30 10.0338L30 7.96623L3.65397 7.96623L9.65473 1.45968L8.30169 -1.89693e-06L7.86805e-07 9L8.30169 18Z",
                        fill: "currentColor"
                    })
                }))
            };
            u.defaultProps = {
                width: "30",
                height: "18",
                viewBox: "0 0 30 18",
                fill: "none",
                xmlns: "http://www.w3.org/2000/svg"
            };
            var k = _().bind(n());
            i.Z = function (o) {
                var i = o.className,
                    l = o.onClick,
                    e = o.type,
                    r = void 0 === e ? "right" : e,
                    _ = o.disabled,
                    c = void 0 !== _ && _,
                    n = o.white,
                    d = void 0 !== n && n,
                    s = o.blue,
                    B = void 0 !== s && s,
                    S = "left" === r ? u : a;
                return (0, t.jsx)("button", {
                    className: k("Arrow", "Arrow_".concat(r), {
                        Arrow_disabled: c
                    }, {
                        Arrow_white: d
                    }, {
                        Arrow_blue: B
                    }, i),
                    onClick: l,
                    children: (0, t.jsx)(S, {})
                })
            }
        },
        19299: function (o, i, l) {
            "use strict";
            l.r(i), l.d(i, {
                default: function () {
                    return ao
                }
            });
            l(82526), l(41817), l(24812), l(41539), l(88674), l(15218);
            var e = l(85893),
                t = l(67294),
                r = l(9008),
                _ = l(14146),
                c = (l(21249), l(63845)),
                n = l(95186),
                d = l(15027),
                s = l(47166),
                a = l.n(s),
                u = l(57642),
                k = l.n(u),
                B = l(10075);
            c.Z.use([n.Z]);
            var S = a().bind(k()),
                P = function () {
                    var o = (0, t.useState)([{
                            id: 1,
                            title: "\u0421\u0410\u041c\u0410\u0420\u0410\u0412\u041e\u041b\u0413\u041e\u041c\u0410\u0428 \u0421\u0415\u0413\u041e\u0414\u041d\u042f",
                            sliderList: [{
                                id: 1,
                                aboutUsMainBlock: 1,
                                imgupload: "http://0.0.0.0:8000/main/static/static/img/aboutUsMainBlockSlider/about-us-slide.jpeg"
                            }, {
                                id: 2,
                                aboutUsMainBlock: 1,
                                imgupload: "http://0.0.0.0:8000/main/static/static/img/aboutUsMainBlockSlider/about-us-slide3.jpeg"
                            }, {
                                id: 3,
                                aboutUsMainBlock: 1,
                                imgupload: "http://0.0.0.0:8000/main/static/static/img/aboutUsMainBlockSlider/about-us-slide1.jpeg"
                            }, {
                                id: 4,
                                aboutUsMainBlock: 1,
                                imgupload: "http://0.0.0.0:8000/main/static/static/img/aboutUsMainBlockSlider/about-us-slide4_4lxeWom.jpeg"
                            }],
                            textList: [{
                                id: 1,
                                aboutUsMainBlock: 1,
                                text: "\u041e\u041e\u041e \xab\u0421\u0430\u043c\u0430\u0440\u0430\u0432\u043e\u043b\u0433\u043e\u043c\u0430\u0448\xbb - \u0440\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0435 \u043c\u0430\u0448\u0438\u043d\u043e\u0441\u0442\u0440\u043e\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u043f\u0440\u0435\u0434\u043f\u0440\u0438\u044f\u0442\u0438\u0435 \u0441\u043e 100% \u043b\u043e\u043a\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u0435\u0439 \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430, \u0432\u044b\u043f\u043e\u043b\u043d\u044f\u044e\u0449\u0435\u0435 \u043f\u043e\u043b\u043d\u044b\u0439 \u0446\u0438\u043a\u043b \u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u044f \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u044f \u0434\u043b\u044f \u043d\u0435\u0444\u0442\u044f\u043d\u043e\u0439, \u0433\u0430\u0437\u043e\u0432\u043e\u0439 \u0438 \u0445\u0438\u043c\u0438\u0447\u0435\u0441\u043a\u043e\u0439 \u043f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u043e\u0441\u0442\u0438 \u0441 \u0430\u0434\u0430\u043f\u0442\u0430\u0446\u0438\u0435\u0439 \u043f\u043e\u0434 \u043b\u044e\u0431\u044b\u0435 \u0442\u0440\u0435\u0431\u043e\u0432\u0430\u043d\u0438\u044f \u043f\u0440\u043e\u0435\u043a\u0442\u043e\u0432 \u0438 \u0443\u0441\u043b\u043e\u0432\u0438\u0439 \u044d\u043a\u0441\u043f\u043b\u0443\u0430\u0442\u0430\u0446\u0438\u0438."
                            }, {
                                id: 2,
                                aboutUsMainBlock: 1,
                                text: "\u041e\u041e\u041e \xab\u0421\u0430\u043c\u0430\u0440\u0430\u0432\u043e\u043b\u0433\u043e\u043c\u0430\u0448\xbb \u0443\u043a\u043e\u043c\u043f\u043b\u0435\u043a\u0442\u043e\u0432\u0430\u043d\u043e \u0432\u044b\u0441\u043e\u043a\u043e\u043a\u0432\u0430\u043b\u0438\u0444\u0438\u0446\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u044b\u043c\u0438 \u0441\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0441\u0442\u0430\u043c\u0438."
                            }, {
                                id: 3,
                                aboutUsMainBlock: 1,
                                text: "\u0412\u0441\u044f \u0432\u044b\u043f\u0443\u0441\u043a\u0430\u0435\u043c\u0430\u044f \u041e\u041e\u041e \xab\u0421\u0430\u043c\u0430\u0440\u0430\u0432\u043e\u043b\u0433\u043e\u043c\u0430\u0448\xbb \u043f\u0440\u043e\u0434\u0443\u043a\u0446\u0438\u044f \u0438\u0437\u0433\u043e\u0442\u0430\u0432\u043b\u0438\u0432\u0430\u0435\u0442\u0441\u044f \u0438\u0437 \u0440\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0438\u0445 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u043e\u0432 \u0438 \u043a\u043e\u043c\u043f\u043b\u0435\u043a\u0442\u0443\u044e\u0449\u0438\u0445, \u0447\u0442\u043e \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u043e \u0417\u0430\u043a\u043b\u044e\u0447\u0435\u043d\u0438\u0435\u043c \u043e \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u0438 \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430 \u043f\u0440\u043e\u043c\u044b\u0448\u043b\u0435\u043d\u043d\u043e\u0439 \u043f\u0440\u043e\u0434\u0443\u043a\u0446\u0438\u0438 \u043d\u0430 \u0442\u0435\u0440\u0440\u0438\u0442\u043e\u0440\u0438\u0438 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438 \u041c\u0418\u041d\u041f\u0420\u041e\u041c\u0422\u041e\u0420\u0413 \u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u043e\u0439 \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u0438."
                            }, {
                                id: 4,
                                aboutUsMainBlock: 1,
                                text: "\u041f\u0440\u0435\u0434\u043f\u0440\u0438\u044f\u0442\u0438\u0435 \u043d\u0430\u0445\u043e\u0434\u0438\u0442\u0441\u044f \u0432 \u043f\u043e\u0441\u0442\u043e\u044f\u043d\u043d\u043e\u043c \u0440\u0430\u0437\u0432\u0438\u0442\u0438\u0438. \u0421 2014 \u0433\u043e\u0434\u0430 \u043f\u0440\u043e\u0434\u043e\u043b\u0436\u0430\u0435\u0442\u0441\u044f \u043c\u0430\u0441\u0448\u0442\u0430\u0431\u043d\u0430\u044f \u0440\u0435\u043a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0438\u044f, \u0430\u043a\u0442\u0438\u0432\u043d\u043e \u0437\u0430\u043a\u0443\u043f\u0430\u0435\u0442\u0441\u044f \u0438 \u0432\u0432\u043e\u0434\u0438\u0442\u0441\u044f \u0432 \u044d\u043a\u0441\u043f\u043b\u0443\u0430\u0442\u0430\u0446\u0438\u044e \u043d\u043e\u0432\u043e\u0435 \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u0435."
                            }]
                        }]),
                        i = o[0],
                        l = o[1];
                    (0, t.useEffect)((function () {
                        fetch("/apiaboutUsMainBlockViewSet/?format=json&id=1").then((function (o) {
                            return o.json()
                        })).then((function (o) {
                            return l(o)
                        }))
                    }), []);
                    var r = i[0].sliderList,
                        _ = (0, t.useRef)(null),
                        n = (0, t.useState)(0),
                        s = n[0],
                        a = n[1];
                    return (0, t.useEffect)((function () {
                        return i[0].sliderList.length > 0 && (_.current = new c.Z(".".concat(S("MainBlockSlider__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: S("MainBlockSlider__item"),
                                wrapperClass: S("MainBlockSlider__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(S("MainBlockSlider__arrow_prev")),
                                    nextEl: ".".concat(S("MainBlockSlider__arrow_next"))
                                },
                                on: {
                                    slideChange: function (o) {
                                        a(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                _ && _.current && _.current.destroy()
                            }
                    }), [i]), (0, e.jsx)("div", {
                        className: S("MainBlockSlider"),
                        children: (0, e.jsxs)(d.Z, {
                            className: S("MainBlockSlider__container"),
                            children: [(0, e.jsx)("div", {
                                className: S("MainBlockSlider__titleWrap"),
                                children: (0, e.jsx)("h1", {
                                    className: S("MainBlockSlider__title"),
                                    children: i[0].title
                                })
                            }), (0, e.jsxs)("div", {
                                className: S("MainBlockSlider__contentWrap"),
                                children: [(0, e.jsx)("div", {
                                    className: S("MainBlockSlider__content"),
                                    children: (0, e.jsx)("div", {
                                        className: S("MainBlockSlider__textWrap"),
                                        children: i[0].textList.map((function (o) {
                                            return (0, e.jsx)("div", {
                                                className: S("MainBlockSlider__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: o.text
                                                },
                                            }, o.id)
                                        }))
                                    })
                                }), (0, e.jsxs)("div", {
                                    className: S("MainBlockSlider__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: S("MainBlockSlider__items"),
                                        children: (0, e.jsx)("div", {
                                            className: S("MainBlockSlider__itemsWrap"),
                                            children: r.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: S("MainBlockSlider__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: S("MainBlockSlider__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: S("MainBlockSlider__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: S("MainBlockSlider__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: S("MainBlockSlider__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: S("MainBlockSlider__navigation_currentSlide"),
                                                children: s + 1
                                            }), (0, e.jsxs)("div", {
                                                className: S("MainBlockSlider__navigation_allSlides"),
                                                children: ["/", i[0].sliderList.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: S("MainBlockSlider__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: S("MainBlockSlider__arrow", "MainBlockSlider__arrow_prev", "".concat(S(0 == s ? "MainBlockSlider__arrow_disabled" : "MainBlockSlider__arrow_prev"))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: S("MainBlockSlider__arrow", "MainBlockSlider__arrow_next"),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })]
                        })
                    })
                },
                p = l(30653),
                m = l(10774),
                v = l.n(m);
            c.Z.use([n.Z]);
            var y = a().bind(v()),
                x = function () {
                    var o = (0, t.useState)([{
                            id: 1,
                            number: "32 500",
                            disc: "\u043c\xb2 \u2014 \u043f\u043b\u043e\u0449\u0430\u0434\u044c \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0439 \u0431\u0430\u0437\u044b"
                        }, {
                            id: 2,
                            number: "> 3 500",
                            disc: "\u0438\u0437\u0434\u0435\u043b\u0438\u0439 \u0432 \u0433\u043e\u0434 \u2013 \u043f\u043e\u0442\u0435\u043d\u0446\u0438\u0430\u043b \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430"
                        }, {
                            id: 3,
                            number: "> 30 000",
                            disc: "\u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u043d\u043e\u0433\u043e \u0438 \u043f\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d\u043d\u043e\u0433\u043e \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u044f"
                        }, {
                            id: 4,
                            number: "> 200",
                            disc: "\u0435\u0434\u0438\u043d\u0438\u0446 \u0442\u0435\u0445\u043d\u043e\u043b\u043e\u0433\u0438\u0447\u0435\u0441\u043a\u043e\u0433\u043e \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u044f"
                        }, {
                            id: 5,
                            number: "> 25",
                            disc: "\u0435\u0434\u0438\u043d\u0438\u0446 \u0430\u0442\u0442\u0435\u0441\u0442\u043e\u0432\u0430\u043d\u043d\u043e\u0433\u043e \u0441\u0432\u0430\u0440\u043e\u0447\u043d\u043e\u0433\u043e \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u044f"
                        }, {
                            id: 6,
                            number: "> 15",
                            disc: "\u0430\u0442\u0442\u0435\u0441\u0442\u043e\u0432\u0430\u043d\u043d\u044b\u0445 \u0442\u0435\u0445\u043d\u043e\u043b\u043e\u0433\u0438\u0439 \u0441\u0432\u0430\u0440\u043a\u0438 \u0438 \u043d\u0430\u043f\u043b\u0430\u0432\u043a\u0438"
                        }]),
                        i = o[0],
                        l = o[1];
                    return (0, t.useEffect)((function () {
                        fetch("/apiaboutUsNumberBlockViewSet/?format=json").then((function (o) {
                            return o.json()
                        })).then((function (o) {
                            return l(o)
                        }))
                    }), []), (0, e.jsx)("div", {
                        className: y("AboutCompanyBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: y("AboutCompanyBlock__container"),
                            children: (0, e.jsx)("div", {
                                className: y("AboutCompanyBlock__contentWrap"),
                                children: (0, e.jsx)("div", {
                                    className: y("AboutCompanyBlock__content"),
                                    children: i.map((function (o) {
                                        return (0, e.jsxs)("div", {
                                            className: y("AboutCompanyBlock__textWrap"),
                                            children: [(0, e.jsxs)("div", {
                                                className: y("AboutCompanyBlock__numberWrap"),
                                                children: [(0, e.jsx)("h3", {
                                                    className: y("AboutCompanyBlock__number"),
                                                    children: (0, p.ZP)(o.number)
                                                }), (0, e.jsx)("span", {
                                                    className: y("AboutCompanyBlock__underline")
                                                })]
                                            }), (0, e.jsx)("div", {
                                                className: y("AboutCompanyBlock__fact"),
                                                children: (0, p.ZP)(o.disc)
                                            })]
                                        }, o.id)
                                    }))
                                })
                            })
                        })
                    })
                },
                g = l(78565),
                h = l.n(g);
            c.Z.use([n.Z]);
            var W = a().bind(h()),
                b = function () {
                    var o = (0, t.useState)([{
                            id: 1,
                            title: "\u0412 \u0445\u043e\u0434\u0435 \u0440\u0435\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u0438 \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u044b \u043f\u043e \u043d\u0430\u0440\u0430\u0449\u0438\u0432\u0430\u043d\u0438\u044e \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0433\u043e \u043f\u043e\u0442\u0435\u043d\u0446\u0438\u0430\u043b\u0430 \u0443\u0436\u0435 \u0432\u0432\u0435\u0434\u0435\u043d\u044b \u0432 \u044d\u043a\u0441\u043f\u043b\u0443\u0430\u0442\u0430\u0446\u0438\u044e:",
                            disc: "\u0422\u0430\u043a\u0436\u0435 \u0440\u0430\u0441\u0448\u0438\u0440\u0435\u043d\u044b \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0435 \u043f\u043b\u043e\u0449\u0430\u0434\u0438 \u0434\u043b\u044f \u043e\u0431\u0435\u0441\u043f\u0435\u0447\u0435\u043d\u0438\u044f \u0432\u043e\u0437\u043c\u043e\u0436\u043d\u043e\u0441\u0442\u0438 \u0437\u043d\u0430\u0447\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0433\u043e \u0443\u0432\u0435\u043b\u0438\u0447\u0435\u043d\u0438\u044f \u043e\u0431\u044a\u0435\u043c\u043e\u0432 \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430, \u0441\u043e\u043a\u0440\u0430\u0449\u0435\u043d\u0438\u044f \u0441\u0440\u043e\u043a\u043e\u0432 \u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u044f \u0438 \u043e\u0441\u0432\u043e\u0435\u043d\u0438\u044f \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430, \u0432 \u0440\u0430\u043c\u043a\u0430\u0445 \u043f\u0440\u043e\u0433\u0440\u0430\u043c\u043c\u044b \u043f\u043e \u0438\u043c\u043f\u043e\u0440\u0442\u043e\u0437\u0430\u043c\u0435\u0449\u0435\u043d\u0438\u044e \u043d\u043e\u0432\u044b\u0445 \u0432\u0438\u0434\u043e\u0432 \u043f\u0440\u043e\u0434\u0443\u043a\u0446\u0438\u0438, \u0442\u0430\u043a\u0438\u0445 \u043a\u0430\u043a \u0448\u0430\u0440\u043e\u0432\u044b\u0435 \u043a\u0440\u0430\u043d\u044b \u0432\u044b\u0441\u043e\u043a\u043e\u0442\u0435\u043c\u043f\u0435\u0440\u0430\u0442\u0443\u0440\u043d\u043e\u0433\u043e \u0438\u0441\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f DN 1000 \u0438 \u0431\u043e\u043b\u0435\u0435.",
                            sliderList: [{
                                id: 1,
                                aboutUsSecondBlock: 1,
                                imgupload: "http://0.0.0.0:8000/main/static/static/img/aboutUsSecondBlockSlider/image-1.png",
                                disc: "\u0426\u0435\u0445 \u0434\u043b\u044f \u043e\u0441\u0432\u043e\u0435\u043d\u0438\u044f \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430 \u0448\u0430\u0440\u043e\u0432\u044b\u0445 \u043a\u0440\u0430\u043d\u043e\u0432 DN 1000 \u0438 \u0431\u043e\u043b\u0435\u0435"
                            }, {
                                id: 2,
                                aboutUsSecondBlock: 1,
                                imgupload: "http://0.0.0.0:8000/main/static/static/img/aboutUsSecondBlockSlider/image-2.png",
                                disc: "\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0439 \u0443\u0447\u0430\u0441\u0442\u043e\u043a \u043f\u043b\u0430\u0437\u043c\u0435\u043d\u043d\u043e\u0439 \u0438 \u0433\u0430\u0437\u043e\u0432\u043e\u0439 \u0440\u0435\u0437\u043a\u0438"
                            }, {
                                id: 3,
                                aboutUsSecondBlock: 1,
                                imgupload: "http://0.0.0.0:8000/main/static/static/img/aboutUsSecondBlockSlider/image-3.png",
                                disc: "\u0423\u0447\u0430\u0441\u0442\u043e\u043a \u0432\u044b\u0441\u043e\u043a\u043e\u0441\u043a\u0440\u043e\u0441\u0442\u043d\u043e\u0433\u043e \u0433\u0430\u0437\u0435\u0442\u0435\u0440\u043c\u0438\u0447\u0435\u0441\u043a\u043e\u0433\u043e \u043d\u0430\u043f\u044b\u043b\u0435\u043d\u0438\u044f \u043f\u043e\u043a\u0440\u044b\u0442\u0438\u0439"
                            }, {
                                id: 4,
                                aboutUsSecondBlock: 1,
                                imgupload: "http://0.0.0.0:8000/main/static/static/img/aboutUsSecondBlockSlider/image-4.png",
                                disc: "\u041c\u043e\u0434\u0435\u0440\u043d\u0438\u0437\u0438\u0440\u043e\u0432\u0430\u043d\u0430 \u0438 \u0440\u0430\u0441\u0448\u0438\u0440\u0435\u043d\u0430 \u043e\u0431\u043b\u0430\u0441\u0442\u044c \u0430\u043a\u043a\u0440\u0435\u0434\u0438\u0442\u0430\u0446\u0438\u0438 \u0446\u0435\u043d\u0442\u0440\u0430\u043b\u044c\u043d\u043e\u0439 \u0437\u0430\u0432\u043e\u0434\u0441\u043a\u043e\u0439 \u043b\u0430\u0431\u043e\u0440\u0430\u0442\u043e\u0440\u0438\u0438"
                            }]
                        }]),
                        i = o[0],
                        l = o[1];
                    (0, t.useEffect)((function () {
                        fetch("/apiaboutUsSecondBlockViewSet/?format=json&id=1").then((function (o) {
                            return o.json()
                        })).then((function (o) {
                            return l(o)
                        }))
                    }), []);
                    var r = (0, t.useRef)(null);
                    return (0, t.useEffect)((function () {
                        return r.current = new c.Z(".".concat(W("AboutPotentialBlock__achievements")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: W("AboutPotentialBlock__achievement"),
                                wrapperClass: W("AboutPotentialBlock__achievementsWrap"),
                                navigation: {
                                    prevEl: ".".concat(W("AboutPotentialBlock__arrow_prev")),
                                    nextEl: ".".concat(W("AboutPotentialBlock__arrow_next"))
                                },
                                observer: !0
                            }),
                            function () {
                                r && r.current && r.current.destroy()
                            }
                    }), [i]), (0, e.jsx)("div", {
                        className: W("AboutPotentialBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: W("Header__container"),
                            children: (0, e.jsxs)("div", {
                                className: W("AboutPotentialBlock__contentWrap"),
                                children: [(0, e.jsx)("div", {
                                    className: W("AboutPotentialBlock__content")
                                }), (0, e.jsxs)("div", {
                                    className: W("AboutPotentialBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: W("AboutPotentialBlock__navigationBlock"),
                                        children: (0, e.jsx)("div", {
                                            className: W("AboutPotentialBlock__navigationTitleWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: W("AboutPotentialBlock__navigationTitle"),
                                                children: i[0].title
                                            })
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: W("AboutPotentialBlock__achievements"),
                                        children: [(0, e.jsx)("div", {
                                            className: W("AboutPotentialBlock__achievementsWrap"),
                                            children: i[0].sliderList.map((function (o) {
                                                return (0, e.jsxs)("div", {
                                                    className: W("AboutPotentialBlock__achievement"),
                                                    children: [(0, e.jsx)("div", {
                                                        className: W("AboutPotentialBlock__achievementImageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: W("AboutPotentialBlock__achievementImage"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    }), (0, e.jsx)("div", {
                                                        className: W("AboutPotentialBlock__achievementDescription"),
                                                        children: o.disc
                                                    })] 
                                                }, o.id)
                                            }))
                                        }), (0, e.jsx)("div", {
                                            className: W("AboutPotentialBlock__description"),
                                            dangerouslySetInnerHTML:{
                                                __html: i[0].disc
                                            }
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                j = l(82050),
                N = l(5563),
                w = l(42840),
                A = l.n(w);
            c.Z.use([n.Z]);
            var f = a().bind(A()),
                C = function (o) {
                    var i = o.title,
                        l = o.anchor,
                        r = o.description,
                        _ = o.sliderList,
                        n = "".concat(l, "__arrow"),
                        s = "".concat(l, "__arrow_prev"),
                        a = (0, t.useRef)(null),
                        u = (0, t.useState)(0),
                        k = u[0],
                        S = u[1];
                    return (0, t.useEffect)((function () {
                        return _.length > 0 && (a.current = new c.Z(".".concat(f("ProductionSliderBlock__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: f("ProductionSliderBlock__item"),
                                wrapperClass: f("ProductionSliderBlock__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(f(s)),
                                    nextEl: ".".concat(f(n))
                                },
                                on: {
                                    slideChange: function (o) {
                                        S(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                a && a.current && a.current.destroy()
                            }
                    }), [_]), (0, e.jsx)("div", {
                        className: f("ProductionSliderBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: f("ProductionSliderBlock__container"),
                            children: (0, e.jsxs)("div", {
                                className: f("ProductionSliderBlock__contentWrap"),
                                children: [(0, e.jsxs)("div", {
                                    className: f("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        id: l,
                                        className: f("ProductionSliderBlock__titleWrap"),
                                        children: (0, e.jsx)(N.Z, {
                                            className: f("ProductionSliderBlock__secondTitle"),
                                            level: "3",
                                            children: (0, p.ZP)(i)
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: f("ProductionSliderBlock__textWrap"),
                                        children: [(0, e.jsx)("span", {
                                            className: f("ProductionSliderBlock__underline")
                                        }), (0, e.jsx)("div", {
                                            className: f("ProductionSliderBlock__descriptionWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: f("ProductionSliderBlock__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: r
                                                }
                                            })
                                        })]
                                    })]
                                }), (0, e.jsxs)("div", {
                                    className: f("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: f("ProductionSliderBlock__items"),
                                        children: (0, e.jsx)("div", {
                                            className: f("ProductionSliderBlock__itemsWrap"),
                                            children: _.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: f("ProductionSliderBlock__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: f("ProductionSliderBlock__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: f("ProductionSliderBlock__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: f("ProductionSliderBlock__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: f("ProductionSliderBlock__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: f("ProductionSliderBlock__navigation_currentSlide"),
                                                children: k + 1
                                            }), (0, e.jsxs)("div", {
                                                className: f("ProductionSliderBlock__navigation_allSlides"),
                                                children: ["/", _.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: f("ProductionSliderBlock__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: f("ProductionSliderBlock__arrow", s, "".concat(f(0 == k ? "ProductionSliderBlock__arrow_disabled" : s))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: f("ProductionSliderBlock__arrow", n),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                M = l(10924),
                Z = l.n(M);
            c.Z.use([n.Z]);
            var T = a().bind(Z()),
                L = function (o) {
                    var i = o.title,
                        l = o.anchor,
                        r = o.description,
                        _ = o.sliderList,
                        n = "".concat(l, "__arrow_next_1"),
                        s = "".concat(l, "__arrow_prev_1"),
                        a = (0, t.useRef)(null),
                        u = (0, t.useState)(0),
                        k = u[0],
                        S = u[1];
                    return (0, t.useEffect)((function () {
                        return _.length > 0 && (a.current = new c.Z(".".concat(T("ProductionSliderBlock__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: T("ProductionSliderBlock__item"),
                                wrapperClass: T("ProductionSliderBlock__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(T(s)),
                                    nextEl: ".".concat(T(n))
                                },
                                on: {
                                    slideChange: function (o) {
                                        S(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                a && a.current && a.current.destroy()
                            }
                    }), [_]), (0, e.jsx)("div", {
                        className: T("ProductionSliderBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: T("ProductionSliderBlock__container"),
                            children: (0, e.jsxs)("div", {
                                className: T("ProductionSliderBlock__contentWrap"),
                                children: [(0, e.jsxs)("div", {
                                    className: T("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        id: l,
                                        className: T("ProductionSliderBlock__titleWrap"),
                                        children: (0, e.jsx)(N.Z, {
                                            className: T("ProductionSliderBlock__secondTitle"),
                                            level: "3",
                                            children: (0, p.ZP)(i)
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: T("ProductionSliderBlock__textWrap"),
                                        children: [(0, e.jsx)("span", {
                                            className: T("ProductionSliderBlock__underline")
                                        }), (0, e.jsx)("div", {
                                            className: T("ProductionSliderBlock__descriptionWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: T("ProductionSliderBlock__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: r
                                                }
                                            })
                                        })]
                                    })]
                                }), (0, e.jsxs)("div", {
                                    className: T("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: T("ProductionSliderBlock__items"),
                                        children: (0, e.jsx)("div", {
                                            className: T("ProductionSliderBlock__itemsWrap"),
                                            children: _.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: T("ProductionSliderBlock__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: T("ProductionSliderBlock__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: T("ProductionSliderBlock__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: T("ProductionSliderBlock__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: T("ProductionSliderBlock__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: T("ProductionSliderBlock__navigation_currentSlide"),
                                                children: k + 1
                                            }), (0, e.jsxs)("div", {
                                                className: T("ProductionSliderBlock__navigation_allSlides"),
                                                children: ["/", _.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: T("ProductionSliderBlock__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: T("ProductionSliderBlock__arrow", s, "".concat(T(0 == k ? "ProductionSliderBlock__arrow_disabled" : s))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: T("ProductionSliderBlock__arrow", n),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                E = l(7585),
                U = l.n(E);
            c.Z.use([n.Z]);
            var I = a().bind(U()),
                V = function (o) {
                    var i = o.title,
                        l = o.anchor,
                        r = o.description,
                        _ = o.sliderList,
                        n = "".concat(l, "__arrow_next_2"),
                        s = "".concat(l, "__arrow_prev_2"),
                        a = (0, t.useRef)(null),
                        u = (0, t.useState)(0),
                        k = u[0],
                        S = u[1];
                    return (0, t.useEffect)((function () {
                        return _.length > 0 && (a.current = new c.Z(".".concat(I("ProductionSliderBlock__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: I("ProductionSliderBlock__item"),
                                wrapperClass: I("ProductionSliderBlock__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(I(s)),
                                    nextEl: ".".concat(I(n))
                                },
                                on: {
                                    slideChange: function (o) {
                                        S(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                a && a.current && a.current.destroy()
                            }
                    }), [_]), (0, e.jsx)("div", {
                        className: I("ProductionSliderBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: I("ProductionSliderBlock__container"),
                            children: (0, e.jsxs)("div", {
                                className: I("ProductionSliderBlock__contentWrap"),
                                children: [(0, e.jsxs)("div", {
                                    className: I("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        id: l,
                                        className: I("ProductionSliderBlock__titleWrap"),
                                        children: (0, e.jsx)(N.Z, {
                                            className: I("ProductionSliderBlock__secondTitle"),
                                            level: "3",
                                            children: (0, p.ZP)(i)
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: I("ProductionSliderBlock__textWrap"),
                                        children: [(0, e.jsx)("span", {
                                            className: I("ProductionSliderBlock__underline")
                                        }), (0, e.jsx)("div", {
                                            className: I("ProductionSliderBlock__descriptionWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: I("ProductionSliderBlock__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: r
                                                }
                                            })
                                        })]
                                    })]
                                }), (0, e.jsxs)("div", {
                                    className: I("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: I("ProductionSliderBlock__items"),
                                        children: (0, e.jsx)("div", {
                                            className: I("ProductionSliderBlock__itemsWrap"),
                                            children: _.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: I("ProductionSliderBlock__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: I("ProductionSliderBlock__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: I("ProductionSliderBlock__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: I("ProductionSliderBlock__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: I("ProductionSliderBlock__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: I("ProductionSliderBlock__navigation_currentSlide"),
                                                children: k + 1
                                            }), (0, e.jsxs)("div", {
                                                className: I("ProductionSliderBlock__navigation_allSlides"),
                                                children: ["/", _.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: I("ProductionSliderBlock__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: I("ProductionSliderBlock__arrow", s, "".concat(I(0 == k ? "ProductionSliderBlock__arrow_disabled" : s))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: I("ProductionSliderBlock__arrow", n),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                F = l(54022),
                O = l.n(F);
            c.Z.use([n.Z]);
            var D = a().bind(O()),
                H = function (o) {
                    var i = o.title,
                        l = o.anchor,
                        r = o.description,
                        _ = o.sliderList,
                        n = "".concat(l, "__arrow_next_3"),
                        s = "".concat(l, "__arrow_prev_3"),
                        a = (0, t.useRef)(null),
                        u = (0, t.useState)(0),
                        k = u[0],
                        S = u[1];
                    return (0, t.useEffect)((function () {
                        return _.length > 0 && (a.current = new c.Z(".".concat(D("ProductionSliderBlock__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: D("ProductionSliderBlock__item"),
                                wrapperClass: D("ProductionSliderBlock__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(D(s)),
                                    nextEl: ".".concat(D(n))
                                },
                                on: {
                                    slideChange: function (o) {
                                        S(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                a && a.current && a.current.destroy()
                            }
                    }), [_]), (0, e.jsx)("div", {
                        className: D("ProductionSliderBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: D("ProductionSliderBlock__container"),
                            children: (0, e.jsxs)("div", {
                                className: D("ProductionSliderBlock__contentWrap"),
                                children: [(0, e.jsxs)("div", {
                                    className: D("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        id: l,
                                        className: D("ProductionSliderBlock__titleWrap"),
                                        children: (0, e.jsx)(N.Z, {
                                            className: D("ProductionSliderBlock__secondTitle"),
                                            level: "3",
                                            children: (0, p.ZP)(i)
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: D("ProductionSliderBlock__textWrap"),
                                        children: [(0, e.jsx)("span", {
                                            className: D("ProductionSliderBlock__underline")
                                        }), (0, e.jsx)("div", {
                                            className: D("ProductionSliderBlock__descriptionWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: D("ProductionSliderBlock__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: r
                                                }
                                            })
                                        })]
                                    })]
                                }), (0, e.jsxs)("div", {
                                    className: D("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: D("ProductionSliderBlock__items"),
                                        children: (0, e.jsx)("div", {
                                            className: D("ProductionSliderBlock__itemsWrap"),
                                            children: _.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: D("ProductionSliderBlock__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: D("ProductionSliderBlock__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: D("ProductionSliderBlock__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: D("ProductionSliderBlock__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: D("ProductionSliderBlock__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: D("ProductionSliderBlock__navigation_currentSlide"),
                                                children: k + 1
                                            }), (0, e.jsxs)("div", {
                                                className: D("ProductionSliderBlock__navigation_allSlides"),
                                                children: ["/", _.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: D("ProductionSliderBlock__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: D("ProductionSliderBlock__arrow", s, "".concat(D(0 == k ? "ProductionSliderBlock__arrow_disabled" : s))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: D("ProductionSliderBlock__arrow", n),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                G = l(72606),
                q = l.n(G);
            c.Z.use([n.Z]);
            var Q = a().bind(q()),
                R = function (o) {
                    var i = o.title,
                        l = o.anchor,
                        r = o.description,
                        _ = o.sliderList,
                        n = "".concat(l, "__arrow_next_4"),
                        s = "".concat(l, "__arrow_prev_4"),
                        a = (0, t.useRef)(null),
                        u = (0, t.useState)(0),
                        k = u[0],
                        S = u[1];
                    return (0, t.useEffect)((function () {
                        return _.length > 0 && (a.current = new c.Z(".".concat(Q("ProductionSliderBlock__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: Q("ProductionSliderBlock__item"),
                                wrapperClass: Q("ProductionSliderBlock__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(Q(s)),
                                    nextEl: ".".concat(Q(n))
                                },
                                on: {
                                    slideChange: function (o) {
                                        S(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                a && a.current && a.current.destroy()
                            }
                    }), [_]), (0, e.jsx)("div", {
                        className: Q("ProductionSliderBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: Q("ProductionSliderBlock__container"),
                            children: (0, e.jsxs)("div", {
                                className: Q("ProductionSliderBlock__contentWrap"),
                                children: [(0, e.jsxs)("div", {
                                    className: Q("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        id: l,
                                        className: Q("ProductionSliderBlock__titleWrap"),
                                        children: (0, e.jsx)(N.Z, {
                                            className: Q("ProductionSliderBlock__secondTitle"),
                                            level: "3",
                                            children: (0, p.ZP)(i)
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: Q("ProductionSliderBlock__textWrap"),
                                        children: [(0, e.jsx)("span", {
                                            className: Q("ProductionSliderBlock__underline")
                                        }), (0, e.jsx)("div", {
                                            className: Q("ProductionSliderBlock__descriptionWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: Q("ProductionSliderBlock__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: r
                                                }
                                            })
                                        })]
                                    })]
                                }), (0, e.jsxs)("div", {
                                    className: Q("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: Q("ProductionSliderBlock__items"),
                                        children: (0, e.jsx)("div", {
                                            className: Q("ProductionSliderBlock__itemsWrap"),
                                            children: _.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: Q("ProductionSliderBlock__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: Q("ProductionSliderBlock__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: Q("ProductionSliderBlock__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: Q("ProductionSliderBlock__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: Q("ProductionSliderBlock__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: Q("ProductionSliderBlock__navigation_currentSlide"),
                                                children: k + 1
                                            }), (0, e.jsxs)("div", {
                                                className: Q("ProductionSliderBlock__navigation_allSlides"),
                                                children: ["/", _.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: Q("ProductionSliderBlock__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: Q("ProductionSliderBlock__arrow", s, "".concat(Q(0 == k ? "ProductionSliderBlock__arrow_disabled" : s))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: Q("ProductionSliderBlock__arrow", n),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                z = l(72781),
                J = l.n(z);
            c.Z.use([n.Z]);
            var X = a().bind(J()),
                K = function (o) {
                    var i = o.title,
                        l = o.anchor,
                        r = o.description,
                        _ = o.sliderList,
                        n = "".concat(l, "__arrow_next_5"),
                        s = "".concat(l, "__arrow_prev_5"),
                        a = (0, t.useRef)(null),
                        u = (0, t.useState)(0),
                        k = u[0],
                        S = u[1];
                    return (0, t.useEffect)((function () {
                        return _.length > 0 && (a.current = new c.Z(".".concat(X("ProductionSliderBlock__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: X("ProductionSliderBlock__item"),
                                wrapperClass: X("ProductionSliderBlock__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(X(s)),
                                    nextEl: ".".concat(X(n))
                                },
                                on: {
                                    slideChange: function (o) {
                                        S(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                a && a.current && a.current.destroy()
                            }
                    }), [_]), (0, e.jsx)("div", {
                        className: X("ProductionSliderBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: X("ProductionSliderBlock__container"),
                            children: (0, e.jsxs)("div", {
                                className: X("ProductionSliderBlock__contentWrap"),
                                children: [(0, e.jsxs)("div", {
                                    className: X("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        id: l,
                                        className: X("ProductionSliderBlock__titleWrap"),
                                        children: (0, e.jsx)(N.Z, {
                                            className: X("ProductionSliderBlock__secondTitle"),
                                            level: "3",
                                            children: (0, p.ZP)(i)
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: X("ProductionSliderBlock__textWrap"),
                                        children: [(0, e.jsx)("span", {
                                            className: X("ProductionSliderBlock__underline")
                                        }), (0, e.jsx)("div", {
                                            className: X("ProductionSliderBlock__descriptionWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: X("ProductionSliderBlock__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: r
                                                }
                                            })
                                        })]
                                    })]
                                }), (0, e.jsxs)("div", {
                                    className: X("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: X("ProductionSliderBlock__items"),
                                        children: (0, e.jsx)("div", {
                                            className: X("ProductionSliderBlock__itemsWrap"),
                                            children: _.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: X("ProductionSliderBlock__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: X("ProductionSliderBlock__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: X("ProductionSliderBlock__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: X("ProductionSliderBlock__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: X("ProductionSliderBlock__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: X("ProductionSliderBlock__navigation_currentSlide"),
                                                children: k + 1
                                            }), (0, e.jsxs)("div", {
                                                className: X("ProductionSliderBlock__navigation_allSlides"),
                                                children: ["/", _.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: X("ProductionSliderBlock__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: X("ProductionSliderBlock__arrow", s, "".concat(X(0 == k ? "ProductionSliderBlock__arrow_disabled" : s))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: X("ProductionSliderBlock__arrow", n),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                Y = l(27819),
                $ = l.n(Y);
            c.Z.use([n.Z]);
            var oo = a().bind($()),
                io = function (o) {
                    var i = o.title,
                        l = o.anchor,
                        r = o.description,
                        _ = o.sliderList,
                        n = "".concat(l, "__arrow_next_6"),
                        s = "".concat(l, "__arrow_prev_6"),
                        a = (0, t.useRef)(null),
                        u = (0, t.useState)(0),
                        k = u[0],
                        S = u[1];
                    return (0, t.useEffect)((function () {
                        return _.length > 0 && (a.current = new c.Z(".".concat(oo("ProductionSliderBlock__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: oo("ProductionSliderBlock__item"),
                                wrapperClass: oo("ProductionSliderBlock__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(oo(s)),
                                    nextEl: ".".concat(oo(n))
                                },
                                on: {
                                    slideChange: function (o) {
                                        S(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                a && a.current && a.current.destroy()
                            }
                    }), [_]), (0, e.jsx)("div", {
                        className: oo("ProductionSliderBlock"),
                        children: (0, e.jsx)(d.Z, {
                            className: oo("ProductionSliderBlock__container"),
                            children: (0, e.jsxs)("div", {
                                className: oo("ProductionSliderBlock__contentWrap"),
                                children: [(0, e.jsxs)("div", {
                                    className: oo("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        id: l,
                                        className: oo("ProductionSliderBlock__titleWrap"),
                                        children: (0, e.jsx)(N.Z, {
                                            className: oo("ProductionSliderBlock__secondTitle"),
                                            level: "3",
                                            children: (0, p.ZP)(i)
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: oo("ProductionSliderBlock__textWrap"),
                                        children: [(0, e.jsx)("span", {
                                            className: oo("ProductionSliderBlock__underline")
                                        }), (0, e.jsx)("div", {
                                            className: oo("ProductionSliderBlock__descriptionWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: oo("ProductionSliderBlock__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: r
                                                }
                                            })
                                        })]
                                    })]
                                }), (0, e.jsxs)("div", {
                                    className: oo("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: oo("ProductionSliderBlock__items"),
                                        children: (0, e.jsx)("div", {
                                            className: oo("ProductionSliderBlock__itemsWrap"),
                                            children: _.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: oo("ProductionSliderBlock__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: oo("ProductionSliderBlock__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: oo("ProductionSliderBlock__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: oo("ProductionSliderBlock__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: oo("ProductionSliderBlock__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: oo("ProductionSliderBlock__navigation_currentSlide"),
                                                children: k + 1
                                            }), (0, e.jsxs)("div", {
                                                className: oo("ProductionSliderBlock__navigation_allSlides"),
                                                children: ["/", _.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: oo("ProductionSliderBlock__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: oo("ProductionSliderBlock__arrow", s, "".concat(oo(0 == k ? "ProductionSliderBlock__arrow_disabled" : s))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: oo("ProductionSliderBlock__arrow", n),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })
                        })
                    })
                },
                lo = l(19740),
                eo = l.n(lo);
            c.Z.use([n.Z]);
            var to = a().bind(eo()),
                ro = function (o) {
                    var i = o.title,
                        l = o.anchor,
                        r = o.description,
                        _ = o.sliderList,
                        n = "".concat(l, "__arrow_next_7"),
                        s = "".concat(l, "__arrow_prev_7"),
                        a = (0, t.useRef)(null),
                        u = (0, t.useState)(0),
                        k = u[0],
                        S = u[1];
                    return (0, t.useEffect)((function () {
                        return _.length > 0 && (a.current = new c.Z(".".concat(to("ProductionSliderBlock__items")), {
                                loop: !1,
                                slidesPerView: "auto",
                                slideClass: to("ProductionSliderBlock__item"),
                                wrapperClass: to("ProductionSliderBlock__itemsWrap"),
                                navigation: {
                                    prevEl: ".".concat(to(s)),
                                    nextEl: ".".concat(to(n))
                                },
                                on: {
                                    slideChange: function (o) {
                                        S(o.realIndex)
                                    }
                                }
                            })),
                            function () {
                                a && a.current && a.current.destroy()
                            }
                    }), [_]), (0, e.jsx)("div", {
                        className: to("ProductionSliderBlock"),
                        children: (0, e.jsxs)(d.Z, {
                            className: to("ProductionSliderBlock__container"),
                            children: [(0, e.jsx)("div", {
                                children: (0, e.jsx)("h2", {
                                    className: to("ProductionSliderBlock__title"),
                                    children: "\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0435\u043d\u043d\u0430\u044f \u0431\u0430\u0437\u0430"
                                })
                            }), (0, e.jsxs)("div", {
                                className: to("ProductionSliderBlock__contentWrap"),
                                children: [(0, e.jsxs)("div", {
                                    className: to("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        id: l,
                                        className: to("ProductionSliderBlock__titleWrap"),
                                        children: (0, e.jsx)(N.Z, {
                                            className: to("ProductionSliderBlock__secondTitle"),
                                            level: "3",
                                            children: (0, p.ZP)(i)
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: to("ProductionSliderBlock__textWrap"),
                                        children: [(0, e.jsx)("span", {
                                            className: to("ProductionSliderBlock__underline")
                                        }), (0, e.jsx)("div", {
                                            className: to("ProductionSliderBlock__descriptionWrap"),
                                            children: (0, e.jsx)("div", {
                                                className: to("ProductionSliderBlock__description"),
                                                dangerouslySetInnerHTML: {
                                                    __html: r
                                                }
                                            })
                                        })]
                                    })]
                                }), (0, e.jsxs)("div", {
                                    className: to("ProductionSliderBlock__content"),
                                    children: [(0, e.jsx)("div", {
                                        className: to("ProductionSliderBlock__items"),
                                        children: (0, e.jsx)("div", {
                                            className: to("ProductionSliderBlock__itemsWrap"),
                                            children: _.map((function (o) {
                                                return (0, e.jsx)("div", {
                                                    className: to("ProductionSliderBlock__item"),
                                                    children: (0, e.jsx)("div", {
                                                        className: to("ProductionSliderBlock__imageWrap"),
                                                        children: (0, e.jsx)("img", {
                                                            className: to("ProductionSliderBlock__image"),
                                                            src: o.imgupload,
                                                            alt: o.imgupload
                                                        })
                                                    })
                                                }, o.id)
                                            }))
                                        })
                                    }), (0, e.jsxs)("div", {
                                        className: to("ProductionSliderBlock__navigationWrap"),
                                        children: [(0, e.jsxs)("div", {
                                            className: to("ProductionSliderBlock__navigation"),
                                            children: [(0, e.jsx)("div", {
                                                className: to("ProductionSliderBlock__navigation_currentSlide"),
                                                children: k + 1
                                            }), (0, e.jsxs)("div", {
                                                className: to("ProductionSliderBlock__navigation_allSlides"),
                                                children: ["/", _.length]
                                            })]
                                        }), (0, e.jsxs)("div", {
                                            className: to("ProductionSliderBlock__arrowWrap"),
                                            children: [(0, e.jsx)(B.Z, {
                                                className: to("ProductionSliderBlock__arrow", s, "".concat(to(0 == k ? "ProductionSliderBlock__arrow_disabled" : s))),
                                                type: "left"
                                            }), (0, e.jsx)(B.Z, {
                                                className: to("ProductionSliderBlock__arrow", n),
                                                type: "right"
                                            })]
                                        })]
                                    })]
                                })]
                            })]
                        })
                    })
                },
                _o = l(81095),
                co = l(41582),
                no = l.n(co),
                so = a().bind(no());

            function ao() {
                var o = (0, t.useState)([{
                        id: 1,
                        anchor: "",
                        title: "",
                        description: "",
                        sliderList: [{
                            id: 1,
                            aboutUsProdBase: 1,
                            imgupload: ""
                        }]
                    }]),
                    i = o[0],
                    l = o[1];
                return (0, t.useEffect)((function () {
                    fetch("/apiaboutUsProdBase/?format=json").then((function (o) {
                        return o.json()
                    })).then((function (o) {
                        return l(o)
                    }))
                }), []), (0, e.jsxs)("div", {
                    className: no().container,
                    children: [(0, e.jsx)(r.default, {
                        children: (0, e.jsx)("title", {
                            children: _o.TP.title
                        })
                    }), (0, e.jsxs)(_.Z, {
                        className: so("MainPage"),
                        children: [(0, e.jsx)(P, {}), (0, e.jsx)(x, {}), (0, e.jsx)(e.Fragment, {
                            children: void 0 == i[0] ? (0, e.jsx)(e.Fragment, {}) : (0, e.jsx)(e.Fragment, {
                                children: (0, e.jsx)(ro, {
                                    title: i[0].title,
                                    description: i[0].description,
                                    anchor: i[0].anchor,
                                    sliderList: i[0].sliderList
                                })
                            })
                        }), (0, e.jsx)(e.Fragment, {
                            children: void 0 == i[1] ? (0, e.jsx)(e.Fragment, {}) : (0, e.jsx)(e.Fragment, {
                                children: (0, e.jsx)(L, {
                                    title: i[1].title,
                                    description: i[1].description,
                                    anchor: i[1].anchor,
                                    sliderList: i[1].sliderList
                                })
                            })
                        }), (0, e.jsx)(e.Fragment, {
                            children: void 0 == i[2] ? (0, e.jsx)(e.Fragment, {}) : (0, e.jsx)(e.Fragment, {
                                children: (0, e.jsx)(V, {
                                    title: i[2].title,
                                    description: i[2].description,
                                    anchor: i[2].anchor,
                                    sliderList: i[2].sliderList
                                })
                            })
                        }), (0, e.jsx)(e.Fragment, {
                            children: void 0 == i[3] ? (0, e.jsx)(e.Fragment, {}) : (0, e.jsx)(e.Fragment, {
                                children: (0, e.jsx)(R, {
                                    title: i[3].title,
                                    description: i[3].description,
                                    anchor: i[3].anchor,
                                    sliderList: i[3].sliderList
                                })
                            })
                        }), (0, e.jsx)(e.Fragment, {
                            children: void 0 == i[4] ? (0, e.jsx)(e.Fragment, {}) : (0, e.jsx)(e.Fragment, {
                                children: (0, e.jsx)(H, {
                                    title: i[4].title,
                                    description: i[4].description,
                                    anchor: i[4].anchor,
                                    sliderList: i[4].sliderList
                                })
                            })
                        }), (0, e.jsx)(e.Fragment, {
                            children: void 0 == i[5] ? (0, e.jsx)(e.Fragment, {}) : (0, e.jsx)(e.Fragment, {
                                children: (0, e.jsx)(io, {
                                    title: i[5].title,
                                    description: i[5].description,
                                    anchor: i[5].anchor,
                                    sliderList: i[5].sliderList
                                })
                            })
                        }), (0, e.jsx)(e.Fragment, {
                            children: void 0 == i[6] ? (0, e.jsx)(e.Fragment, {}) : (0, e.jsx)(e.Fragment, {
                                children: (0, e.jsx)(K, {
                                    title: i[6].title,
                                    description: i[6].description,
                                    anchor: i[6].anchor,
                                    sliderList: i[6].sliderList
                                })
                            })
                        }), (0, e.jsx)(e.Fragment, {
                            children: void 0 == i[7] ? (0, e.jsx)(e.Fragment, {}) : (0, e.jsx)(e.Fragment, {
                                children: (0, e.jsx)(C, {
                                    title: i[7].title,
                                    description: i[7].description,
                                    anchor: i[7].anchor,
                                    sliderList: i[7].sliderList
                                })
                            })
                        }), (0, e.jsx)(b, {}), (0, e.jsx)(j.Z, {
                            title: _o.er.titleLong,
                            titleDescription: _o.er.titleDescription,
                            button: _o.er.button
                        })]
                    })]
                })
            }
        },
        69823: function (o, i, l) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/about-us", function () {
                return l(19299)
            }])
        },
        97085: function (o) {
            o.exports = {
                Arrow: "styles_Arrow__1qcSu",
                Arrow_disabled: "styles_Arrow_disabled__2lel5",
                Arrow_white: "styles_Arrow_white__s62dv",
                Arrow_blue: "styles_Arrow_blue__2rF_U"
            }
        },
        10774: function (o) {
            o.exports = {
                AboutCompanyBlock__linkWrapp: "styles_AboutCompanyBlock__linkWrapp__3phSn",
                AboutCompanyBlock__index: "styles_AboutCompanyBlock__index__an6C_",
                AboutCompanyBlock__titleWrap: "styles_AboutCompanyBlock__titleWrap__2wLxf",
                AboutCompanyBlock__title: "styles_AboutCompanyBlock__title__2aKzq",
                AboutCompanyBlock__contentWrap: "styles_AboutCompanyBlock__contentWrap__ZbOPW",
                AboutCompanyBlock__content: "styles_AboutCompanyBlock__content__31iyM",
                AboutCompanyBlock__sliderBlock: "styles_AboutCompanyBlock__sliderBlock__10Dx7",
                AboutCompanyBlock__descriptionBlock: "styles_AboutCompanyBlock__descriptionBlock__SfVq5",
                AboutCompanyBlock__textWrap: "styles_AboutCompanyBlock__textWrap__2DWGq",
                AboutCompanyBlock__fact: "styles_AboutCompanyBlock__fact__a9P_l",
                AboutCompanyBlock__description: "styles_AboutCompanyBlock__description__3BS93",
                AboutCompanyBlock__numberWrap: "styles_AboutCompanyBlock__numberWrap__pGFtc",
                AboutCompanyBlock__number: "styles_AboutCompanyBlock__number__2P-EW",
                AboutCompanyBlock__underline: "styles_AboutCompanyBlock__underline__Om80a",
                AboutCompanyBlock__items: "styles_AboutCompanyBlock__items__2W2eS",
                AboutCompanyBlock__itemsWrap: "styles_AboutCompanyBlock__itemsWrap__Vwn1A",
                AboutCompanyBlock__item: "styles_AboutCompanyBlock__item__QzW_d",
                AboutCompanyBlock__textImageWrap: "styles_AboutCompanyBlock__textImageWrap__1SvLp",
                AboutCompanyBlock__imageWrap: "styles_AboutCompanyBlock__imageWrap__3txvZ",
                AboutCompanyBlock__image: "styles_AboutCompanyBlock__image__1Efsd",
                AboutCompanyBlock__linkWrap: "styles_AboutCompanyBlock__linkWrap__2SeVm",
                AboutCompanyBlock__link: "styles_AboutCompanyBlock__link__dBhmC",
                AboutCompanyBlock__arrowSingle: "styles_AboutCompanyBlock__arrowSingle__2X98l",
                AboutCompanyBlock__navigationWrap: "styles_AboutCompanyBlock__navigationWrap__1L2il",
                AboutCompanyBlock__navigation: "styles_AboutCompanyBlock__navigation__4yFdB",
                AboutCompanyBlock__navigation_currentSlide: "styles_AboutCompanyBlock__navigation_currentSlide__1nBuA",
                AboutCompanyBlock__navigation_allSlides: "styles_AboutCompanyBlock__navigation_allSlides__TEcGb",
                AboutCompanyBlock__arrowWrap: "styles_AboutCompanyBlock__arrowWrap__uJLch",
                AboutCompanyBlock__arrow_next: "styles_AboutCompanyBlock__arrow_next__3QlZr",
                AboutCompanyBlock__arrow_disabled: "styles_AboutCompanyBlock__arrow_disabled__3U4Qi",
                AboutCompanyBlock__iconsWrap: "styles_AboutCompanyBlock__iconsWrap__2fZbv",
                AboutCompanyBlock__buttonWrapper: "styles_AboutCompanyBlock__buttonWrapper__121-h"
            }
        },
        78565: function (o) {
            o.exports = {
                AboutPotentialBlock__titleWrap: "styles_AboutPotentialBlock__titleWrap__1tArW",
                AboutPotentialBlock__title: "styles_AboutPotentialBlock__title__1fceU",
                AboutPotentialBlock__contentWrap: "styles_AboutPotentialBlock__contentWrap__2DPIY",
                AboutPotentialBlock__content: "styles_AboutPotentialBlock__content__1U5Tp",
                AboutPotentialBlock__tabs: "styles_AboutPotentialBlock__tabs__WTuKD",
                AboutPotentialBlock__tabsWrap: "styles_AboutPotentialBlock__tabsWrap__1mM9A",
                AboutPotentialBlock__tabContent: "styles_AboutPotentialBlock__tabContent__2JHTf",
                AboutPotentialBlock__itemsWrap: "styles_AboutPotentialBlock__itemsWrap__zYYpI",
                AboutPotentialBlock__item: "styles_AboutPotentialBlock__item__2brWG",
                AboutPotentialBlock__imageWrap: "styles_AboutPotentialBlock__imageWrap__y3JHq",
                MainAboutBlock__imageTop: "styles_MainAboutBlock__imageTop__39TEH",
                MainAboutBlock__image: "styles_MainAboutBlock__image__2Z5z1",
                AboutPotentialBlock__image: "styles_AboutPotentialBlock__image__1uP4m",
                AboutPotentialBlock__imageTop: "styles_AboutPotentialBlock__imageTop__2_swv",
                AboutPotentialBlock__achievements: "styles_AboutPotentialBlock__achievements__38ERA",
                AboutPotentialBlock__achievementsWrap: "styles_AboutPotentialBlock__achievementsWrap__fZqcQ",
                AboutPotentialBlock__achievement: "styles_AboutPotentialBlock__achievement__2x_L-",
                AboutPotentialBlock__achievementImageWrap: "styles_AboutPotentialBlock__achievementImageWrap__1b0QZ",
                AboutPotentialBlock__achievementImage: "styles_AboutPotentialBlock__achievementImage__3HWGI",
                AboutPotentialBlock__achievementButtonWrap: "styles_AboutPotentialBlock__achievementButtonWrap__1xoZA",
                AboutPotentialBlock__navigationBlock: "styles_AboutPotentialBlock__navigationBlock__2CHww",
                AboutPotentialBlock__navigation: "styles_AboutPotentialBlock__navigation__3MdU6",
                AboutPotentialBlock__navigationTitleWrap: "styles_AboutPotentialBlock__navigationTitleWrap__2tgp-",
                AboutPotentialBlock__navigationTitle: "styles_AboutPotentialBlock__navigationTitle__36tTH",
                AboutPotentialBlock__arrowWrap: "styles_AboutPotentialBlock__arrowWrap__3nm6H",
                AboutPotentialBlock__description: "styles_AboutPotentialBlock__description__1JlYG",
                AboutPotentialBlock__achievementDescription: "styles_AboutPotentialBlock__achievementDescription__1Q7Ih",
                AboutPotentialBlock__tabsWrap_active: "styles_AboutPotentialBlock__tabsWrap_active__ckVVy",
                AboutPotentialBlock__button: "styles_AboutPotentialBlock__button__3u8Pc",
                AboutPotentialBlock__tabsWrap_inActive: "styles_AboutPotentialBlock__tabsWrap_inActive__2dFxa",
                AboutPotentialBlock__buttonWrap: "styles_AboutPotentialBlock__buttonWrap__2B6QY",
                AboutPotentialBlock__buttonWrap_active: "styles_AboutPotentialBlock__buttonWrap_active__3Wtk0",
                AboutPotentialBlock__button_active: "styles_AboutPotentialBlock__button_active__3ygIu",
                AboutPotentialBlock__button_inactive: "styles_AboutPotentialBlock__button_inactive__3Tl7f",
                active: "styles_active__1s3aT",
                inactive: "styles_inactive__uw75-"
            }
        },
        42840: function (o) {
            o.exports = {
                ProductionSliderBlock: "styles_ProductionSliderBlock__2E3zP",
                ProductionSliderBlock__descriptionWrap: "styles_ProductionSliderBlock__descriptionWrap__16Z6P",
                ProductionSliderBlock__titleWrap: "styles_ProductionSliderBlock__titleWrap__2XLCo",
                ProductionSliderBlock__secondTitle: "styles_ProductionSliderBlock__secondTitle__31YqD",
                ProductionSliderBlock__contentWrap: "styles_ProductionSliderBlock__contentWrap__3M69R",
                ProductionSliderBlock__content: "styles_ProductionSliderBlock__content__2_p2r",
                ProductionSliderBlock__textWrap: "styles_ProductionSliderBlock__textWrap__3sJb5",
                ProductionSliderBlock__description: "styles_ProductionSliderBlock__description__K7pTV",
                ProductionSliderBlock__underline: "styles_ProductionSliderBlock__underline__2u9y0",
                ProductionSliderBlock__items: "styles_ProductionSliderBlock__items__18i4P",
                ProductionSliderBlock__itemsWrap: "styles_ProductionSliderBlock__itemsWrap__XO0Qc",
                ProductionSliderBlock__item: "styles_ProductionSliderBlock__item__zgPZ4",
                ProductionSliderBlock__imageWrap: "styles_ProductionSliderBlock__imageWrap__20RPH",
                ProductionSliderBlock__image: "styles_ProductionSliderBlock__image__2WToh",
                ProductionSliderBlock__navigationWrap: "styles_ProductionSliderBlock__navigationWrap__19PDT",
                ProductionSliderBlock__navigation: "styles_ProductionSliderBlock__navigation__YM3Jk",
                ProductionSliderBlock__navigation_currentSlide: "styles_ProductionSliderBlock__navigation_currentSlide__3q6Jk",
                ProductionSliderBlock__navigation_allSlides: "styles_ProductionSliderBlock__navigation_allSlides__3GyTC",
                ProductionSliderBlock__arrowWrap: "styles_ProductionSliderBlock__arrowWrap__2GGXM",
                ProductionSliderBlock__arrow_next: "styles_ProductionSliderBlock__arrow_next__29TCl",
                ProductionSliderBlock__arrow_disabled: "styles_ProductionSliderBlock__arrow_disabled__3y3Yg",
                ProductionSliderBlock__iconsWrap: "styles_ProductionSliderBlock__iconsWrap__3lCno",
                ProductionSliderBlock__buttonWrapper: "styles_ProductionSliderBlock__buttonWrapper__AHiqv",
                ProductionSliderBlock__buttonWrap: "styles_ProductionSliderBlock__buttonWrap__1AgKL"
            }
        },
        27819: function (o) {
            o.exports = {
                ProductionSliderBlock: "styles_ProductionSliderBlock__2bCHJ",
                ProductionSliderBlock__titleWrap: "styles_ProductionSliderBlock__titleWrap__2kTVq",
                ProductionSliderBlock__descriptionWrap: "styles_ProductionSliderBlock__descriptionWrap__36TRB",
                ProductionSliderBlock__title: "styles_ProductionSliderBlock__title__QVmm3",
                ProductionSliderBlock__secondTitle: "styles_ProductionSliderBlock__secondTitle__WD7pP",
                ProductionSliderBlock__contentWrap: "styles_ProductionSliderBlock__contentWrap__2GVNI",
                ProductionSliderBlock__content: "styles_ProductionSliderBlock__content__3a4vt",
                ProductionSliderBlock__textWrap: "styles_ProductionSliderBlock__textWrap__1GnOT",
                ProductionSliderBlock__description: "styles_ProductionSliderBlock__description__3gI_V",
                ProductionSliderBlock__underline: "styles_ProductionSliderBlock__underline__1tM90",
                ProductionSliderBlock__items: "styles_ProductionSliderBlock__items__2CvCz",
                ProductionSliderBlock__itemsWrap: "styles_ProductionSliderBlock__itemsWrap__3S-De",
                ProductionSliderBlock__item: "styles_ProductionSliderBlock__item__1bM_S",
                ProductionSliderBlock__imageWrap: "styles_ProductionSliderBlock__imageWrap__12Hrl",
                ProductionSliderBlock__image: "styles_ProductionSliderBlock__image__2ZmCi",
                ProductionSliderBlock__navigationWrap: "styles_ProductionSliderBlock__navigationWrap__3iqQX",
                ProductionSliderBlock__navigation: "styles_ProductionSliderBlock__navigation__3JwSE",
                ProductionSliderBlock__navigation_currentSlide: "styles_ProductionSliderBlock__navigation_currentSlide__3YMzC",
                ProductionSliderBlock__navigation_allSlides: "styles_ProductionSliderBlock__navigation_allSlides__2OWWl",
                ProductionSliderBlock__arrowWrap: "styles_ProductionSliderBlock__arrowWrap__xGGKi",
                ProductionSliderBlock__arrow_next: "styles_ProductionSliderBlock__arrow_next__2SRQG",
                ProductionSliderBlock__arrow_disabled: "styles_ProductionSliderBlock__arrow_disabled__3Dh8C",
                ProductionSliderBlock__iconsWrap: "styles_ProductionSliderBlock__iconsWrap__Ik2NS",
                ProductionSliderBlock__buttonWrapper: "styles_ProductionSliderBlock__buttonWrapper__2cZht",
                ProductionSliderBlock__buttonWrap: "styles_ProductionSliderBlock__buttonWrap__3lAMT"
            }
        },
        72606: function (o) {
            o.exports = {
                ProductionSliderBlock: "styles_ProductionSliderBlock__10z4S",
                ProductionSliderBlock__titleWrap: "styles_ProductionSliderBlock__titleWrap__1e8aP",
                ProductionSliderBlock__descriptionWrap: "styles_ProductionSliderBlock__descriptionWrap__3x5-F",
                ProductionSliderBlock__title: "styles_ProductionSliderBlock__title__2q-IV",
                ProductionSliderBlock__secondTitle: "styles_ProductionSliderBlock__secondTitle__1De50",
                ProductionSliderBlock__contentWrap: "styles_ProductionSliderBlock__contentWrap__MAMEM",
                ProductionSliderBlock__content: "styles_ProductionSliderBlock__content__3h9XE",
                ProductionSliderBlock__textWrap: "styles_ProductionSliderBlock__textWrap__V_sVK",
                ProductionSliderBlock__description: "styles_ProductionSliderBlock__description__2Q8uQ",
                ProductionSliderBlock__underline: "styles_ProductionSliderBlock__underline__17Njm",
                ProductionSliderBlock__items: "styles_ProductionSliderBlock__items__1BMKB",
                ProductionSliderBlock__itemsWrap: "styles_ProductionSliderBlock__itemsWrap__1cHO-",
                ProductionSliderBlock__item: "styles_ProductionSliderBlock__item__19tc9",
                ProductionSliderBlock__imageWrap: "styles_ProductionSliderBlock__imageWrap__1wy5K",
                ProductionSliderBlock__image: "styles_ProductionSliderBlock__image__3UudH",
                ProductionSliderBlock__navigationWrap: "styles_ProductionSliderBlock__navigationWrap__1wk62",
                ProductionSliderBlock__navigation: "styles_ProductionSliderBlock__navigation__11-6D",
                ProductionSliderBlock__navigation_currentSlide: "styles_ProductionSliderBlock__navigation_currentSlide__2xy-C",
                ProductionSliderBlock__navigation_allSlides: "styles_ProductionSliderBlock__navigation_allSlides__2NvtA",
                ProductionSliderBlock__arrowWrap: "styles_ProductionSliderBlock__arrowWrap__1m7ET",
                ProductionSliderBlock__arrow_next: "styles_ProductionSliderBlock__arrow_next__2w8q-",
                ProductionSliderBlock__arrow_disabled: "styles_ProductionSliderBlock__arrow_disabled__1BZst",
                ProductionSliderBlock__iconsWrap: "styles_ProductionSliderBlock__iconsWrap__2CuJf",
                ProductionSliderBlock__buttonWrapper: "styles_ProductionSliderBlock__buttonWrapper__qUayC",
                ProductionSliderBlock__buttonWrap: "styles_ProductionSliderBlock__buttonWrap__l7z9B"
            }
        },
        72781: function (o) {
            o.exports = {
                ProductionSliderBlock: "styles_ProductionSliderBlock__3HUTP",
                ProductionSliderBlock__titleWrap: "styles_ProductionSliderBlock__titleWrap__3gEz8",
                ProductionSliderBlock__title: "styles_ProductionSliderBlock__title__qbdGH",
                ProductionSliderBlock__descriptionWrap: "styles_ProductionSliderBlock__descriptionWrap__1idYT",
                ProductionSliderBlock__secondTitle: "styles_ProductionSliderBlock__secondTitle__2p083",
                ProductionSliderBlock__contentWrap: "styles_ProductionSliderBlock__contentWrap__8bSwT",
                ProductionSliderBlock__content: "styles_ProductionSliderBlock__content__yIGM1",
                ProductionSliderBlock__textWrap: "styles_ProductionSliderBlock__textWrap__3SujE",
                ProductionSliderBlock__description: "styles_ProductionSliderBlock__description__33jrM",
                ProductionSliderBlock__underline: "styles_ProductionSliderBlock__underline__3dlRN",
                ProductionSliderBlock__items: "styles_ProductionSliderBlock__items__3FGrB",
                ProductionSliderBlock__itemsWrap: "styles_ProductionSliderBlock__itemsWrap__3FsKp",
                ProductionSliderBlock__item: "styles_ProductionSliderBlock__item__1gdc6",
                ProductionSliderBlock__imageWrap: "styles_ProductionSliderBlock__imageWrap__3vtj6",
                ProductionSliderBlock__image: "styles_ProductionSliderBlock__image__1Tq99",
                ProductionSliderBlock__navigationWrap: "styles_ProductionSliderBlock__navigationWrap__3siTS",
                ProductionSliderBlock__navigation: "styles_ProductionSliderBlock__navigation__JghR_",
                ProductionSliderBlock__navigation_currentSlide: "styles_ProductionSliderBlock__navigation_currentSlide__3GttH",
                ProductionSliderBlock__navigation_allSlides: "styles_ProductionSliderBlock__navigation_allSlides__3lDba",
                ProductionSliderBlock__arrowWrap: "styles_ProductionSliderBlock__arrowWrap__2BcQf",
                ProductionSliderBlock__arrow_next: "styles_ProductionSliderBlock__arrow_next__2VrO3",
                ProductionSliderBlock__arrow_disabled: "styles_ProductionSliderBlock__arrow_disabled__2DDTU",
                ProductionSliderBlock__iconsWrap: "styles_ProductionSliderBlock__iconsWrap__2eWpd",
                ProductionSliderBlock__buttonWrapper: "styles_ProductionSliderBlock__buttonWrapper__3_CE7",
                ProductionSliderBlock__buttonWrap: "styles_ProductionSliderBlock__buttonWrap__1Nj-f"
            }
        },
        57642: function (o) {
            o.exports = {
                MainBlockSlider: "styles_MainBlockSlider__1CQVq",
                MainBlockSlider__titleWrap: "styles_MainBlockSlider__titleWrap__2npWm",
                MainBlockSlider__title: "styles_MainBlockSlider__title__GVvHV",
                MainBlockSlider__contentWrap: "styles_MainBlockSlider__contentWrap__3Dz1a",
                MainBlockSlider__content: "styles_MainBlockSlider__content__2vjIA",
                MainBlockSlider__textWrap: "styles_MainBlockSlider__textWrap__3MHSV",
                MainBlockSlider__description: "styles_MainBlockSlider__description__1iA1-",
                MainBlockSlider__items: "styles_MainBlockSlider__items__23xfm",
                MainBlockSlider__itemsWrap: "styles_MainBlockSlider__itemsWrap__h6h8q",
                MainBlockSlider__item: "styles_MainBlockSlider__item__3s0rQ",
                MainBlockSlider__imageWrap: "styles_MainBlockSlider__imageWrap__OtOyb",
                MainBlockSlider__image: "styles_MainBlockSlider__image__3M5AI",
                MainBlockSlider__navigationWrap: "styles_MainBlockSlider__navigationWrap__2UI9g",
                MainBlockSlider__navigation: "styles_MainBlockSlider__navigation__3mEsM",
                MainBlockSlider__navigation_currentSlide: "styles_MainBlockSlider__navigation_currentSlide__17NMK",
                MainBlockSlider__navigation_allSlides: "styles_MainBlockSlider__navigation_allSlides__eHyre",
                MainBlockSlider__arrowWrap: "styles_MainBlockSlider__arrowWrap__2zhLJ",
                MainBlockSlider__arrow_next: "styles_MainBlockSlider__arrow_next__2txUl",
                MainBlockSlider__arrow_disabled: "styles_MainBlockSlider__arrow_disabled__1l_wq",
                MainBlockSlider__iconsWrap: "styles_MainBlockSlider__iconsWrap__3Ge7J",
                MainBlockSlider__buttonWrapper: "styles_MainBlockSlider__buttonWrapper__3l110",
                MainBlockSlider__buttonWrap: "styles_MainBlockSlider__buttonWrap__218op"
            }
        },
        10924: function (o) {
            o.exports = {
                ProductionSliderBlock: "styles_ProductionSliderBlock__3pgAI",
                ProductionSliderBlock__titleWrap: "styles_ProductionSliderBlock__titleWrap__3fFfd",
                ProductionSliderBlock__title: "styles_ProductionSliderBlock__title__2hD2b",
                ProductionSliderBlock__descriptionWrap: "styles_ProductionSliderBlock__descriptionWrap__3atUi",
                ProductionSliderBlock__secondTitle: "styles_ProductionSliderBlock__secondTitle__3vdDF",
                ProductionSliderBlock__contentWrap: "styles_ProductionSliderBlock__contentWrap__1QV48",
                ProductionSliderBlock__content: "styles_ProductionSliderBlock__content__2GZ3k",
                ProductionSliderBlock__textWrap: "styles_ProductionSliderBlock__textWrap__1yz8n",
                ProductionSliderBlock__description: "styles_ProductionSliderBlock__description__lxCk-",
                ProductionSliderBlock__underline: "styles_ProductionSliderBlock__underline__1kxpQ",
                ProductionSliderBlock__items: "styles_ProductionSliderBlock__items__3nKbF",
                ProductionSliderBlock__itemsWrap: "styles_ProductionSliderBlock__itemsWrap__1qbZm",
                ProductionSliderBlock__item: "styles_ProductionSliderBlock__item__2ZS1z",
                ProductionSliderBlock__imageWrap: "styles_ProductionSliderBlock__imageWrap__2OZwm",
                ProductionSliderBlock__image: "styles_ProductionSliderBlock__image__1NeyC",
                ProductionSliderBlock__navigationWrap: "styles_ProductionSliderBlock__navigationWrap__eonyD",
                ProductionSliderBlock__navigation: "styles_ProductionSliderBlock__navigation__1QK8y",
                ProductionSliderBlock__navigation_currentSlide: "styles_ProductionSliderBlock__navigation_currentSlide__t1mwN",
                ProductionSliderBlock__navigation_allSlides: "styles_ProductionSliderBlock__navigation_allSlides__1pf52",
                ProductionSliderBlock__arrowWrap: "styles_ProductionSliderBlock__arrowWrap__lc-Fx",
                ProductionSliderBlock__arrow_next: "styles_ProductionSliderBlock__arrow_next__3V00H",
                ProductionSliderBlock__arrow_disabled: "styles_ProductionSliderBlock__arrow_disabled__3an1i",
                ProductionSliderBlock__iconsWrap: "styles_ProductionSliderBlock__iconsWrap__3ZJt_",
                ProductionSliderBlock__buttonWrapper: "styles_ProductionSliderBlock__buttonWrapper__2kI2x",
                ProductionSliderBlock__buttonWrap: "styles_ProductionSliderBlock__buttonWrap__1kXz8"
            }
        },
        7585: function (o) {
            o.exports = {
                ProductionSliderBlock: "styles_ProductionSliderBlock__p2cXl",
                ProductionSliderBlock__titleWrap: "styles_ProductionSliderBlock__titleWrap__3oATM",
                ProductionSliderBlock__title: "styles_ProductionSliderBlock__title__2ohDE",
                ProductionSliderBlock__descriptionWrap: "styles_ProductionSliderBlock__descriptionWrap__1vqdd",
                ProductionSliderBlock__secondTitle: "styles_ProductionSliderBlock__secondTitle__1XvyB",
                ProductionSliderBlock__contentWrap: "styles_ProductionSliderBlock__contentWrap__3hIql",
                ProductionSliderBlock__content: "styles_ProductionSliderBlock__content__yjPlR",
                ProductionSliderBlock__textWrap: "styles_ProductionSliderBlock__textWrap__3LJs4",
                ProductionSliderBlock__description: "styles_ProductionSliderBlock__description__2-lvZ",
                ProductionSliderBlock__underline: "styles_ProductionSliderBlock__underline__RZolh",
                ProductionSliderBlock__items: "styles_ProductionSliderBlock__items__1Dr5o",
                ProductionSliderBlock__itemsWrap: "styles_ProductionSliderBlock__itemsWrap__MlJXh",
                ProductionSliderBlock__item: "styles_ProductionSliderBlock__item__1BlXV",
                ProductionSliderBlock__imageWrap: "styles_ProductionSliderBlock__imageWrap__3VGpV",
                ProductionSliderBlock__image: "styles_ProductionSliderBlock__image__dynWE",
                ProductionSliderBlock__navigationWrap: "styles_ProductionSliderBlock__navigationWrap__1rQSH",
                ProductionSliderBlock__navigation: "styles_ProductionSliderBlock__navigation__1RA67",
                ProductionSliderBlock__navigation_currentSlide: "styles_ProductionSliderBlock__navigation_currentSlide__3GqXk",
                ProductionSliderBlock__navigation_allSlides: "styles_ProductionSliderBlock__navigation_allSlides__3jowA",
                ProductionSliderBlock__arrowWrap: "styles_ProductionSliderBlock__arrowWrap__1VkFL",
                ProductionSliderBlock__arrow_next: "styles_ProductionSliderBlock__arrow_next__2iPrc",
                ProductionSliderBlock__arrow_disabled: "styles_ProductionSliderBlock__arrow_disabled__8SR4m",
                ProductionSliderBlock__iconsWrap: "styles_ProductionSliderBlock__iconsWrap__3egO_",
                ProductionSliderBlock__buttonWrapper: "styles_ProductionSliderBlock__buttonWrapper__3Ss0l",
                ProductionSliderBlock__buttonWrap: "styles_ProductionSliderBlock__buttonWrap__3oAuU"
            }
        },
        19740: function (o) {
            o.exports = {
                ProductionSliderBlock: "styles_ProductionSliderBlock__2lCXe",
                ProductionSliderBlock__titleWrap: "styles_ProductionSliderBlock__titleWrap__2n3YK",
                ProductionSliderBlock__descriptionWrap: "styles_ProductionSliderBlock__descriptionWrap__1mxnq",
                ProductionSliderBlock__title: "styles_ProductionSliderBlock__title__2p8Ew",
                ProductionSliderBlock__secondTitle: "styles_ProductionSliderBlock__secondTitle__3LFe-",
                ProductionSliderBlock__contentWrap: "styles_ProductionSliderBlock__contentWrap__32wDH",
                ProductionSliderBlock__content: "styles_ProductionSliderBlock__content__sgOOG",
                ProductionSliderBlock__textWrap: "styles_ProductionSliderBlock__textWrap__184vp",
                ProductionSliderBlock__description: "styles_ProductionSliderBlock__description__Uuj7I",
                ProductionSliderBlock__underline: "styles_ProductionSliderBlock__underline__3ncCx",
                ProductionSliderBlock__items: "styles_ProductionSliderBlock__items__A1AJz",
                ProductionSliderBlock__itemsWrap: "styles_ProductionSliderBlock__itemsWrap__3zAK1",
                ProductionSliderBlock__item: "styles_ProductionSliderBlock__item__3a9jR",
                ProductionSliderBlock__imageWrap: "styles_ProductionSliderBlock__imageWrap__1imhM",
                ProductionSliderBlock__image: "styles_ProductionSliderBlock__image__v7wrg",
                ProductionSliderBlock__navigationWrap: "styles_ProductionSliderBlock__navigationWrap__3t7i0",
                ProductionSliderBlock__navigation: "styles_ProductionSliderBlock__navigation__1gM64",
                ProductionSliderBlock__navigation_currentSlide: "styles_ProductionSliderBlock__navigation_currentSlide__GmHj_",
                ProductionSliderBlock__navigation_allSlides: "styles_ProductionSliderBlock__navigation_allSlides__3NTHk",
                ProductionSliderBlock__arrowWrap: "styles_ProductionSliderBlock__arrowWrap__44EWq",
                ProductionSliderBlock__arrow_next: "styles_ProductionSliderBlock__arrow_next__195jO",
                ProductionSliderBlock__arrow_disabled: "styles_ProductionSliderBlock__arrow_disabled__iNSEu",
                ProductionSliderBlock__iconsWrap: "styles_ProductionSliderBlock__iconsWrap__213Ih",
                ProductionSliderBlock__buttonWrapper: "styles_ProductionSliderBlock__buttonWrapper__uWKul",
                ProductionSliderBlock__buttonWrap: "styles_ProductionSliderBlock__buttonWrap__R6pIl"
            }
        },
        54022: function (o) {
            o.exports = {
                ProductionSliderBlock: "styles_ProductionSliderBlock__2slY2",
                ProductionSliderBlock__titleWrap: "styles_ProductionSliderBlock__titleWrap__3aQQL",
                ProductionSliderBlock__descriptionWrap: "styles_ProductionSliderBlock__descriptionWrap__19si9",
                ProductionSliderBlock__title: "styles_ProductionSliderBlock__title__2kUUZ",
                ProductionSliderBlock__secondTitle: "styles_ProductionSliderBlock__secondTitle__3ZivY",
                ProductionSliderBlock__contentWrap: "styles_ProductionSliderBlock__contentWrap__aFsw3",
                ProductionSliderBlock__content: "styles_ProductionSliderBlock__content__2o5WX",
                ProductionSliderBlock__textWrap: "styles_ProductionSliderBlock__textWrap__2V2U9",
                ProductionSliderBlock__description: "styles_ProductionSliderBlock__description__1KYua",
                ProductionSliderBlock__underline: "styles_ProductionSliderBlock__underline__6OhIQ",
                ProductionSliderBlock__items: "styles_ProductionSliderBlock__items__soeVO",
                ProductionSliderBlock__itemsWrap: "styles_ProductionSliderBlock__itemsWrap__1OmmA",
                ProductionSliderBlock__item: "styles_ProductionSliderBlock__item__3-2p6",
                ProductionSliderBlock__imageWrap: "styles_ProductionSliderBlock__imageWrap__17H8L",
                ProductionSliderBlock__image: "styles_ProductionSliderBlock__image__6YIdI",
                ProductionSliderBlock__navigationWrap: "styles_ProductionSliderBlock__navigationWrap__1xwRD",
                ProductionSliderBlock__navigation: "styles_ProductionSliderBlock__navigation__-HcHy",
                ProductionSliderBlock__navigation_currentSlide: "styles_ProductionSliderBlock__navigation_currentSlide__1lbQ-",
                ProductionSliderBlock__navigation_allSlides: "styles_ProductionSliderBlock__navigation_allSlides__1GGnj",
                ProductionSliderBlock__arrowWrap: "styles_ProductionSliderBlock__arrowWrap__3XAnI",
                ProductionSliderBlock__arrow_next: "styles_ProductionSliderBlock__arrow_next__1hFP1",
                ProductionSliderBlock__arrow_disabled: "styles_ProductionSliderBlock__arrow_disabled__1NY9s",
                ProductionSliderBlock__iconsWrap: "styles_ProductionSliderBlock__iconsWrap__2N2OM",
                ProductionSliderBlock__buttonWrapper: "styles_ProductionSliderBlock__buttonWrapper__3ODQd",
                ProductionSliderBlock__buttonWrap: "styles_ProductionSliderBlock__buttonWrap__GGNpw"
            }
        },
        41582: function (o) {
            o.exports = {
                MainPage: "styles_MainPage__HzL1l"
            }
        }
    },
    function (o) {
        o.O(0, [663, 334, 653, 53, 146, 308, 774, 888, 179], (function () {
            return i = 69823, o(o.s = i);
            var i
        }));
        var i = o.O();
        _N_E = i
    }
]);