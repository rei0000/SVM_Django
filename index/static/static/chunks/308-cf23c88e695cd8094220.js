(self.webpackChunk_N_E = self.webpackChunk_N_E || []).push([
    [308], {
        23646: function (e, t, r) {
            var n = r(67228);
            e.exports = function (e) {
                if (Array.isArray(e)) return n(e)
            }
        },
        59713: function (e) {
            e.exports = function (e, t, r) {
                return t in e ? Object.defineProperty(e, t, {
                    value: r,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = r, e
            }
        },
        46860: function (e) {
            e.exports = function (e) {
                if ("undefined" !== typeof Symbol && Symbol.iterator in Object(e)) return Array.from(e)
            }
        },
        98206: function (e) {
            e.exports = function () {
                throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
            }
        },
        319: function (e, t, r) {
            var n = r(23646),
                o = r(46860),
                s = r(60379),
                c = r(98206);
            e.exports = function (e) {
                return n(e) || o(e) || s(e) || c()
            }
        },
        53671: function (e, t, r) {
            var n = r(17854),
                o = r(19662),
                s = r(47908),
                c = r(68361),
                u = r(26244),
                a = n.TypeError,
                i = function (e) {
                    return function (t, r, n, i) {
                        o(r);
                        var d = s(t),
                            l = c(d),
                            _ = u(d),
                            f = e ? _ - 1 : 0,
                            p = e ? -1 : 1;
                        if (n < 2)
                            for (;;) {
                                if (f in l) {
                                    i = l[f], f += p;
                                    break
                                }
                                if (f += p, e ? f < 0 : _ <= f) throw a("Reduce of empty array with no initial value")
                            }
                        for (; e ? f >= 0 : _ > f; f += p) f in l && (i = r(i, l[f], f, d));
                        return i
                    }
                };
            e.exports = {
                left: i(!1),
                right: i(!0)
            }
        },
        85827: function (e, t, r) {
            "use strict";
            var n = r(82109),
                o = r(53671).left,
                s = r(9341),
                c = r(7392),
                u = r(35268);
            n({
                target: "Array",
                proto: !0,
                forced: !s("reduce") || !u && c > 79 && c < 83
            }, {
                reduce: function (e) {
                    var t = arguments.length;
                    return o(this, e, t, t > 1 ? arguments[1] : void 0)
                }
            })
        },
        65069: function (e, t, r) {
            "use strict";
            var n = r(82109),
                o = r(1702),
                s = r(43157),
                c = o([].reverse),
                u = [1, 2];
            n({
                target: "Array",
                proto: !0,
                forced: String(u) === String(u.reverse())
            }, {
                reverse: function () {
                    return s(this) && (this.length = this.length), c(this)
                }
            })
        },
        69957: function (e, t, r) {
            "use strict";
            r(24812);
            var n = r(85893),
                o = (r(67294), r(47166)),
                s = r.n(o),
                c = r(94328),
                u = r.n(c),
                a = s().bind(u());
            t.Z = function (e) {
                var t = e.theme,
                    r = void 0 === t ? "wBorder" : t,
                    o = e.onClick,
                    s = e.className,
                    c = e.children,
                    u = e.download;
                return u ? (0, n.jsx)("a", {
                    href: u,
                    className: a("Button", "Button_".concat(r), s),
                    onClick: o,
                    download: !0,
                    target: "_blank",
                    rel: "noreferrer",
                    children: c
                }) : (0, n.jsx)("button", {
                    className: a("Button", "Button_".concat(r), s),
                    onClick: o,
                    children: c
                })
            }
        },
        5563: function (e, t, r) {
            "use strict";
            r(24812);
            var n = r(85893),
                o = (r(67294), r(47166)),
                s = r.n(o),
                c = r(99936),
                u = r.n(c),
                a = s().bind(u());
            t.Z = function (e) {
                var t = e.level,
                    r = void 0 === t ? 1 : t,
                    o = e.className,
                    s = e.children,
                    c = e.noTransfrom,
                    u = void 0 !== c && c,
                    i = "h1";
                switch (+r) {
                    case 2:
                        i = "h2";
                        break;
                    case 3:
                        i = "h3";
                        break;
                    case 4:
                        i = "h4";
                        break;
                    default:
                        i = "h1"
                }
                return (0, n.jsx)(i, {
                    className: a("Heading", "Heading_level-".concat(r), {
                        Heading_notransform: u
                    }, o),
                    children: s
                })
            }
        },
        82050: function (e, t, r) {
            "use strict";
            r(21249), r(24812);
            var n = r(96156),
                o = r(85893),
                s = r(67294),
                c = r(30653),
                u = r(15027),
                a = r(5563),
                i = r(69957),
                d = r(47166),
                l = r.n(d),
                _ = r(40863),
                f = r.n(_);

            function p(e, t) {
                var r = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(e);
                    t && (n = n.filter((function (t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), r.push.apply(r, n)
                }
                return r
            }

            function m(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var r = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? p(Object(r), !0).forEach((function (t) {
                        (0, n.Z)(e, t, r[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(r)) : p(Object(r)).forEach((function (t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(r, t))
                    }))
                }
                return e
            }
            var h = function (e) {
                return (0, o.jsxs)("svg", m(m({}, e), {}, {
                    children: [(0, o.jsxs)("g", {
                        clipPath: "url(#a)",
                        children: [(0, o.jsx)("path", {
                            d: "M20.9346 5.83087L15.3097 0.206016C15.1785 0.0746719 14.9993 0 14.8125 0H4.96875C3.80564 0 2.85938 0.946266 2.85938 2.10938V21.8906C2.85938 23.0537 3.80564 24 4.96875 24H19.0312C20.1944 24 21.1406 23.0537 21.1406 21.8906V6.32812C21.1406 6.13641 21.0601 5.95636 20.9346 5.83087ZM15.5156 2.40061L18.74 5.625H16.2188C15.831 5.625 15.5156 5.30958 15.5156 4.92188V2.40061ZM19.0312 22.5938H4.96875C4.58105 22.5938 4.26562 22.2783 4.26562 21.8906V2.10938C4.26562 1.72167 4.58105 1.40625 4.96875 1.40625H14.1094V4.92188C14.1094 6.08498 15.0556 7.03125 16.2188 7.03125H19.7344V21.8906C19.7344 22.2783 19.419 22.5938 19.0312 22.5938Z",
                            fill: "#03439A"
                        }), (0, o.jsx)("path", {
                            d: "M16.2188 9.9375H7.78125C7.39294 9.9375 7.07812 10.2523 7.07812 10.6406C7.07812 11.0289 7.39294 11.3438 7.78125 11.3438H16.2188C16.6071 11.3438 16.9219 11.0289 16.9219 10.6406C16.9219 10.2523 16.6071 9.9375 16.2188 9.9375Z",
                            fill: "#03439A"
                        }), (0, o.jsx)("path", {
                            d: "M16.2188 12.75H7.78125C7.39294 12.75 7.07812 13.0648 7.07812 13.4531C7.07812 13.8414 7.39294 14.1562 7.78125 14.1562H16.2188C16.6071 14.1562 16.9219 13.8414 16.9219 13.4531C16.9219 13.0648 16.6071 12.75 16.2188 12.75Z",
                            fill: "#03439A"
                        }), (0, o.jsx)("path", {
                            d: "M16.2188 15.5625H7.78125C7.39294 15.5625 7.07812 15.8773 7.07812 16.2656C7.07812 16.6539 7.39294 16.9688 7.78125 16.9688H16.2188C16.6071 16.9688 16.9219 16.6539 16.9219 16.2656C16.9219 15.8773 16.6071 15.5625 16.2188 15.5625Z",
                            fill: "#03439A"
                        }), (0, o.jsx)("path", {
                            d: "M13.4062 18.375H7.78125C7.39294 18.375 7.07812 18.6898 7.07812 19.0781C7.07812 19.4664 7.39294 19.7812 7.78125 19.7812H13.4062C13.7946 19.7812 14.1094 19.4664 14.1094 19.0781C14.1094 18.6898 13.7946 18.375 13.4062 18.375Z",
                            fill: "#03439A"
                        })]
                    }), (0, o.jsx)("defs", {
                        children: (0, o.jsx)("clipPath", {
                            id: "a",
                            children: (0, o.jsx)("rect", {
                                width: "24",
                                height: "24",
                                fill: "white"
                            })
                        })
                    })]
                }))
            };
            h.defaultProps = {
                width: "24",
                height: "24",
                viewBox: "0 0 24 24",
                fill: "none",
                xmlns: "http://www.w3.org/2000/svg"
            };
            var v = l().bind(f());
            t.Z = function (e) {
                var t = e.title,
                    r = e.titleDescription,
                    n = e.button,
                    d = (0, s.useState)(!0),
                    l = d[0],
                    _ = d[1];
                return (0, o.jsx)("div", {
                    className: v("RequestProductsForm"),
                    children: (0, o.jsx)(u.Z, {
                        className: v("RequestProductsForm__container"),
                        children: (0, o.jsxs)("div", {
                            className: v("RequestProductsForm__contentWrap"),
                            children: [(0, o.jsxs)("div", {
                                className: v("RequestProductsForm__content"),
                                children: [(0, o.jsx)("div", {
                                    className: v("RequestProductsForm__titleWrap"),
                                    children: (0, o.jsx)(a.Z, {
                                        className: v("RequestProductsForm__title"),
                                        level: "3",
                                        children: (0, c.ZP)(t)
                                    })
                                }), (0, o.jsx)("div", {
                                    className: v("RequestProductsForm__content__description"),
                                    children: r
                                }), n.map((function (e) {
                                    return (0, o.jsx)("div", {
                                        className: v("RequestProductsForm__content__button"),
                                        children: (0, o.jsxs)("div", {
                                            className: v("RequestProductsForm__content__buttonWrap"),
                                            children: [(0, o.jsx)(h, {}), e.button]
                                        })
                                    }, e.id)
                                }))]
                            }), (0, o.jsxs)("div", {
                                className: v("RequestProductsForm__content"),
                                children: [(0, o.jsxs)("form", {
                                    className: v("RequestProductsForm__form"),
                                    children: [(0, o.jsxs)("div", {
                                        className: v("RequestProductsForm__fields"),
                                        children: [(0, o.jsx)("input", {
                                            className: v("RequestProductsForm__input"),
                                            type: "email",
                                            placeholder: "\u0412\u0430\u0448\u0435 \u0438\u043c\u044f"
                                        }), (0, o.jsx)("input", {
                                            className: v("RequestProductsForm__input"),
                                            type: "email",
                                            placeholder: "E-mail/\u0422\u0435\u043b\u0435\u0444\u043e\u043d"
                                        }), (0, o.jsx)("input", {
                                            className: v("RequestProductsForm__input"),
                                            type: "text",
                                            placeholder: "\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435 \u043e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u0438"
                                        }), (0, o.jsx)("input", {
                                            className: v("RequestProductsForm__input"),
                                            type: "file"
                                        }), (0, o.jsx)("textarea", {
                                            className: v("RequestProductsForm__inputBig"),
                                            placeholder: "\u0422\u0435\u043a\u0441\u0442 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f"
                                        })]
                                    }), (0, o.jsxs)("div", {
                                        className: v("RequestProductsForm__checkboxWrap"),
                                        children: [(0, o.jsx)("input", {
                                            className: v("RequestProductsForm__checkbox"),
                                            type: "checkbox",
                                            checked: l,
                                            onChange: function () {
                                                _(!l)
                                            }
                                        }), (0, o.jsx)("div", {
                                            className: v("RequestProductsForm__checkboxText"),
                                            children: "\u0414\u0430\u044e \u0441\u043e\u0433\u043b\u0430\u0441\u0438\u0435 \u043d\u0430 \u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0443 \u0438 \u0445\u0440\u0430\u043d\u0435\u043d\u0438\u0435 \u0441\u0432\u043e\u0438\u0445 \u043f\u0435\u0440\u0441\u043e\u043d\u0430\u043b\u044c\u043d\u044b\u0445 \u0434\u0430\u043d\u043d\u044b\u0445 \u0438 \u0441\u043e\u0433\u043b\u0430\u0448\u0430\u044e\u0441\u044c \u0441 \u041f\u043e\u043b\u0438\u0442\u0438\u043a\u043e\u0439 \u043a\u043e\u043d\u0444\u0438\u0434\u0435\u043d\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u0438"
                                        })]
                                    })]
                                }), (0, o.jsx)("div", {
                                    className: v("RequestProductsForm__buttonWrap"),
                                    children: (0, o.jsx)(i.Z, {
                                        theme: "wBorder",
                                        className: v("RequestProductsForm__button"),
                                        children: "\u041e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u044c"
                                    })
                                })]
                            })]
                        })
                    })
                })
            }
        },
        23398: function (e, t, r) {
            "use strict";
            var n;
            t.__esModule = !0, t.AmpStateContext = void 0;
            var o = ((n = r(67294)) && n.__esModule ? n : {
                default: n
            }).default.createContext({});
            t.AmpStateContext = o
        },
        76393: function (e, t, r) {
            "use strict";
            t.__esModule = !0, t.isInAmpMode = c, t.useAmp = function () {
                return c(o.default.useContext(s.AmpStateContext))
            };
            var n, o = (n = r(67294)) && n.__esModule ? n : {
                    default: n
                },
                s = r(23398);

            function c() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                    t = e.ampFirst,
                    r = void 0 !== t && t,
                    n = e.hybrid,
                    o = void 0 !== n && n,
                    s = e.hasQuery,
                    c = void 0 !== s && s;
                return r || o && c
            }
        },
        92775: function (e, t, r) {
            "use strict";
            r(92222), r(57327), r(82772), r(66992), r(21249), r(85827), r(65069), r(47042), r(5212), r(69070), r(38880), r(41539), r(70189), r(78783), r(23157), r(4129), r(88921), r(96248), r(13599), r(11477), r(64362), r(15389), r(90401), r(45164), r(91238), r(54837), r(87485), r(56767), r(76651), r(61437), r(35285), r(39865), r(78206), r(33948);
            var n = r(59713);

            function o(e, t) {
                var r = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(e);
                    t && (n = n.filter((function (t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), r.push.apply(r, n)
                }
                return r
            }
            t.__esModule = !0, t.defaultHead = _, t.default = void 0;
            var s, c = function (e) {
                    if (e && e.__esModule) return e;
                    if (null === e || "object" !== typeof e && "function" !== typeof e) return {
                        default: e
                    };
                    var t = l();
                    if (t && t.has(e)) return t.get(e);
                    var r = {},
                        n = Object.defineProperty && Object.getOwnPropertyDescriptor;
                    for (var o in e)
                        if (Object.prototype.hasOwnProperty.call(e, o)) {
                            var s = n ? Object.getOwnPropertyDescriptor(e, o) : null;
                            s && (s.get || s.set) ? Object.defineProperty(r, o, s) : r[o] = e[o]
                        } r.default = e, t && t.set(e, r);
                    return r
                }(r(67294)),
                u = (s = r(73244)) && s.__esModule ? s : {
                    default: s
                },
                a = r(23398),
                i = r(41165),
                d = r(76393);

            function l() {
                if ("function" !== typeof WeakMap) return null;
                var e = new WeakMap;
                return l = function () {
                    return e
                }, e
            }

            function _() {
                var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
                    t = [c.default.createElement("meta", {
                        charSet: "utf-8"
                    })];
                return e || t.push(c.default.createElement("meta", {
                    name: "viewport",
                    content: "width=device-width"
                })), t
            }

            function f(e, t) {
                return "string" === typeof t || "number" === typeof t ? e : t.type === c.default.Fragment ? e.concat(c.default.Children.toArray(t.props.children).reduce((function (e, t) {
                    return "string" === typeof t || "number" === typeof t ? e : e.concat(t)
                }), [])) : e.concat(t)
            }
            var p = ["name", "httpEquiv", "charSet", "itemProp"];

            function m(e, t) {
                return e.reduce((function (e, t) {
                    var r = c.default.Children.toArray(t.props.children);
                    return e.concat(r)
                }), []).reduce(f, []).reverse().concat(_(t.inAmpMode)).filter(function () {
                    var e = new Set,
                        t = new Set,
                        r = new Set,
                        n = {};
                    return function (o) {
                        var s = !0,
                            c = !1;
                        if (o.key && "number" !== typeof o.key && o.key.indexOf("$") > 0) {
                            c = !0;
                            var u = o.key.slice(o.key.indexOf("$") + 1);
                            e.has(u) ? s = !1 : e.add(u)
                        }
                        switch (o.type) {
                            case "title":
                            case "base":
                                t.has(o.type) ? s = !1 : t.add(o.type);
                                break;
                            case "meta":
                                for (var a = 0, i = p.length; a < i; a++) {
                                    var d = p[a];
                                    if (o.props.hasOwnProperty(d))
                                        if ("charSet" === d) r.has(d) ? s = !1 : r.add(d);
                                        else {
                                            var l = o.props[d],
                                                _ = n[d] || new Set;
                                            "name" === d && c || !_.has(l) ? (_.add(l), n[d] = _) : s = !1
                                        }
                                }
                        }
                        return s
                    }
                }()).reverse().map((function (e, r) {
                    var s = e.key || r;
                    if (!t.inAmpMode && "link" === e.type && e.props.href && ["https://fonts.googleapis.com/css", "https://use.typekit.net/"].some((function (t) {
                            return e.props.href.startsWith(t)
                        }))) {
                        var u = function (e) {
                            for (var t = 1; t < arguments.length; t++) {
                                var r = null != arguments[t] ? arguments[t] : {};
                                t % 2 ? o(Object(r), !0).forEach((function (t) {
                                    n(e, t, r[t])
                                })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(r)) : o(Object(r)).forEach((function (t) {
                                    Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(r, t))
                                }))
                            }
                            return e
                        }({}, e.props || {});
                        return u["data-href"] = u.href, u.href = void 0, u["data-optimized-fonts"] = !0, c.default.cloneElement(e, u)
                    }
                    return c.default.cloneElement(e, {
                        key: s
                    })
                }))
            }
            var h = function (e) {
                var t = e.children,
                    r = (0, c.useContext)(a.AmpStateContext),
                    n = (0, c.useContext)(i.HeadManagerContext);
                return c.default.createElement(u.default, {
                    reduceComponentsToState: m,
                    headManager: n,
                    inAmpMode: (0, d.isInAmpMode)(r)
                }, t)
            };
            t.default = h
        },
        73244: function (e, t, r) {
            "use strict";
            var n = r(319),
                o = r(34575),
                s = r(93913),
                c = (r(81506), r(2205)),
                u = r(78585),
                a = r(29754);

            function i(e) {
                var t = function () {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Date.prototype.toString.call(Reflect.construct(Date, [], (function () {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function () {
                    var r, n = a(e);
                    if (t) {
                        var o = a(this).constructor;
                        r = Reflect.construct(n, arguments, o)
                    } else r = n.apply(this, arguments);
                    return u(this, r)
                }
            }
            t.__esModule = !0, t.default = void 0;
            var d = r(67294),
                l = function (e) {
                    c(r, e);
                    var t = i(r);

                    function r(e) {
                        var s;
                        return o(this, r), (s = t.call(this, e))._hasHeadManager = void 0, s.emitChange = function () {
                            s._hasHeadManager && s.props.headManager.updateHead(s.props.reduceComponentsToState(n(s.props.headManager.mountedInstances), s.props))
                        }, s._hasHeadManager = s.props.headManager && s.props.headManager.mountedInstances, s
                    }
                    return s(r, [{
                        key: "componentDidMount",
                        value: function () {
                            this._hasHeadManager && this.props.headManager.mountedInstances.add(this), this.emitChange()
                        }
                    }, {
                        key: "componentDidUpdate",
                        value: function () {
                            this.emitChange()
                        }
                    }, {
                        key: "componentWillUnmount",
                        value: function () {
                            this._hasHeadManager && this.props.headManager.mountedInstances.delete(this), this.emitChange()
                        }
                    }, {
                        key: "render",
                        value: function () {
                            return null
                        }
                    }]), r
                }(d.Component);
            t.default = l
        },
        94328: function (e) {
            e.exports = {
                Button: "styles_Button__JVxxi",
                Button_noBorder: "styles_Button_noBorder__aDEJg"
            }
        },
        99936: function (e) {
            e.exports = {
                Heading: "styles_Heading__1jcMq",
                Heading_notransform: "styles_Heading_notransform__jggwP",
                "Heading_level-1": "styles_Heading_level-1__grV3t",
                "Heading_level-2": "styles_Heading_level-2__I6U-q",
                "Heading_level-3": "styles_Heading_level-3__UYMsw",
                "Heading_level-4": "styles_Heading_level-4__2w4dT"
            }
        },
        40863: function (e) {
            e.exports = {
                RequestProductsForm: "styles_RequestProductsForm__3VZp6",
                RequestProductsForm__form: "styles_RequestProductsForm__form__2rEJX",
                RequestProductsForm__container: "styles_RequestProductsForm__container__3Ibv5",
                RequestProductsForm__contentWrap: "styles_RequestProductsForm__contentWrap__2c-qr",
                RequestProductsForm__content: "styles_RequestProductsForm__content__1pkzs",
                RequestProductsForm__content__description: "styles_RequestProductsForm__content__description__3NY7M",
                RequestProductsForm__content__button: "styles_RequestProductsForm__content__button__1a0b0",
                RequestProductsForm__content__buttonWrap: "styles_RequestProductsForm__content__buttonWrap__3fjYB",
                RequestProductsForm__titleWrap: "styles_RequestProductsForm__titleWrap__1wT4A",
                RequestProductsForm__title: "styles_RequestProductsForm__title__2wR7_",
                RequestProductsForm__fields: "styles_RequestProductsForm__fields__39Zh5",
                RequestProductsForm__input: "styles_RequestProductsForm__input__i5r6K",
                RequestProductsForm__inputBig: "styles_RequestProductsForm__inputBig__10-_k",
                RequestProductsForm__checkboxWrap: "styles_RequestProductsForm__checkboxWrap__1JBqW",
                RequestProductsForm__checkboxText: "styles_RequestProductsForm__checkboxText__3-FNr",
                RequestProductsForm__buttonWrap: "styles_RequestProductsForm__buttonWrap__1Ylme",
                RequestProductsForm__button: "styles_RequestProductsForm__button__31cUS"
            }
        },
        9008: function (e, t, r) {
            e.exports = r(92775)
        },
        50247: function () {}
    }
]);