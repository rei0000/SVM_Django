from django.contrib import admin
from aboutUs.models import aboutUsMainBlock, aboutUsMainBlockSlider, aboutUsMainBlockText, aboutUsNumberBlock, aboutUsProdBaseSlider, aboutUsProdBase, aboutUsSecondBlock, aboutUsSecondBlockSlider, aboutUsRewueFile
admin.site.register(aboutUsProdBaseSlider)
admin.site.register(aboutUsProdBase)
admin.site.register(aboutUsMainBlock)
admin.site.register(aboutUsMainBlockText)
admin.site.register(aboutUsMainBlockSlider)
admin.site.register(aboutUsNumberBlock)
admin.site.register(aboutUsSecondBlock)
admin.site.register(aboutUsSecondBlockSlider)
admin.site.register(aboutUsRewueFile)