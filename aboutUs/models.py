from calendar import c
from django.db import models
from ckeditor.fields import RichTextField
from .validators import validate_file_size

class aboutUsMainBlock(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.TextField(max_length=999)

    def __str__(self):
        return str(self.title)
    class Meta:
        verbose_name_plural = "О нас, первый блок Наименование"
class aboutUsMainBlockText(models.Model):
    id = models.AutoField(primary_key=True)
    aboutUsMainBlock = models.ForeignKey(aboutUsMainBlock, related_name='textList', on_delete=models.CASCADE)
    text = RichTextField(blank=True, null=True, max_length=999)

    def __str__(self):
        return str(self.text)
    class Meta:
        verbose_name_plural = "О нас, первый блок текст"

class aboutUsMainBlockSlider(models.Model):
    id = models.AutoField(primary_key=True)
    aboutUsMainBlock = models.ForeignKey(aboutUsMainBlock, related_name='sliderList', on_delete=models.CASCADE)
    imgupload=models.FileField( upload_to='static/static/img/aboutUsMainBlockSlider', validators=[validate_file_size])
    def __str__(self):
        return str(self.imgupload)
    class Meta:
        verbose_name_plural = "О нас, первый блок слайдер"
class aboutUsSecondBlock(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.TextField(max_length=999)
    disc = RichTextField(blank=True, null=True, max_length=999)
    def __str__(self):
        return str(self.title)
    class Meta:
        verbose_name_plural = "О нас, последний блок тайтл и описание"

class aboutUsSecondBlockSlider(models.Model):
    id = models.AutoField(primary_key=True)
    aboutUsSecondBlock = models.ForeignKey(aboutUsSecondBlock, related_name='sliderList', on_delete=models.CASCADE)
    imgupload=models.FileField(upload_to='static/static/img/aboutUsSecondBlockSlider', validators=[validate_file_size])
    disc = models.TextField(max_length=999)
    def __str__(self):
        return str(self.disc)
    class Meta:
        verbose_name_plural = "О нас, последний блок слайдер"
    
class aboutUsNumberBlock(models.Model):
    id = models.AutoField(primary_key=True)
    number = models.TextField(max_length=999)
    disc = models.TextField(max_length=999)
    def __str__(self):
        return str(self.disc)
    class Meta:
        verbose_name_plural = "О нас, блок с цифрами и фактами"
    
class aboutUsProdBase(models.Model):
    anchor=models.TextField('anchor', max_length=999)
    title=models.TextField('title', max_length=999)
    description = RichTextField(blank=True, null=True, max_length=999)
    id = models.AutoField(primary_key=True)
    def __str__(self):
        return str(self.title)
    class Meta:
        verbose_name_plural = "О нас, блок производственная база"

class aboutUsProdBaseSlider(models.Model):
    id = models.AutoField(primary_key=True)
    aboutUsProdBase = models.ForeignKey(aboutUsProdBase, related_name='sliderList', on_delete=models.CASCADE)
    imgupload=models.FileField( upload_to='static/static/img/aboutUsProdBaseSlider', validators=[validate_file_size])
    def __str__(self):
        return str(self.imgupload)
    class Meta:
        verbose_name_plural = "О нас, блок производственная база слайдер"

class aboutUsRewueFile(models.Model):
    id = models.AutoField(primary_key=True)
    titleDescription=models.TextField(blank=True, null=True, max_length=999)
    rewuefile=models.FileField( upload_to='static/static/img/aboutUsProdBaseSlider', validators=[validate_file_size])
    def __str__(self):
        return str(self.rewuefile)
    class Meta:
        verbose_name_plural = "О нас, блок отзыва"