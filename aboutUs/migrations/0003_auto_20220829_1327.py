# Generated by Django 3.2.9 on 2022-08-29 09:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aboutUs', '0002_auto_20220826_1142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutusmainblock',
            name='title',
            field=models.TextField(max_length=999),
        ),
        migrations.AlterField(
            model_name='aboutusmainblocktext',
            name='text',
            field=models.TextField(max_length=999),
        ),
        migrations.AlterField(
            model_name='aboutusnumberblock',
            name='disc',
            field=models.TextField(max_length=999),
        ),
        migrations.AlterField(
            model_name='aboutusnumberblock',
            name='number',
            field=models.TextField(max_length=999),
        ),
        migrations.AlterField(
            model_name='aboutusprodbase',
            name='anchor',
            field=models.TextField(max_length=999, verbose_name='anchor'),
        ),
        migrations.AlterField(
            model_name='aboutusprodbase',
            name='description',
            field=models.TextField(max_length=999, verbose_name='ref'),
        ),
        migrations.AlterField(
            model_name='aboutusprodbase',
            name='title',
            field=models.TextField(max_length=999, verbose_name='title'),
        ),
        migrations.AlterField(
            model_name='aboutussecondblock',
            name='disc',
            field=models.TextField(max_length=999),
        ),
        migrations.AlterField(
            model_name='aboutussecondblock',
            name='title',
            field=models.TextField(max_length=999),
        ),
        migrations.AlterField(
            model_name='aboutussecondblockslider',
            name='disc',
            field=models.TextField(max_length=999),
        ),
    ]
