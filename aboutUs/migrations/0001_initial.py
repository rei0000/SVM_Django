# Generated by Django 3.1.3 on 2022-08-01 12:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='aboutUsMainBlock',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('title', models.TextField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='aboutUsNumberBlock',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('number', models.TextField(max_length=100)),
                ('disc', models.TextField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='aboutUsProdBase',
            fields=[
                ('anchor', models.TextField(max_length=50, verbose_name='anchor')),
                ('title', models.TextField(max_length=50, verbose_name='title')),
                ('description', models.TextField(max_length=500, verbose_name='ref')),
                ('id', models.AutoField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='aboutUsSecondBlock',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('title', models.TextField(max_length=100)),
                ('disc', models.TextField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='aboutUsSecondBlockSlider',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('imgupload', models.FileField(upload_to='tatic/static/img/aboutUsSecondBlockSlider')),
                ('disc', models.TextField(max_length=500)),
                ('aboutUsSecondBlock', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sliderList', to='aboutUs.aboutussecondblock')),
            ],
        ),
        migrations.CreateModel(
            name='aboutUsProdBaseSlider',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('imgupload', models.FileField(upload_to='tatic/static/img/aboutUsProdBaseSlider')),
                ('aboutUsProdBase', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sliderList', to='aboutUs.aboutusprodbase')),
            ],
        ),
        migrations.CreateModel(
            name='aboutUsMainBlockText',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('text', models.TextField(max_length=500)),
                ('aboutUsMainBlock', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='textList', to='aboutUs.aboutusmainblock')),
            ],
        ),
        migrations.CreateModel(
            name='aboutUsMainBlockSlider',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('imgupload', models.FileField(upload_to='tatic/static/img/aboutUsMainBlockSlider')),
                ('aboutUsMainBlock', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sliderList', to='aboutUs.aboutusmainblock')),
            ],
        ),
    ]
